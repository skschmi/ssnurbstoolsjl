
include("../nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.del_basis_dRdxi
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.bernstein_basis
import .SSNurbsTools.del_bernstein_basis
import .SSNurbsTools.outputObj
import .SSNurbsTools.InsertKnot
import .SSNurbsTools.nurbs_basis
import .SSNurbsTools.e_a_xi_for_gb_t

using PyPlot; plt = PyPlot;

dim_p = 2
dim_s = 3
p = [3,3]
n = [6,6]
span = n .- 1
knot_v = Array[[0., 0, 0, 0, 1, 3, 4, 4, 4, 4],[0., 0, 0, 0, 1, 3, 4, 4, 4, 4]]
#knot_v = Array[[0., 0, 0, 0, 1, 2, 3, 3, 3, 3],[0., 0, 0, 0, 1, 2, 3, 3, 3, 3]]

c_pts = zeros(dim_s,n[1],n[2])

c_pts[:,1,1] = [0.0, 0.1, 0.1]
c_pts[:,2,1] = [1.0, 0.2, 0.2]
c_pts[:,3,1] = [2.0, 0.3, 0.1]
c_pts[:,4,1] = [3.0, 0.1, 0.2]
c_pts[:,5,1] = [4.0, 0.3, 0.3]
c_pts[:,6,1] = [5.0, 0.1, 0.1]

c_pts[:,1,2] = [0.0, 1.0, 0.2]
c_pts[:,2,2] = [1.0, 1.0, 0.1]
c_pts[:,3,2] = [2.0, 1.0, 0.3]
c_pts[:,4,2] = [3.0, 1.0, 0.2]
c_pts[:,5,2] = [4.0, 1.0, 0.1]
c_pts[:,6,2] = [5.0, 1.0, 0.3]

c_pts[:,1,3] = [0.0, 2.0, 0.4]
c_pts[:,2,3] = [1.0, 2.0, 0.3]
c_pts[:,3,3] = [2.0, 2.0, 0.2]
c_pts[:,4,3] = [3.0, 2.0, 0.1]
c_pts[:,5,3] = [4.0, 2.0, 0.2]
c_pts[:,6,3] = [5.0, 2.0, 0.3]

c_pts[:,1,4] = [0.0, 3.5, 0.3]
c_pts[:,2,4] = [1.0, 3.4, 0.1]
c_pts[:,3,4] = [2.0, 3.3, 0.4]
c_pts[:,4,4] = [3.0, 3.4, 0.1]
c_pts[:,5,4] = [4.0, 3.3, 0.4]
c_pts[:,6,4] = [5.0, 3.4, 0.2]

c_pts[:,1,5] = [0.0, 4.0, 0.1]
c_pts[:,2,5] = [1.0, 4.0, 0.1]
c_pts[:,3,5] = [2.0, 4.0, 0.1]
c_pts[:,4,5] = [3.0, 4.0, 0.1]
c_pts[:,5,5] = [4.0, 4.0, 0.1]
c_pts[:,6,5] = [5.0, 4.0, 0.1]

c_pts[:,1,6] = [0.0, 5.5, 0.2]
c_pts[:,2,6] = [1.0, 5.4, 0.2]
c_pts[:,3,6] = [2.0, 5.3, 0.2]
c_pts[:,4,6] = [3.0, 5.4, 0.3]
c_pts[:,5,6] = [4.0, 5.3, 0.2]
c_pts[:,6,6] = [5.0, 5.4, 0.2]

weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1., 1., 1., 1., 1., 1.])
weights[2] = Array{Float64}([1., 1., 1., 1., 1., 1.])
patch = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)


res_to_draw = [30,30]
X,t,h = draw(patch,res_to_draw)
plt.figure()
plt.scatter3D(X[1,:,:],X[2,:,:],X[3,:,:])
plt.scatter3D(patch.control_pts[1,:,:],patch.control_pts[2,:,:],patch.control_pts[3,:,:],color="r")
plt.xlabel("x")
plt.ylabel("y")
plt.zlabel("z");
plt.title("Surface")

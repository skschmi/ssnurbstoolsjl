

include("../ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts

include("../dualbasis.jl")
using .SSDualBasis
import .SSDualBasis.computeDualBasis
import .SSDualBasis.computeBSplineDualBasis

using .SSDualBasis.SSNurbsTools
import .SSDualBasis.SSNurbsTools.Nurbs
import .SSDualBasis.SSNurbsTools.draw
import .SSDualBasis.SSNurbsTools.basis
import .SSDualBasis.SSNurbsTools.del_basis_dRdxi
import .SSDualBasis.SSNurbsTools.Jacobian
import .SSDualBasis.SSNurbsTools.e_xi_for_t
import .SSDualBasis.SSNurbsTools.domain_X
import .SSDualBasis.SSNurbsTools.bernstein_basis
import .SSDualBasis.SSNurbsTools.del_bernstein_basis
import .SSDualBasis.SSNurbsTools.outputObj
import .SSDualBasis.SSNurbsTools.InsertKnot
import .SSDualBasis.SSNurbsTools.nurbs_basis
import .SSDualBasis.SSNurbsTools.e_a_xi_for_gb_t
import .SSDualBasis.SSNurbsTools.ConvertToBernsteinMesh
import .SSDualBasis.SSNurbsTools.RefineMesh



dim_p = 1
dim_s = 1
p = [3]
n = [7]
spans = n .- 1
knot_v = Array[[0., 0, 0, 0, 1, 2, 3, 4, 4, 4, 4]]
c_pts = reshape([ (knot_v[1][2] + knot_v[1][3] + knot_v[1][4])/3,
                  (knot_v[1][3] + knot_v[1][4] + knot_v[1][5])/3,
                  (knot_v[1][4] + knot_v[1][5] + knot_v[1][6])/3,
                  (knot_v[1][5] + knot_v[1][6] + knot_v[1][7])/3,
                  (knot_v[1][6] + knot_v[1][7] + knot_v[1][8])/3,
                  (knot_v[1][7] + knot_v[1][8] + knot_v[1][9])/3,
                  (knot_v[1][8] + knot_v[1][9] + knot_v[1][10])/3 ],1,7)
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.,1.])
self = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)
bern = Nurbs(self)
ConvertToBernsteinMesh(bern)

using PyPlot; plt = PyPlot;

figure_size = (8, 2.2)

# This is the index of the global basis function that we want the dual basis of.
gb_arr = [1,2,3,4,5,6,7]
supports_arr = [ [1], [1,2], [1,2,3], [1,2,3,4], [2,3,4], [3,4], [4] ]

# The global b-spline basis functions
bsplbasisf = function(gb::Int64,t::Float64)
		Bval = 0.0
		e,xi = e_xi_for_t(self,reshape([t],1,1))
		e = e[1]
		xi = xi[1]
		element_a_for_gb = self.GBEBI[1][e,gb][1]
		if( element_a_for_gb > 0 )
			Bval += basis(self, [e], [element_a_for_gb], [xi])
		end
		return Bval
	end

plot_colors = [ "b", "g", "r", "c", "m", "y", "k", "w"]


function build_matrix_diagram(
				A::Array{Float64,2},
				x::Array{Float64,1},
				b::Array{Float64,1},
				line_thickness_major::Float64,
				marker_size::Float64,
				margin::Float64,
				include_strike::Bool )

	x = rand(10)
	y = rand(10)
	nrows = size(A)[1]
	ncols = size(A)[2]
	@show(nrows)
	@show(ncols)
	colwidth = 1.0
	rowwidth = 1.0
	top = max(rowwidth * nrows, colwidth * ncols)
	bottom = 0.0
	left = 0.0
	right = left + ncols*colwidth + colwidth + 2*colwidth + 2*colwidth + colwidth

	width = right - left
	height = top - bottom
	final_top = top + 4*margin
	final_bottom = top - height - margin
	final_left = left - margin
	final_right = left + width + margin

	fig_width = 5
	fig_height = fig_width * (final_top-final_bottom)/(final_right-final_left)
	@show( fig_width )
	@show( fig_height )
	figure_size_mtrxdiag = (fig_width, fig_height)
	plt.figure(figsize=figure_size_mtrxdiag)
	plt.axis("off")
	plt.ylim([final_bottom, final_top])
	plt.xlim([final_left, final_right])

	# Plot the 'A' matrix.
	begin
		for i = 0:nrows
			plt.plot([left,left+ncols*colwidth],[top-i*rowwidth,top-i*rowwidth],color="k",linewidth=line_thickness_major)
		end
		for i = 0:ncols
			plt.plot([left+i*colwidth,left+i*colwidth],[top,top-nrows*rowwidth],color="k",linewidth=line_thickness_major)
		end
		for i = 1:nrows
			for j = 1:ncols
				if( abs(A[i,j]) > 1e-12 )
					plt.scatter([ left + (j-1)*colwidth + 0.5*colwidth ], [ top - (i-1)*rowwidth - 0.5*rowwidth ],color="k",s=marker_size)
				end
			end
		end
	end

	# Plot the 'x' vector
	xvec_left = left+ncols*colwidth + colwidth
	begin
		# note: the 'x' vector is 'ncols' tall.
		plt.plot([xvec_left, xvec_left],[top,top-ncols*rowwidth],color="k",linewidth=line_thickness_major)
		plt.plot([xvec_left + colwidth, xvec_left + colwidth],[top,top-ncols*rowwidth],color="k",linewidth=line_thickness_major)
		for i = 1:ncols+1
			plt.plot([xvec_left, xvec_left + colwidth],[top-(i-1)*rowwidth,top-(i-1)*rowwidth],color="k",linewidth=line_thickness_major)
		end
		for j = 1:ncols
			plt.scatter([ xvec_left + 0.5*colwidth ], [ top - (j-1)*rowwidth - 0.5*rowwidth ],color="k",s=marker_size)
		end
	end

	# draw the equals sign
	equals_left = xvec_left + 2*colwidth
	begin
		plt.plot([equals_left, equals_left + 0.75*colwidth],[top-div(nrows,2)*rowwidth,top-div(nrows,2)*rowwidth],color="k",linewidth=line_thickness_major)
		plt.plot([equals_left, equals_left + 0.75*colwidth],[top-div(nrows,2)*rowwidth-0.5*rowwidth,top-div(nrows,2)*rowwidth-0.5*rowwidth ],color="k",linewidth=line_thickness_major)
	end

	# Plot the 'b' vector
	bvec_left = equals_left + 2*colwidth
	begin
		# note: the 'b' vector is 'nrows' tall.
		plt.plot([bvec_left, bvec_left],[top,top-nrows*rowwidth],color="k",linewidth=line_thickness_major)
		plt.plot([bvec_left + colwidth, bvec_left + colwidth],[top,top-nrows*rowwidth],color="k",linewidth=line_thickness_major)
		for i = 1:nrows+1
			plt.plot([bvec_left, bvec_left + colwidth],[top-(i-1)*rowwidth,top-(i-1)*rowwidth],color="k",linewidth=line_thickness_major)
		end
		@show(b)
		for j = 1:nrows
			if( abs(b[j]) > 1e-12 )
				plt.scatter([ bvec_left + 0.5*colwidth ], [ top - (j-1)*rowwidth - 0.5*rowwidth ],color="k",s=marker_size)
			end
		end
	end

	ax = plt.axes()
	ax[:text]( left+0.4*colwidth, top+0.2*rowwidth, "i → ", color="black" )
	ax[:text]( right+0.3*colwidth, top-2*rowwidth+0.3*rowwidth, "← A", color="black" )
	ax[:text]( left-0.6*colwidth, top-0.7*rowwidth, "B", color="black" )
	ax[:text]( left-0.65*colwidth, top-1.4*rowwidth, "↓", color="black" )
	ax[:text]( left-0.6*colwidth, top-(nrows-2)*rowwidth-0.7*rowwidth, "e", color="black" )
	ax[:text]( left-0.65*colwidth, top-(nrows-2)*rowwidth-1.4*rowwidth, "↓", color="black" )
	ax[:text]( left + 2*colwidth - 0.6*colwidth, top+1.0*rowwidth, "e = 1", color="black" )
	ax[:text]( left + 6*colwidth - 0.7*colwidth, top+1.0*rowwidth, "e = 2", color="black" )

	if( include_strike )
		plt.plot([left-0.2*colwidth,left+ncols*colwidth+0.0*colwidth],[top-1.5*rowwidth,top-1.5*rowwidth],color="r",linewidth=line_thickness_major*2.5)
		plt.plot([right-1.0*colwidth,right+0.4*colwidth],[top-1.5*rowwidth,top-1.5*rowwidth],color="r",linewidth=line_thickness_major*2.5)
	end

	return
end





# Assembling the dual basis function from the bernstein coeffs
dualbasisf = function(COEFFS::Array{Float64,2},t::Float64)
		Bval = 0.0
		for e0 = 1:4
			for a = 1:4
				e,xi = e_xi_for_t(self,reshape([t],1,1))
				e = e[1]
				xi = xi[1]
				if( e == e0 )
					Bval += COEFFS[a,e0]*bernstein_basis(a,self.degree[1],xi,dmin=-1.,dmax=1.)
				end
			end
		end
		return Bval
	end

# Plotting dual basis function
COEFFS_ARR = Array{Any}(undef,length(gb_arr))
#for i = 1:length(gb_arr)
begin
	
	i = 2
	gb = gb_arr[i]
	support = supports_arr[i]

	## Now compute the dual basis for one of the global basis functions of the B-Spline.
	COEFFS,Amat,dvec,bvec = computeBSplineDualBasis( self, gb, support )

	begin
		plt.figure(figsize=figure_size)
		COEFFS_ARR[i] = COEFFS
		t = collect(minimum(knot_v[1]):0.005:maximum(knot_v[1]))
		B = zeros(length(t))
		for j = 1:length(t)
			B[j] = dualbasisf(COEFFS,t[j])
		end
		# Set the discontinuous sections to NaN so they don't plot.
		for j = 2:length(t)
			if( abs(B[j-1]-B[j]) > 0.5 )
				B[j] = NaN
			end
		end
		plt.plot(t,B)
		plt.title("Dual basis function "*L"\bar{N}_2")
		plt.savefig("plots_ignore/dualbasis_plot_N2.pdf")
	end

	begin
		line_thickness_major = 2.0
		marker_size = 40.0
		margin = 0.1
		include_strike=false
		build_matrix_diagram( Amat, dvec, bvec, line_thickness_major, marker_size, margin, include_strike )
		plt.savefig("plots_ignore/dualbasis_system_N2.pdf")

		include_strike=true
		build_matrix_diagram( Amat, dvec, bvec, line_thickness_major, marker_size, margin, include_strike )
		plt.savefig("plots_ignore/dualbasis_system_N2_striked.pdf")
	end
end

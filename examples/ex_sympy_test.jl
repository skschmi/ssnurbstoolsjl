
using SymPy
s1 = symbols("s1")
f = 3*s1 + 4
answer = N(evalf(f,subs=Dict([(s1,3.2)])))
@show answer

a = Sym(4)
answer = N(evalf(a))
@show answer


f = Sym(1/2)
N(evalf(f,subs=Dict([(s1,0.3)])))

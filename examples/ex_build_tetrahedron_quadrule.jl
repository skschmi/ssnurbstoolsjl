
include("../nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.del_basis_dRdxi
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.bernstein_basis
import .SSNurbsTools.del_bernstein_basis
import .SSNurbsTools.outputObj
import .SSNurbsTools.InsertKnot
import .SSNurbsTools.nurbs_basis
import .SSNurbsTools.e_a_xi_for_gb_t

include("../ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts
using LinearAlgebra
import .LinearAlgebra.det

degree = 12
gqt = GaussQuadTable( NumGaussPts( degree )+1 )
#@show( gqt )
hexpts = zeros(3,gqt.count,gqt.count,gqt.count)
hexwts = zeros(gqt.count,gqt.count,gqt.count)
for i = 1:gqt.count
	for j = 1:gqt.count
		for k = 1:gqt.count
			hexpts[1,i,j,k] = gqt.pts[i]
			hexpts[2,i,j,k] = gqt.pts[j]
			hexpts[3,i,j,k] = gqt.pts[k]
			hexwts[i,j,k] = gqt.wts[i]*gqt.wts[j]*gqt.wts[k]
		end
	end
end

dim_p = 3
dim_s = 3
p = [1,1,1]
n = [2,2,2]
spans = n .- 1
knot_v = Array[[0., 0, 1, 1],[0., 0, 1, 1],[0., 0, 1, 1]]
weights = Array{Array{Float64}}(undef,3)
weights[1] = Array{Float64}([1., 1.])
weights[2] = Array{Float64}([1., 1.])
weights[3] = Array{Float64}([1., 1.])
c_pts = zeros(dim_s,n[1],n[2],n[3])
c_pts[:,1,1,1] = [0.0, 0.0, 0.0]
c_pts[:,2,1,1] = [1.0, 0.0, 0.0]
c_pts[:,1,2,1] = [0.0, 1.0, 0.0]
c_pts[:,1,1,2] = [0.0, 0.0, 1.0]
c_pts[:,1,2,2] = [0.0, 0.5, 0.5]
c_pts[:,2,1,2] = [0.5, 0.0, 0.5]
c_pts[:,2,2,1] = [0.5, 0.5, 0.0]
c_pts[:,2,2,2] = [1.0/3.0, 1.0/3.0, 1.0/3.0]
self = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)

# using PyPlot; plt = PyPlot;
# res = [15,15,15]
# X,t,h = draw(self,res)
# println( "size(X): ", size(X) )
# plt.figure()
# plt.scatter3D(X[1,:,:,:],X[2,:,:,:],X[3,:,:,:])
# plt.scatter3D(self.control_pts[1,:,:,:],self.control_pts[2,:,:,:],self.control_pts[3,:,:,:],color="r")
# plt.xlabel("x")
# plt.ylabel("y")
# plt.zlabel("z");
# plt.title("Surface")


tetpts = zeros(3,gqt.count,gqt.count,gqt.count)
tetwts = zeros(gqt.count,gqt.count,gqt.count)
function jdet(s,t,u)
	J = SSNurbsTools.Jacobian(self,[1,1,1],[s,t,u])
	return det(J)
end
#ans = jdet(0.1,0.1,0.7)
#@show( ans )

for i = 1:gqt.count
	for j = 1:gqt.count
		for k = 1:gqt.count
			newpt = domain_X( self, [1,1,1], reshape( hexpts[:,i,j,k], 3, 1 ) )
			tetpts[1,i,j,k] = newpt[1]
			tetpts[2,i,j,k] = newpt[2]
			tetpts[3,i,j,k] = newpt[3]
			tetwts[i,j,k] = 6 * hexwts[i,j,k] * jdet( hexpts[1,i,j,k], hexpts[2,i,j,k], hexpts[3,i,j,k] )
		end
	end
end

## Printing out all the gauss points and weights
println( "" )
println( "" )
global weights_sum = 0.0
for i = 1:gqt.count
	for j = 1:gqt.count
		for k = 1:gqt.count
			println( "{ ", tetpts[1,i,j,k], ",    ", tetpts[2,i,j,k], ",    ", tetpts[3,i,j,k], "  },                       ", tetwts[i,j,k], ", "  )
			global weights_sum += tetwts[i,j,k]
		end
	end
end
println( "" )
println( "" )
println( "Weights sum: ", weights_sum )

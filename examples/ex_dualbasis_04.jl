

include("../ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts

include("../dualbasis.jl")
using .SSDualBasis
import .SSDualBasis.computeDualBasis
import .SSDualBasis.computeBSplineDualBasis

using .SSDualBasis.SSNurbsTools
import .SSDualBasis.SSNurbsTools.Nurbs
import .SSDualBasis.SSNurbsTools.draw
import .SSDualBasis.SSNurbsTools.basis
import .SSDualBasis.SSNurbsTools.del_basis_dRdxi
import .SSDualBasis.SSNurbsTools.Jacobian
import .SSDualBasis.SSNurbsTools.e_xi_for_t
import .SSDualBasis.SSNurbsTools.domain_X
import .SSDualBasis.SSNurbsTools.bernstein_basis
import .SSDualBasis.SSNurbsTools.del_bernstein_basis
import .SSDualBasis.SSNurbsTools.outputObj
import .SSDualBasis.SSNurbsTools.InsertKnot
import .SSDualBasis.SSNurbsTools.nurbs_basis
import .SSDualBasis.SSNurbsTools.e_a_xi_for_gb_t
import .SSDualBasis.SSNurbsTools.ConvertToBernsteinMesh



dim_p = 1
dim_s = 1
p = [3]
n = [11]
spans = n .- 1
knot_v = Array[[0., 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 8, 8]]
c_pts = reshape([ (knot_v[1][2] + knot_v[1][3] + knot_v[1][4])/3,
                  (knot_v[1][3] + knot_v[1][4] + knot_v[1][5])/3,
                  (knot_v[1][4] + knot_v[1][5] + knot_v[1][6])/3,
                  (knot_v[1][5] + knot_v[1][6] + knot_v[1][7])/3,
                  (knot_v[1][6] + knot_v[1][7] + knot_v[1][8])/3,
                  (knot_v[1][7] + knot_v[1][8] + knot_v[1][9])/3,
                  (knot_v[1][8] + knot_v[1][9] + knot_v[1][10])/3,
                  (knot_v[1][9] + knot_v[1][10] + knot_v[1][11])/3,
                  (knot_v[1][10] + knot_v[1][11] + knot_v[1][12])/3,
                  (knot_v[1][11] + knot_v[1][12] + knot_v[1][13])/3,
                  (knot_v[1][12] + knot_v[1][13] + knot_v[1][14])/3 ],1,11)
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,1.])
self = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)
bern = Nurbs(self)
ConvertToBernsteinMesh(bern)

using PyPlot; plt = PyPlot;

# This is the index of the global basis function that we want the dual basis of.
gb_arr = [1,2,3,4,5,6,7,8,9,10,11]
supports_arr = [ [1], [1,2], [1,2,3], [1,2,3,4], [2,3,4,5], [3,4,5,6], [4,5,6,7], [5,6,7,8], [6,7,8], [7,8], [8] ]

# The global b-spline basis functions
bsplbasisf = function(gb::Int64,t::Float64)
		Bval = 0.0
		e,xi = e_xi_for_t(self,reshape([t],1,1))
		e = e[1]
		xi = xi[1]
		element_a_for_gb = self.GBEBI[1][e,gb][1]
		if( element_a_for_gb > 0 )
			Bval += basis(self, [e], [element_a_for_gb], [xi])
		end
		return Bval
	end

# Plotting the regular b-spline basis functions
figure_size = (8, 2)
plt.figure(figsize=figure_size)
for gb in gb_arr
	t = collect(minimum(knot_v[1]):0.005:maximum(knot_v[1]))
	B = zeros(length(t))
	for i = 1:length(t)
		B[i] = bsplbasisf(gb,t[i])
	end
	plt.plot(t,B)
	plt.title("B-spline basis")
end
### ********
#plt.savefig("bsplinebasesplot_02.pdf")


# Plotting the bernstein basis functions
begin
	bernsteinbasisf = function(bbi::Int64,t::Float64)
			Bval = 0.0
			e,xi = e_xi_for_t(self,reshape([t],1,1))
			e = e[1]
			xi = xi[1]
			a = mod(bbi-1,self.degree[1]+1)+1 #local bernstein basis index in the cell.
			if( e > 0 )
				Bval += bernstein_basis(a,self.degree[1],xi,dmin=-1.0,dmax=1.0)
			end
			return Bval
		end
	figure_size = (8, 2)
	plt.figure(figsize=figure_size)
	for bbi = 1:(self.nbezcurves[1] * (self.degree[1]+1))
		t = collect(minimum(knot_v[1]):0.005:maximum(knot_v[1]))
		B = zeros(length(t))
		for i = 1:length(t)
			B[i] = bernsteinbasisf(bbi,t[i])
		end
		# Set the discontinuous sections to NaN so they don't plot.
		for i = 2:length(t)
			if( abs(B[i-1]-B[i]) > 0.5 )
				B[i] = NaN
			end
		end
		plt.plot(t,B)
		plt.title("Bernstein bases")
	end
end
### ********
#plt.savefig("bernsteinbasesplot_02.pdf")

# Assembling the dual basis function from the bernstein coeffs
dualbasisf = function(COEFFS::Array{Float64,2},t::Float64)
		Bval = 0.0
		for e0 = 1:self.nbezcurves[1]
			for a = 1:(self.degree[1]+1)
				e,xi = e_xi_for_t(self,reshape([t],1,1))
				e = e[1]
				xi = xi[1]
				if( e == e0 )
					Bval += COEFFS[a,e0]*bernstein_basis(a,self.degree[1],xi,dmin=-1.,dmax=1.)
				end
			end
		end
		return Bval
	end


# Plotting dual basis function
figure_size = (8, 2)
plt.figure(figsize=figure_size)
COEFFS_ARR = Array{Any}(undef,length(gb_arr))
for i = 1:length(gb_arr)

	gb = gb_arr[i]
	support = supports_arr[i]

	## Now compute the dual basis for one of the global basis functions of the B-Spline.
	COEFFS,Amat,dvec,bvec = computeBSplineDualBasis( self, gb, support )

	COEFFS_ARR[i] = COEFFS
	t = collect(minimum(knot_v[1]):0.005:maximum(knot_v[1]))
	B = zeros(length(t))
	for i = 1:length(t)
		B[i] = dualbasisf(COEFFS,t[i])
	end
	# Set the discontinuous sections to NaN so they don't plot.
	for i = 2:length(t)
		if( abs(B[i-1]-B[i]) > 0.5 )
			B[i] = NaN
		end
	end
	plt.plot(t,B)
	plt.title("Dual basis")
end
### ********
#plt.savefig("dualbasesplot_02.pdf")


println("")
println("")
println("")
println("")
println("")


# Projecting a sine-curve onto the basis (to see how it looks).
begin
	# Plot the original sin function
	figure_size = (8, 2)
	plt.figure(figsize=figure_size)
	t = collect(minimum(knot_v[1]):0.005:maximum(knot_v[1]))
	sinf = function(t::Float64)
			return sin.(0.5*pi*t)
		end
	sinf_arr = Array{Float64}([sinf(t[i]) for i=1:length(t)])
	plt.plot(t,sinf_arr)


	GQT1D = SSGaussQuad.GaussQuadTable( 100*NumGaussPts(self.degree[1]) )
	bspl_coeffs = zeros( length(gb_arr) )
	for i = 1:length(gb_arr)
		gb = gb_arr[i]
		support = supports_arr[i]

		# Grab the bernstein coefficients for this dual basis out of the array (computed above)
		COEFFS = COEFFS_ARR[i]

		# The jacobian of the integration
		jacobianf = function(params)
				# dx/dxi
				return (maximum(knot_v[1])-minimum(knot_v[1]))/2.0
			end
		# The function under the integral.
		f = function(params)
				xi = params[1]  # xi goes from -1 to 1.
				t = minimum(knot_v[1]) + ((xi+1.0)/2.0) * (maximum(knot_v[1]) - minimum(knot_v[1]))
				return dualbasisf(COEFFS,t) * sinf(t)
			end
		# Computing the coefficient for this b-spline basis function.
		bspl_coeffs[gb] = GaussQuadIntegrate(f,GQT1D,jacobianf)
	end

	# The new projected function.
	projectedf = function(t::Float64)
			fval = 0.0
			for i = 1:length(gb_arr)
				gb = gb_arr[i]
				fval += bspl_coeffs[gb] * bsplbasisf(gb,t)
			end
			return fval
		end

	# Plot the new projected function.
	t = collect(minimum(knot_v[1]):0.005:maximum(knot_v[1]))
	PROJF = zeros(length(t))
	for i = 1:length(t)
		PROJF[i] = projectedf(t[i])
	end
	plt.plot(t,PROJF)
	plt.title("Projected function")
	plt.legend(["Exact","Projected"])
	### ********
	#plt.savefig("projectedsinf_02.pdf")

end



###

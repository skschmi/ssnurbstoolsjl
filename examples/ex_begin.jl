
include("../nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.RefineMesh
import .SSNurbsTools.draw
import .SSNurbsTools.Jacobian_ds
import .SSNurbsTools.domain_dXdsdxi
import .SSNurbsTools.delBasis
import .SSNurbsTools.delBasis_ds


dim_p = 2
dim_s = 2
p = [2,2]
n = [3,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 1, 1, 1],[0., 0, 0, 1, 1, 1]]
c_pts = Array{Function}(undef,dim_s,n[1],n[2])
#=
weights = Array{Array{Function}}(2)
for i = 1:length(weights)
    weights[i] = Array{Function}(3)
end
=#
weights = Array{Array{Float64}}(undef,2)
for i = 1:length(weights)
    weights[i] = ones(3)
end

#************ c_pts[:,1,1] = [0.,0.0]
c_pts[1,1,1] = function(s) 0. end
c_pts[2,1,1] = function(s) 0. end

#************ c_pts[:,2,1] = [1.,0.0]
c_pts[1,2,1] = function(s) 1. end
c_pts[2,2,1] = function(s) 0. end

#************ c_pts[:,3,1] = [2.,0.0]
c_pts[1,3,1] = function(s) 2. end
c_pts[2,3,1] = function(s) 0. end

#************ c_pts[:,1,2] = [0.,0.5]
c_pts[1,1,2] = function(s) 0. end
c_pts[2,1,2] = function(s) 0.5 end

#************ c_pts[:,2,2] = [1.,0.5]
c_pts[1,2,2] = function(s) 1. end
c_pts[2,2,2] = function(s)
            (s[1] + 0.5*(1.0-s[1]))*0.5
        end

#************ c_pts[:,3,2] = [2.,0.5]
c_pts[1,3,2] = function(s) 2. end
c_pts[2,3,2] = function(s)
            s[1]*0.5
        end

#************ c_pts[:,1,3] = [0.,1.0]
c_pts[1,1,3] = function(s) 0. end
c_pts[2,1,3] = function(s) 1. end

#************ c_pts[:,2,3] = [1.,1.0]
c_pts[1,2,3] = function(s) 1. end
c_pts[2,2,3] = function(s)
            (s[1]+0.5*(1.0-s[1]))*1.0
        end

#************ c_pts[:,3,3] = [2.,1.0]
c_pts[1,3,3] = function(s) 2. end
c_pts[2,3,3] = function(s)
    s[1]*1.0
end

#=
#******************** weights = Array[[1., 1., 1.],[1., 1., 1.]]
weights[1][1] = function(s) 1. end
weights[1][2] = function(s) 1. end
weights[1][3] = function(s) 1. end

#******************** weights = Array[[1., 1., 1.],[1., 1., 1.]]
weights[2][1] = function(s) 1. end
weights[2][2] = function(s) 1. end
weights[2][3] = function(s) 1. end
=#



self = nothing


using PyPlot; plt = PyPlot;

for (k,s_design_val) = enumerate(1.0:-0.02:0.8)

    s_design = [s_design_val]
    @show s_design

    global self = Nurbs(dim_p,
                 dim_s,
                 p,
                 knot_v,
                 span,
                 c_pts,
                 weights;
                 s=s_design)

    RefineMesh(self,[3,2])

    res = [50,25]
    X,t,h = draw(self,res)
    println( "size(X): ", size(X) )
    plt.figure()
    plt.scatter(X[1,:],X[2,:],color="b")
    plt.scatter(self.control_pts[1,:],self.control_pts[2,:],color="r")
    plt.xlim(-0.1,2.1)
    plt.ylim(-0.5,1.5)
    plt.title("Geometry")
    plt.savefig("plots_ignore/plot_exBegin_"*lpad(string(k),2,"0")*".pdf")
    plt.close()

end

@show Jacobian_ds(self,[1,1],[0.3,0.4],1)
@show domain_dXdsdxi(self,[1,1],[0.3,0.4],1)

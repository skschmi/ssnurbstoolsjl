

include("../dualbasis.jl")
using .SSDualBasis
import .SSDualBasis.computeDualBasis


include("../nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.del_basis_dRdxi
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.bernstein_basis
import .SSNurbsTools.del_bernstein_basis
import .SSNurbsTools.outputObj
import .SSNurbsTools.InsertKnot
import .SSNurbsTools.nurbs_basis
import .SSNurbsTools.e_a_xi_for_gb_t

using PyPlot; plt = PyPlot;
plt.figure()
degree = 4
for basis_index = 1:degree+1

	function input_basis( a::Int64, xi::Float64 )
		return SSNurbsTools.bernstein_basis( a, degree, xi; dmin=-1.0, dmax=1.0 )
	end
	dual_coeffs = SSDualBasis.computeDualBasis( input_basis, degree, degree+1, basis_index )
	# Plot the result.
	x = collect(-1.0:0.01:1.0)
	y = zeros(length(x))
	for a = 1:degree+1
		y[:] += [ dual_coeffs[a]*input_basis( a, x[i] ) for i = 1:length(x) ][:]
	end
	plt.plot(x,y)
	plt.title("Bernstein dual basis, degree="*string(degree))
end


###

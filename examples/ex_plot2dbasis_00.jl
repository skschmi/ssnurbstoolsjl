include("../dualbasis.jl")
using .SSDualBasis
import .SSDualBasis.computeDualBasis


include("../nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.del_basis_dRdxi
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.bernstein_basis
import .SSNurbsTools.del_bernstein_basis
import .SSNurbsTools.outputObj
import .SSNurbsTools.InsertKnot
import .SSNurbsTools.nurbs_basis
import .SSNurbsTools.e_a_xi_for_gb_t

using PyPlot; plt = PyPlot;

# Making a flat patch
dim_p = 2
dim_s = 3
p = [2,3]
n = [3,4]
spans = n .- 1
knot_v = Array[[ 0.,   0, 0, 1, 1,   1 ],[ 0.,   0, 0, 0, 1, 1, 1,   1 ]]

weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1., 1., 1.])
weights[2] = Array{Float64}([1., 1., 1., 1.])

c_pts = zeros(dim_s,n[1],n[2])

## This is the actual geometry data (comment this out and use the field data below if you want to do that).
c_pts[:,1,1] = [0.0, 0.0, 0.0]
c_pts[:,2,1] = [1.0, 0.0, 0.0]
c_pts[:,3,1] = [2.0, 0.0, 0.0]

c_pts[:,1,2] = [0.0, 1.0, 0.0]
c_pts[:,2,2] = [1.0, 1.0, 0.0]
c_pts[:,3,2] = [2.0, 1.0, 0.0]

c_pts[:,1,3] = [0.0, 2.0, 0.0]
c_pts[:,2,3] = [1.0, 2.0, 0.0]
c_pts[:,3,3] = [2.0, 2.0, 0.0]

c_pts[:,1,4] = [0.0, 3.0, 0.0]
c_pts[:,2,4] = [1.0, 3.0, 0.0]
c_pts[:,3,4] = [2.0, 3.0, 0.0]

patch = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)

res_to_draw = [60,60]
X,t,h = draw(patch,res_to_draw)
# plt.figure()
# plt.scatter3D(X[1,:,:],X[2,:,:],X[3,:,:])
# plt.scatter3D(patch.control_pts[1,:,:],patch.control_pts[2,:,:],patch.control_pts[3,:,:],color="r")
# plt.xlabel("x")
# plt.ylabel("y")
# plt.zlabel("z");
# plt.title("Surface")

let
	local t0to1
	local tneg1to1
	local BASISVALS
	local plot_e
	local plot_a
	t0to1 = copy(t)
	tneg1to1 =  (t .* 2.0) .- 1;
	plt.figure()
	BASISVALS = zeros( size(t)[2] );
	plot_e = [1,1] #which element to plot the basis on
	plot_a = [2,1] #which basis on that element to plot
	for i = 1:size(t)[2]
		BASISVALS[i] = basis( patch, plot_e, plot_a, tneg1to1[:,i] )
	end
	plt.scatter3D( t0to1[1,:], t0to1[2,:], BASISVALS )
	plt.title("UNDER CONSTRUCTION 1")
	#outputObj(patch,"plots_ignore/patch.obj",res_to_draw)
end

for degree in p
	let
		local t
		local t0to1
		local tneg1to1
		t = collect( range(0.0,stop=1.0,length=50) )
		plt.figure()
		for plt_i = 1:degree+1
			BASISVALS = zeros( size(t) );
			for i = 1:length(t)
				BASISVALS[i] = bernstein_basis( plt_i, degree, t[i] )
			end
			plt.plot( t, BASISVALS )
		end
		plt.title("UNDER CONSTRUCTION degree="*string(degree))
		#outputObj(patch,"plots_ignore/patch.obj",res_to_draw)
	end
end


let
	local t
	local t0to1
	t = collect( range(-1.0,stop=1.0,length=1000) )
	plt.figure()
	BASISVALS = zeros( size(t) );

	for i = 1:length(t)
		if( t[i] < 0 )
			BASISVALS[i] = t[i]+1.0;
		else
			BASISVALS[i] = (2*t[i]-1)^2
		end
	end
	plt.plot( t, BASISVALS )
	#plt.title("UNDER CONSTRUCTION degree="*string(degree))
	#outputObj(patch,"plots_ignore/patch.obj",res_to_draw)
end

# Build plots that show a basic 2d U-Spline (B-Spline) geometry,
# Show the basis functions on the boundary.

include("../ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts

include("../dualbasis.jl")
using .SSDualBasis
import .SSDualBasis.computeDualBasis
import .SSDualBasis.computeBSplineDualBasis

using .SSDualBasis.SSNurbsTools
import .SSDualBasis.SSNurbsTools.Nurbs
import .SSDualBasis.SSNurbsTools.draw
import .SSDualBasis.SSNurbsTools.basis
import .SSDualBasis.SSNurbsTools.del_basis_dRdxi
import .SSDualBasis.SSNurbsTools.Jacobian
import .SSDualBasis.SSNurbsTools.e_xi_for_t
import .SSDualBasis.SSNurbsTools.domain_X
import .SSDualBasis.SSNurbsTools.bernstein_basis
import .SSDualBasis.SSNurbsTools.del_bernstein_basis
import .SSDualBasis.SSNurbsTools.outputObj
import .SSDualBasis.SSNurbsTools.InsertKnot
import .SSDualBasis.SSNurbsTools.nurbs_basis
import .SSDualBasis.SSNurbsTools.e_a_xi_for_gb_t
import .SSDualBasis.SSNurbsTools.ConvertToBernsteinMesh
import .SSDualBasis.SSNurbsTools.RefineMesh
import .SSDualBasis.SSNurbsTools.UniqueKnotArray

using PyPlot; plt = PyPlot;

dim_p = 2
dim_s = 3
p = [3,3]
n = [7,7]
span = n .- 1
knot_v = Array[[0.,  0, 0, 0, 1, 2, 3, 4, 4, 4,   4],[0.,   0, 0, 0, 1, 2, 3, 4, 4, 4,   4]]

c_pts_1d = reshape([ (knot_v[1][2] + knot_v[1][3] + knot_v[1][4])/3,
					(knot_v[1][3] + knot_v[1][4] + knot_v[1][5])/3,
					(knot_v[1][4] + knot_v[1][5] + knot_v[1][6])/3,
					(knot_v[1][5] + knot_v[1][6] + knot_v[1][7])/3,
					(knot_v[1][6] + knot_v[1][7] + knot_v[1][8])/3,
					(knot_v[1][7] + knot_v[1][8] + knot_v[1][9])/3,
					(knot_v[1][8] + knot_v[1][9] + knot_v[1][10])/3 ],1,7)

c_pts = zeros(dim_s,n[1],n[2])

for i = 1:n[1]
	for j = 1:n[2]
		c_pts[:,j,i] = [c_pts_1d[j], c_pts_1d[i], 0.0]
	end
end

weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1., 1., 1., 1., 1., 1., 1.])
weights[2] = Array{Float64}([1., 1., 1., 1., 1., 1., 1.])
self = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)


res_to_draw = [30,30]
X,t,h = draw(self,res_to_draw)

fig = plt.figure()
ax = plt.Axes3D(fig)
plt.axis("off")
# plot the surface
#ax[:scatter3D](X[1,:,:],X[2,:,:],X[3,:,:])
#ax[:scatter3D](self.control_pts[1,:,:],self.control_pts[2,:,:],self.control_pts[3,:,:],color="r")

# plot the element boundaries
uniqueknots = [ UniqueKnotArray(self.knot_v[1]), UniqueKnotArray(self.knot_v[2]) ]
for i = 1:self.nbezcurves[1]+1
	ax[:plot3D]( [ uniqueknots[1][i], uniqueknots[1][i] ], [ minimum(uniqueknots[2]), maximum(uniqueknots[2]) ], [0.0, 0.0], color="black" )
end
for i = 1:self.nbezcurves[1]+1
	ax[:plot3D]([ minimum(uniqueknots[1]), maximum(uniqueknots[1]) ], [ uniqueknots[2][i], uniqueknots[2][i] ], [0.0, 0.0], color="black" )
end


# The global b-spline basis functions
bsplbasisf = function(gb::Array{Int64},t::Array{Float64})
		Bval = 0.0
		e,xi = e_xi_for_t(self,t)
		element_a_for_gb = zeros(Int64,2)
		element_a_for_gb[1] = self.GBEBI[1][e[1],gb[1]]
		element_a_for_gb[2] = self.GBEBI[2][e[2],gb[2]]
		if( minimum(element_a_for_gb) > 0 )
			Bval += basis(self, e, element_a_for_gb, xi)
		end
		return Bval
	end

#val = bsplbasisf([1,1],[0.3,0.2])
#@show(val)

# plot basis functions on the y=0 boundary
begin
	h = 0.01
	for basis_j = 1:n[1]
		x = minimum(uniqueknots[1]):h:maximum(uniqueknots[1])
		y = zeros(length(x))
		basisfvals = zeros(length(x))
		for i = 1:length(x)
			basisfvals[i] = bsplbasisf([basis_j,1],[x[i],y[i]])
		end
		ax[:plot3D](x, y, basisfvals )
	end
	for basis_j = 1:n[2]
		y = minimum(uniqueknots[2]):h:maximum(uniqueknots[2])
		x = ones(length(y)) * maximum(uniqueknots[1])
		basisfvals = zeros(length(y))
		for i = 1:length(y)
			basisfvals[i] = bsplbasisf([n[2],basis_j],[x[i],y[i]])
		end
		ax[:plot3D](x, y, basisfvals )
	end
end

plt.zlim([0,1.2])

plt.xlabel("x")
plt.ylabel("y")
plt.zlabel("z");
plt.title("Surface")

# Build plots that show a basic 2d U-Spline (B-Spline) geometry,
# Show the basis functions on the boundary.

include("../ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts

include("../dualbasis.jl")
using .SSDualBasis
import .SSDualBasis.computeDualBasis
import .SSDualBasis.computeBSplineDualBasis
import .SSDualBasis.computeBSplineDualBasisGeneral

using .SSDualBasis.SSNurbsTools
import .SSDualBasis.SSNurbsTools.Nurbs
import .SSDualBasis.SSNurbsTools.draw
import .SSDualBasis.SSNurbsTools.basis
import .SSDualBasis.SSNurbsTools.del_basis_dRdxi
import .SSDualBasis.SSNurbsTools.Jacobian
import .SSDualBasis.SSNurbsTools.e_xi_for_t
import .SSDualBasis.SSNurbsTools.domain_X
import .SSDualBasis.SSNurbsTools.bernstein_basis
import .SSDualBasis.SSNurbsTools.del_bernstein_basis
import .SSDualBasis.SSNurbsTools.outputObj
import .SSDualBasis.SSNurbsTools.InsertKnot
import .SSDualBasis.SSNurbsTools.nurbs_basis
import .SSDualBasis.SSNurbsTools.e_a_xi_for_gb_t
import .SSDualBasis.SSNurbsTools.ConvertToBernsteinMesh
import .SSDualBasis.SSNurbsTools.RefineMesh
import .SSDualBasis.SSNurbsTools.UniqueKnotArray

using PyPlot; plt = PyPlot;

dim_p = 2
dim_s = 3
p = [3,3]
n = [7,7]
span = n .- 1
knot_v = Array[[0.,  0, 0, 0, 1, 2, 3, 4, 4, 4,   4],[0.,   0, 0, 0, 1, 2, 3, 4, 4, 4,   4]]

c_pts_1d = reshape([ (knot_v[1][2] + knot_v[1][3] + knot_v[1][4])/3,
					(knot_v[1][3] + knot_v[1][4] + knot_v[1][5])/3,
					(knot_v[1][4] + knot_v[1][5] + knot_v[1][6])/3,
					(knot_v[1][5] + knot_v[1][6] + knot_v[1][7])/3,
					(knot_v[1][6] + knot_v[1][7] + knot_v[1][8])/3,
					(knot_v[1][7] + knot_v[1][8] + knot_v[1][9])/3,
					(knot_v[1][8] + knot_v[1][9] + knot_v[1][10])/3 ],1,7)

c_pts = zeros(dim_s,n[1],n[2])

for i = 1:n[1]
	for j = 1:n[2]
		c_pts[:,j,i] = [c_pts_1d[j], c_pts_1d[i], 0.0]
	end
end

weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1., 1., 1., 1., 1., 1., 1.])
weights[2] = Array{Float64}([1., 1., 1., 1., 1., 1., 1.])
self = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)

gb = Array{Int64}([2,1])
support = Array{Array{Int64}}([[1,2],[1]])
computeBSplineDualBasisGeneral( self, gb, support )

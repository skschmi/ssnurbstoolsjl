

include("../ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts

include("../dualbasis.jl")
using .SSDualBasis
import .SSDualBasis.computeDualBasis
import .SSDualBasis.computeBSplineDualBasis

using .SSDualBasis.SSNurbsTools
import .SSDualBasis.SSNurbsTools.Nurbs
import .SSDualBasis.SSNurbsTools.draw
import .SSDualBasis.SSNurbsTools.basis
import .SSDualBasis.SSNurbsTools.del_basis_dRdxi
import .SSDualBasis.SSNurbsTools.Jacobian
import .SSDualBasis.SSNurbsTools.e_xi_for_t
import .SSDualBasis.SSNurbsTools.domain_X
import .SSDualBasis.SSNurbsTools.bernstein_basis
import .SSDualBasis.SSNurbsTools.del_bernstein_basis
import .SSDualBasis.SSNurbsTools.outputObj
import .SSDualBasis.SSNurbsTools.InsertKnot
import .SSDualBasis.SSNurbsTools.nurbs_basis
import .SSDualBasis.SSNurbsTools.e_a_xi_for_gb_t
import .SSDualBasis.SSNurbsTools.ConvertToBernsteinMesh



dim_p = 1
dim_s = 1
p = [3]
n = [7]
spans = n .- 1
knot_v = Array[[0., 0, 0, 0, 1, 2, 3, 4, 4, 4, 4]]
c_pts = reshape([ (knot_v[1][2] + knot_v[1][3] + knot_v[1][4])/3,
                  (knot_v[1][3] + knot_v[1][4] + knot_v[1][5])/3,
                  (knot_v[1][4] + knot_v[1][5] + knot_v[1][6])/3,
                  (knot_v[1][5] + knot_v[1][6] + knot_v[1][7])/3,
                  (knot_v[1][6] + knot_v[1][7] + knot_v[1][8])/3,
                  (knot_v[1][7] + knot_v[1][8] + knot_v[1][9])/3,
                  (knot_v[1][8] + knot_v[1][9] + knot_v[1][10])/3 ],1,7)
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.,1.])
self = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)
bern = Nurbs(self)
ConvertToBernsteinMesh(bern)

using PyPlot; plt = PyPlot;

# This is the index of the global basis function that we want the dual basis of.
gb_arr = [1,2,3,4,5,6,7]
supports_arr = [ [1], [1,2], [1,2,3], [1,2,3,4], [2,3,4], [3,4], [4] ]

# The global b-spline basis functions
bsplbasisf = function(gb::Int64,t::Float64)
		Bval = 0.0
		e,xi = e_xi_for_t(self,reshape([t],1,1))
		e = e[1]
		xi = xi[1]
		element_a_for_gb = self.GBEBI[1][e,gb][1]
		if( element_a_for_gb > 0 )
			Bval += basis(self, [e], [element_a_for_gb], [xi])
		end
		return Bval
	end

# Plotting the regular basis function
figure_size = (8, 2)
plt.figure(figsize=figure_size)
for gb in gb_arr
	t = collect(minimum(knot_v[1]):0.01:maximum(knot_v[1]))
	B = zeros(length(t))
	for i = 1:length(t)
		B[i] = bsplbasisf(gb,t[i])
	end
	plt.plot(t,B)
	plt.title("B-spline basis")
end

## TODO: What I want to do is see if we can compute a dual basis the regular way directly from the b-spline basis (without regards to the bernstein).

using PyPlot; plt = PyPlot;
plt.figure()
degree = 3
num_bases = self.spans[1]+1
for basis_index = 1:num_bases
	function input_basis( a::Int64, t::Float64 )
		return bsplbasisf( a, t )
	end
	dual_coeffs = SSDualBasis.computeDualBasis( input_basis, degree, num_bases, basis_index, param_min=minimum(knot_v[1]), param_max=maximum(knot_v[1]) )
	# Plot the result.
	x = collect(minimum(knot_v[1]):0.01:maximum(knot_v[1]))
	y = zeros(length(x))
	for a = 1:num_bases
		y[:] += [ dual_coeffs[a]*input_basis( a, x[i] ) for i = 1:length(x) ][:]
	end
	plt.plot(x,y)
	plt.title("B-spline dual basis, degree="*string(degree))
end




###

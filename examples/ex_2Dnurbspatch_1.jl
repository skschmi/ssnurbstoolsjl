
include("../nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.del_basis_dRdxi
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.bernstein_basis
import .SSNurbsTools.del_bernstein_basis
import .SSNurbsTools.outputObj
import .SSNurbsTools.InsertKnot
import .SSNurbsTools.nurbs_basis
import .SSNurbsTools.e_a_xi_for_gb_t

using PyPlot; plt = PyPlot;

dim_p = 2
dim_s = 3
p = [3,3]
n = [4,4]
span = n .- 1
knot_v = Array[[0., 0, 0, 0, 1, 1, 1, 1],[0., 0, 0, 0, 1, 1, 1, 1]]

c_pts = zeros(dim_s,n[1],n[2])

c_pts[:,1,1] = [0.0,0.1,0.1]
c_pts[:,2,1] = [1.0,0.2,0.4]
c_pts[:,3,1] = [2.0,0.3,0.3]
c_pts[:,4,1] = [3.0,0.1,0.2]

c_pts[:,1,2] = [0.0,1.0,0.3]
c_pts[:,2,2] = [1.0,1.0,0.9]
c_pts[:,3,2] = [2.0,1.0,0.8]
c_pts[:,4,2] = [3.0,1.0,0.7]

c_pts[:,1,3] = [0.0,2.0,0.7]
c_pts[:,2,3] = [1.0,2.0,0.6]
c_pts[:,3,3] = [2.0,2.0,0.2]
c_pts[:,4,3] = [3.0,2.0,0.3]

c_pts[:,1,4] = [0.0,3.5,0.1]
c_pts[:,2,4] = [1.0,3.4,0.2]
c_pts[:,3,4] = [2.0,3.3,0.4]
c_pts[:,4,4] = [3.0,3.4,0.3]

weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1., 1., 1., 1.])
weights[2] = Array{Float64}([1., 1., 1., 1.])
patch = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)
InsertKnot(patch,1,0.25)
InsertKnot(patch,1,0.75)
InsertKnot(patch,2,0.25)
InsertKnot(patch,2,0.75)
## afterwords, the knots look like this:   0   0 0 0 0.25 0.75 1 1 1   1
## with six total basis functions

res_to_draw = [30,30]
X,t,h = draw(patch,res_to_draw)
plt.figure()
plt.scatter3D(X[1,:,:],X[2,:,:],X[3,:,:])
plt.scatter3D(patch.control_pts[1,:,:],patch.control_pts[2,:,:],patch.control_pts[3,:,:],color="r")
plt.xlabel("x")
plt.ylabel("y")
plt.zlabel("z");
plt.title("Surface")
outputObj(patch,"plots_ignore/patch.obj",res_to_draw)


function basis_1d_t1dir(nurbs::Nurbs,a::Int64,t::Float64)
    e,a,xi = e_a_xi_for_gb_t(patch,[a,1],[t,0.0])
    result = nothing
    if(a[1] != 0 && a[2] != 0)
        result = nurbs_basis(a[1],nurbs.degree[1],nurbs.C_el[e[1],e[2]][1],nurbs.weights_el[e[1],e[2]][1],xi[1])
    else
        result = 0.0
    end
    return result
end

plt.figure()
tvals = collect(0.0:0.01:1.0)
basvals = nothing
for basis_i = 1:6
    global basvals = Array{Float64}([basis_1d_t1dir(patch,basis_i,tvals[i]) for i=1:length(tvals)])
    plt.plot(tvals,basvals,color="b")
end



e,a,xi = e_a_xi_for_gb_t(patch,[1,1],[0.1,0.1])
@show basis(patch,e,a,xi)

println("")
println("Basis")
println( replace( string(basis(patch,[3,3],[0.21,-0.03])), ";" => "\n") )

DelBas_dxi1 = Array{Float64}( [ del_basis_dRdxi(patch,[3,3],[a1,a2],[0.21,-0.03])[1] for a1=1:(patch.degree[1]+1), a2=1:(patch.degree[2]+1)])
DelBas_dxi2 = Array{Float64}( [ del_basis_dRdxi(patch,[3,3],[a1,a2],[0.21,-0.03])[2] for a1=1:(patch.degree[1]+1), a2=1:(patch.degree[2]+1)])

println("")
println("Basis dRdxi1")
println( replace( string(DelBas_dxi1), ";" => "\n" ) )

println("")
println("Basis dRdxi2")
println( replace( string(DelBas_dxi2), ";" => "\n" ) )

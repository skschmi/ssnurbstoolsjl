###
### 1D dual-basis projection refinement study
###
### (1) Compute the dual bases of a 1d b-spline curve
### (2) Project onto a sine-wave function
### (3) Compute the L2 error.
### (4) Refine the b-spline (divide each cell in two)
### (5) Repeat steps 1-4.


include("../ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts

include("../dualbasis.jl")
using .SSDualBasis
import .SSDualBasis.computeDualBasis
import .SSDualBasis.computeBSplineDualBasis

using .SSDualBasis.SSNurbsTools
import .SSDualBasis.SSNurbsTools.Nurbs
import .SSDualBasis.SSNurbsTools.draw
import .SSDualBasis.SSNurbsTools.basis
import .SSDualBasis.SSNurbsTools.del_basis_dRdxi
import .SSDualBasis.SSNurbsTools.Jacobian
import .SSDualBasis.SSNurbsTools.e_xi_for_t
import .SSDualBasis.SSNurbsTools.domain_X
import .SSDualBasis.SSNurbsTools.bernstein_basis
import .SSDualBasis.SSNurbsTools.del_bernstein_basis
import .SSDualBasis.SSNurbsTools.outputObj
import .SSDualBasis.SSNurbsTools.InsertKnot
import .SSDualBasis.SSNurbsTools.nurbs_basis
import .SSDualBasis.SSNurbsTools.e_a_xi_for_gb_t
import .SSDualBasis.SSNurbsTools.ConvertToBernsteinMesh
import .SSDualBasis.SSNurbsTools.RefineMesh

## Building the original pre-refinement b-spline
dim_p = 1
dim_s = 1
p = [3]
n = [7]
spans = n .- 1
knot_v = Array[[0., 0, 0, 0, 1, 2, 3, 4, 4, 4, 4]]
c_pts = reshape([ (knot_v[1][2] + knot_v[1][3] + knot_v[1][4])/3,
                  (knot_v[1][3] + knot_v[1][4] + knot_v[1][5])/3,
                  (knot_v[1][4] + knot_v[1][5] + knot_v[1][6])/3,
                  (knot_v[1][5] + knot_v[1][6] + knot_v[1][7])/3,
                  (knot_v[1][6] + knot_v[1][7] + knot_v[1][8])/3,
                  (knot_v[1][7] + knot_v[1][8] + knot_v[1][9])/3,
                  (knot_v[1][8] + knot_v[1][9] + knot_v[1][10])/3 ],1,7)
weights = Array{Array{Float64}}(undef,1)
weights[1] = Array{Float64}([1.,1.,1.,1.,1.,1.,1.])
self = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)
orig_n_elem = self.nbezcurves[1]


using PyPlot; plt = PyPlot;

# Compare the exact to the projected function.
num_refinements = 0
max_num_refinements = 3
plot_this_one = 3
while( num_refinements <= max_num_refinements )

	# Deal with refinement
	if(num_refinements > 0)
		RefineMesh( self, [ (2^num_refinements) * orig_n_elem] )
	end
	println( "num cells: ", self.nbezcurves[1], "      num_refinements: ", num_refinements )


	# This is the index of the global basis function that we want the dual basis of.
	gb_arr = collect(1:self.spans[1]+1)

	## compute the supports array
	#supports_arr = [ [1], [1,2], [1,2,3], [1,2,3,4], [2,3,4], [3,4], [4] ]
	supports_arr = Array{Array{Int64}}(undef,0)
	for j = 1:size(self.GBEBI[1])[2]
		push!( supports_arr, findall( x->(x > 0), self.GBEBI[1][:,j] ) )
	end
	println( "gb_arr: ", gb_arr )
	println( "supports_arr: ", supports_arr )


	# The global b-spline basis functions
	bsplbasisf = function(gb::Int64,t::Float64)
			Bval = 0.0
			e,xi = e_xi_for_t(self,reshape([t],1,1))
			e = e[1]
			xi = xi[1]
			element_a_for_gb = self.GBEBI[1][e,gb][1]
			if( element_a_for_gb > 0 )
				Bval += basis(self, [e], [element_a_for_gb], [xi])
			end
			return Bval
		end
	##

	# Compute the dual basis coeffs
	COEFFS_ARR = Array{Any}(undef,length(gb_arr))
	for i = 1:length(gb_arr)
		gb = gb_arr[i]
		support = supports_arr[i]
		## Now compute the dual basis for one of the global basis functions of the B-Spline.
		COEFFS,Amat,dvec,bvec = computeBSplineDualBasis( self, gb, support )
		COEFFS_ARR[i] = COEFFS
	end

	# Assembling the dual basis function from the bernstein coeffs
	dualbasisf = function(COEFFS::Array{Float64,2},t::Float64)
			Bval = 0.0
			for e0 = 1:self.nbezcurves[1]
				for a = 1:(self.degree[1]+1)
					e,xi = e_xi_for_t(self,reshape([t],1,1))
					e = e[1]
					xi = xi[1]
					if( e == e0 )
						Bval += COEFFS[a,e0]*bernstein_basis(a,self.degree[1],xi,dmin=-1.,dmax=1.)
					end
				end
			end
			return Bval
		end
	##


	# Projecting a sine-curve onto the basis
	begin
		# The exact sin function
		sinf = function(t::Float64)
				return sin.(0.5*pi*t)
			end
		##

		# Computing the b-spline coeffs of the projected function.
		bspl_coeffs = zeros( length(gb_arr) )
		begin
			####
			#### NOTE: WRONG
			####
			#### We're doing the integration wrong right now --- it's a piecewise discontinuous function
			#### so we should only use the gaussian quadrature over each cell domain, not over the whole thing.
			####
			GQT1D = SSGaussQuad.GaussQuadTable( 20*self.nbezcurves[1]*NumGaussPts(self.degree[1]) )
			for i = 1:length(gb_arr)
				gb = gb_arr[i]
				support = supports_arr[i]

				# Grab the bernstein coefficients for this dual basis out of the array (computed above)
				COEFFS = COEFFS_ARR[i]

				# The jacobian of the integration
				jacobianf = function(params)
						# dx/dxi
						return (maximum(knot_v[1])-minimum(knot_v[1]))/2.0
					end
				# The function under the integral.
				f = function(params)
						xi = params[1]  # xi goes from -1 to 1.
						t = minimum(knot_v[1]) + ((xi+1.0)/2.0) * (maximum(knot_v[1]) - minimum(knot_v[1]))
						return dualbasisf(COEFFS,t) * sinf(t)
					end
				# Computing the coefficient for this b-spline basis function.
				bspl_coeffs[gb] = GaussQuadIntegrate(f,GQT1D,jacobianf)
			end
		end

		# The new projected function.
		projectedf = function(t::Float64)
				fval = 0.0
				for i = 1:length(gb_arr)
					gb = gb_arr[i]
					fval += bspl_coeffs[gb] * bsplbasisf(gb,t)
				end
				return fval
			end
		##

		if( true || num_refinements == plot_this_one )

			# Plot the original sin function
			figure_size = (8, 2)
			plt.figure(figsize=figure_size)
			t = collect(minimum(knot_v[1]):0.005:maximum(knot_v[1]))
			sinf = function(t::Float64)
					return sin.(0.5*pi*t)
				end
			sinf_arr = Array{Float64}([sinf(t[i]) for i=1:length(t)])
			plt.plot(t,sinf_arr)

			# Plot the new projected function.
			t = collect(minimum(knot_v[1]):0.005:maximum(knot_v[1]))
			PROJF = zeros(length(t))
			for i = 1:length(t)
				PROJF[i] = projectedf(t[i])
			end
			plt.plot(t,PROJF)
			plt.title("Projected function")
			plt.legend(["Exact","Projected"])

		end

		println( "    projectedf(0.3): ", projectedf(0.3) )
		println( "    sinf(0.3): ", sinf(0.3) )

		global num_refinements += 1
	end
end


include("../nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.del_basis_dRdxi
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.bernstein_basis
import .SSNurbsTools.del_bernstein_basis
import .SSNurbsTools.outputObj
import .SSNurbsTools.InsertKnot
import .SSNurbsTools.nurbs_basis
import .SSNurbsTools.e_a_xi_for_gb_t

include("../ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts
using LinearAlgebra
import .LinearAlgebra.det

degree = 12
gqt = GaussQuadTable( NumGaussPts( degree ) )
#@show( gqt )
square_pts = zeros(2,gqt.count,gqt.count)
square_wts = zeros(gqt.count,gqt.count)
for i = 1:gqt.count
	for j = 1:gqt.count
			square_pts[1,i,j] = gqt.pts[i]
			square_pts[2,i,j] = gqt.pts[j]
			square_wts[i,j] = gqt.wts[i] * gqt.wts[j]
	end
end

dim_p = 2
dim_s = 2
p = [1,1]
n = [2,2]
spans = n .- 1
knot_v = Array[[0., 0, 1, 1],[0., 0, 1, 1]]
weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1., 1.])
weights[2] = Array{Float64}([1., 1.])
c_pts = zeros(dim_s,n[1],n[2])
c_pts[:,1,1] = [0.0, 0.0]
c_pts[:,2,1] = [1.0, 0.0]
c_pts[:,1,2] = [0.0, 1.0]
c_pts[:,2,2] = [0.5, 0.5]
self = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights)

triangle_pts = zeros(2,gqt.count,gqt.count)
triangle_wts = zeros(gqt.count,gqt.count)
function jdet(s,t)
	J = SSNurbsTools.Jacobian(self,[1,1],[s,t])
	return det(J)
end

for i = 1:gqt.count
	for j = 1:gqt.count
		newpt = domain_X( self, [1,1], reshape( square_pts[:,i,j], 2, 1 ) )
		triangle_pts[1,i,j] = newpt[1]
		triangle_pts[2,i,j] = newpt[2]
		triangle_wts[i,j] = square_wts[i,j] * jdet( square_pts[1,i,j], square_pts[2,i,j] )
	end
end

## Printing out all the gauss points and weights
println( "" )
println( "" )
global weights_sum = 0.0
for i = 1:gqt.count
	for j = 1:gqt.count
		println( "{ ", triangle_pts[1,i,j], ",    ", triangle_pts[2,i,j], "  },                       ", triangle_wts[i,j], " / 0.5 * 1.73205080756888, "  )
		global weights_sum += triangle_wts[i,j]
	end
end
println( "" )
println( "" )
println( "Weights sum: ", weights_sum )


include("../nurbstools.jl")
include("../ssindexingtools/indexingtools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.del_basis_dRdxi
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.domain_X
import .SSNurbsTools.bernstein_basis
import .SSNurbsTools.del_bernstein_basis
import .SSNurbsTools.outputObj
import .SSNurbsTools.InsertKnot
import .SSNurbsTools.nurbs_basis
import .SSNurbsTools.e_a_xi_for_gb_t

using .SSIndexingTools
import .SSIndexingTools.IndexForRowCol
import .SSIndexingTools.RowColForIndex

using PyPlot; plt = PyPlot;
using Printf

# Making a quarter cylinder
dim_p = 2
dim_s = 3
p = [2,2]
n = [3,3]
span = n .- 1
knot_v = Array[[0., 0, 0, 1, 1, 1],[0., 0, 0, 1, 1, 1]]

weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1., 1/sqrt(2), 1.])
#weights[2] = Array{Float64}([1., 1/sqrt(2), 1.])
weights[2] = Array{Float64}([1., 1., 1.])

c_pts = zeros(dim_s,n[1],n[2])

## This is the actual geometry data (comment this out and use the field data below if you want to do that).
c_pts[:,1,1] = [0.0, 0.0, 0.0]
c_pts[:,2,1] = [1.0, 0.0, 0.0]
c_pts[:,3,1] = [1.0, 0.0, 1.0]

c_pts[:,1,2] = [0.0, 1.0, 0.0]
c_pts[:,2,2] = [1.0, 1.0, 0.0]
c_pts[:,3,2] = [1.0, 1.0, 1.0]

c_pts[:,1,3] = [0.0, 2.0, 0.0]
c_pts[:,2,3] = [1.0, 2.0, 0.0]
c_pts[:,3,3] = [1.0, 2.0, 1.0]



# This is field data, but for a moment I'll treat it as control point values. (if it's uncommented)
#=
c_pts[:,1,1] = [0.0, 9.0, 0.0]
c_pts[:,2,1] = [1.0, 8.0, 0.0]
c_pts[:,3,1] = [2.0, 4.0, 0.0]

c_pts[:,1,2] = [0.0, 90.0, 0.0]
c_pts[:,2,2] = [2.0, 80.0, 0.0]
c_pts[:,3,2] = [4.0, 40.0, 0.0]

c_pts[:,1,3] = [0.0, 900.0, 0.0]
c_pts[:,2,3] = [5.0, 800.0, 0.0]
c_pts[:,3,3] = [10.0,400.0, 0.0]
=#



patch = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)

InsertKnot(patch,1,0.5)
InsertKnot(patch,2,0.5)

@show patch.control_pts
@show patch.weights

res_to_draw = [30,30]
X,t,h = draw(patch,res_to_draw)
plt.figure()
plt.scatter3D(X[1,:,:],X[2,:,:],X[3,:,:])
plt.scatter3D(patch.control_pts[1,:,:],patch.control_pts[2,:,:],patch.control_pts[3,:,:],color="r")
plt.xlabel("x")
plt.ylabel("y")
plt.zlabel("z");
plt.title("Surface")


basis_order = [1, 2, 3, 16, 4, 5, 6, 13, 7, 8, 9, 14, 10, 11, 12, 15]

lines_heads = ["control_pts <<  ","                ","                "]
open("outcppstyle_01.txt", "w") do f
    for dim_i = 1:3
        write(f,lines_heads[dim_i])
        for j = 1:size(patch.control_pts)[3]
            for i = 1:size(patch.control_pts)[2]
                nrows = size(patch.control_pts)[2]
                old_lin_i = IndexForRowCol(i,j,nrows)
                new_lin_i = findall(x -> x == old_lin_i,basis_order)[1]
                new_i,new_j = RowColForIndex(new_lin_i,nrows)
                @printf(f,"%0.12f,   ",patch.control_pts[dim_i,new_i,new_j])
            end
        end
        write(f,"\n")
    end
end

tensor_prod_weights = patch.weights[1]*patch.weights[2]'
open("outcppstyle_02.txt", "w") do f
    write(f,"nodal_wts << ")
    for j = 1:size(tensor_prod_weights)[2]
        for i = 1:size(tensor_prod_weights)[1]
            nrows = size(tensor_prod_weights)[1]
            old_lin_i = IndexForRowCol(i,j,nrows)
            new_lin_i = findall(x -> x == old_lin_i,basis_order)[1]
            new_i,new_j = RowColForIndex(new_lin_i,nrows)
            @printf(f,"%0.12f,   ",tensor_prod_weights[new_i,new_j])
        end
    end
    write(f,"\n")
end

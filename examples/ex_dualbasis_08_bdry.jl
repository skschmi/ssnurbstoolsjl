# Build plots that show a basic 2d U-Spline (B-Spline) geometry,
# Show the basis functions on the boundary.

include("../ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts

include("../dualbasis.jl")
using .SSDualBasis
import .SSDualBasis.computeDualBasis
import .SSDualBasis.computeBSplineDualBasis

using .SSDualBasis.SSNurbsTools
import .SSDualBasis.SSNurbsTools.Nurbs
import .SSDualBasis.SSNurbsTools.draw
import .SSDualBasis.SSNurbsTools.basis
import .SSDualBasis.SSNurbsTools.del_basis_dRdxi
import .SSDualBasis.SSNurbsTools.Jacobian
import .SSDualBasis.SSNurbsTools.e_xi_for_t
import .SSDualBasis.SSNurbsTools.domain_X
import .SSDualBasis.SSNurbsTools.bernstein_basis
import .SSDualBasis.SSNurbsTools.del_bernstein_basis
import .SSDualBasis.SSNurbsTools.outputObj
import .SSDualBasis.SSNurbsTools.InsertKnot
import .SSDualBasis.SSNurbsTools.nurbs_basis
import .SSDualBasis.SSNurbsTools.e_a_xi_for_gb_t
import .SSDualBasis.SSNurbsTools.ConvertToBernsteinMesh
import .SSDualBasis.SSNurbsTools.RefineMesh
import .SSDualBasis.SSNurbsTools.UniqueKnotArray

using PyPlot; plt = PyPlot;

dim_p = 2
dim_s = 3
p = [3,3]
n = [6,6]
span = n .- 1
knot_v = Array[[0.,  0, 0, 0, 1, 2, 3, 3, 3,  3],[0.,   0, 0, 0, 1, 2, 3, 3, 3,   3]]

c_pts_1d = reshape([ (knot_v[1][2] + knot_v[1][3] + knot_v[1][4])/3,
					(knot_v[1][3] + knot_v[1][4] + knot_v[1][5])/3,
					(knot_v[1][4] + knot_v[1][5] + knot_v[1][6])/3,
					(knot_v[1][5] + knot_v[1][6] + knot_v[1][7])/3,
					(knot_v[1][6] + knot_v[1][7] + knot_v[1][8])/3,
					(knot_v[1][7] + knot_v[1][8] + knot_v[1][9])/3 ],1,6)

c_pts = zeros(dim_s,n[1],n[2])

for i = 1:n[1]
	for j = 1:n[2]
		c_pts[:,j,i] = [c_pts_1d[j], c_pts_1d[i], 0.0]
	end
end

weights = Array{Array{Float64}}(undef,2)
weights[1] = Array{Float64}([1., 1., 1., 1., 1., 1.])
weights[2] = Array{Float64}([1., 1., 1., 1., 1., 1.])
self = Nurbs(dim_p, dim_s, p, knot_v, span, c_pts, weights)


let
	local fig = plt.figure()
	local ax = plt.axes()
	plt.axis("off")
	# plot the element boundaries
	local uniqueknots = [ UniqueKnotArray(self.knot_v[1]), UniqueKnotArray(self.knot_v[2]) ]
	for i = 1:self.nbezcurves[1]+1
		ax[:plot]( [ uniqueknots[1][i], uniqueknots[1][i] ], [ minimum(uniqueknots[2]), maximum(uniqueknots[2]) ], color="black" )
	end
	for i = 1:self.nbezcurves[1]+1
		ax[:plot]([ minimum(uniqueknots[1]), maximum(uniqueknots[1]) ], [ uniqueknots[2][i], uniqueknots[2][i] ], color="black" )
	end
	plt.xlabel("x")
	plt.ylabel("y")
	plt.title("U-Spline 2D Tensor-Product Topology")
	plt.savefig("plots_ignore/bdrybasisplot_2dTPTopology_00.pdf")
end


####### plotting ALL of the global basis functions
let
	local fig = plt.figure()
	local ax = plt.Axes3D(fig)
	plt.axis("off")
	# plot the surface
	#res_to_draw = [30,30]
	#X,t,h = draw(self,res_to_draw)
	#ax[:scatter3D](X[1,:,:],X[2,:,:],X[3,:,:])
	#ax[:scatter3D](self.control_pts[1,:,:],self.control_pts[2,:,:],self.control_pts[3,:,:],color="r")
	# plot the element boundaries
	local uniqueknots = [ UniqueKnotArray(self.knot_v[1]), UniqueKnotArray(self.knot_v[2]) ]
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]( [ uniqueknots[1][i], uniqueknots[1][i] ], [ minimum(uniqueknots[2]), maximum(uniqueknots[2]) ], [0.0, 0.0], color="black" )
	end
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]([ minimum(uniqueknots[1]), maximum(uniqueknots[1]) ], [ uniqueknots[2][i], uniqueknots[2][i] ], [0.0, 0.0], color="black" )
	end
	# The global b-spline basis functions
	bsplbasisf = function(gb::Array{Int64},t::Array{Float64})
			Bval = 0.0
			e,xi = e_xi_for_t(self,t)
			element_a_for_gb = zeros(Int64,2)
			element_a_for_gb[1] = self.GBEBI[1][e[1],gb[1]]
			element_a_for_gb[2] = self.GBEBI[2][e[2],gb[2]]
			if( minimum(element_a_for_gb) > 0 )
				Bval += basis(self, e, element_a_for_gb, xi)
			end
			return Bval
		end
	let
		h = 0.01
		# plot basis functions on the y=0 boundary
		for basis_j = 1:n[1]
			local x = minimum(uniqueknots[1]):h:maximum(uniqueknots[1])
			y = zeros(length(x))
			basisfvals = zeros(length(x))
			for i = 1:length(x)
				basisfvals[i] = bsplbasisf([basis_j,1],[x[i],y[i]])
			end
			ax[:plot3D](x, y, basisfvals )
		end
		# plot basis functions on the x=MAX boundary
		for basis_j = 1:n[2]
			y = minimum(uniqueknots[2]):h:maximum(uniqueknots[2])
			local x = ones(length(y)) * maximum(uniqueknots[1])
			basisfvals = zeros(length(y))
			for i = 1:length(y)
				basisfvals[i] = bsplbasisf([n[2],basis_j],[x[i],y[i]])
			end
			ax[:plot3D](x, y, basisfvals )
		end
	end
	plt.zlim([0,1.2])
	plt.xlabel("x")
	plt.ylabel("y")
	plt.zlabel("z");
	plt.title("U-Spline Basis on Boundary")
	plt.savefig("plots_ignore/bdrybasisplot_bdrybases_00.pdf")
end



####### plotting ALL of the BERNSTEIN basis functions
let
	local fig = plt.figure()
	local ax = plt.Axes3D(fig)
	plt.axis("off")
	# plot the surface
	#res_to_draw = [30,30]
	#X,t,h = draw(self,res_to_draw)
	#ax[:scatter3D](X[1,:,:],X[2,:,:],X[3,:,:])
	#ax[:scatter3D](self.control_pts[1,:,:],self.control_pts[2,:,:],self.control_pts[3,:,:],color="r")
	# plot the element boundaries
	local uniqueknots = [ UniqueKnotArray(self.knot_v[1]), UniqueKnotArray(self.knot_v[2]) ]
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]( [ uniqueknots[1][i], uniqueknots[1][i] ], [ minimum(uniqueknots[2]), maximum(uniqueknots[2]) ], [0.0, 0.0], color="black" )
	end
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]([ minimum(uniqueknots[1]), maximum(uniqueknots[1]) ], [ uniqueknots[2][i], uniqueknots[2][i] ], [0.0, 0.0], color="black" )
	end
	# The global b-spline basis functions
	bsplbasisf = function(gb::Array{Int64},t::Array{Float64})
			Bval = 0.0
			e,xi = e_xi_for_t(self,t)
			element_a_for_gb = zeros(Int64,2)
			element_a_for_gb[1] = self.GBEBI[1][e[1],gb[1]]
			element_a_for_gb[2] = self.GBEBI[2][e[2],gb[2]]
			if( minimum(element_a_for_gb) > 0 )
				Bval += basis(self, e, element_a_for_gb, xi)
			end
			return Bval
		end
	#
	bernsteinbasisf = function(bbi::Array{Int64},t::Array{Float64})
			Bval = 0.0
			e,xi = e_xi_for_t(self,t)
			element_a_for_gb = zeros(Int64,2)
			element_a_for_gb[1] = mod(bbi[1]-1,self.degree[1]+1)+1 #local bernstein basis index in the cell.
			element_a_for_gb[2] = mod(bbi[2]-1,self.degree[2]+1)+1 #local bernstein basis index in the cell.
			if( minimum(element_a_for_gb) > 0 )
				Bval_1 = bernstein_basis(element_a_for_gb[1],self.degree[1],xi[1],dmin=-1.0,dmax=1.0)
				Bval_2 = bernstein_basis(element_a_for_gb[2],self.degree[2],xi[2],dmin=-1.0,dmax=1.0)
				Bval = Bval_1 * Bval_2
			end
			return Bval
		end
	let
		h = 0.0025
		# plot basis functions on the y=0 boundary
		n_bern_1 = self.nbezcurves[1]*(self.degree[1]+1)
		for basis_j = 1:n_bern_1
			local x = minimum(uniqueknots[1]):h:maximum(uniqueknots[1])
			y = zeros(length(x))
			basisfvals = zeros(length(x))
			for i = 1:length(x)
				basisfvals[i] = bernsteinbasisf([basis_j,1],[x[i],y[i]])
			end
			# Set the discontinuous sections to NaN so they don't plot.
			for i = 2:length(basisfvals)
				if( abs(basisfvals[i-1]-basisfvals[i]) > 0.5 )
					basisfvals[i] = NaN
				end
			end
			ax[:plot3D](x, y, basisfvals )
		end
		# plot basis functions on the x=MAX boundary
		n_bern_2 = self.nbezcurves[2]*(self.degree[2]+1)
		for basis_j = 1:n_bern_2
			y = minimum(uniqueknots[2]):h:maximum(uniqueknots[2])
			local x = ones(length(y)) * maximum(uniqueknots[1])
			basisfvals = zeros(length(y))
			for i = 1:length(y)
				basisfvals[i] = bernsteinbasisf([n_bern_2,basis_j],[x[i],y[i]])
			end
			# Set the discontinuous sections to NaN so they don't plot.
			for i = 2:length(basisfvals)
				if( abs(basisfvals[i-1]-basisfvals[i]) > 0.5 )
					basisfvals[i] = NaN
				end
			end
			ax[:plot3D](x, y, basisfvals )
		end
	end
	plt.zlim([0,1.2])
	plt.xlabel("x")
	plt.ylabel("y")
	plt.zlabel("z");
	plt.title("Bernstein Basis on Boundary")
	plt.savefig("plots_ignore/bdrybernsteinplot_bdrybases_00.pdf")
end




####### plotting one of the global basis functions (first of two)
let
	local fig = plt.figure()
	local ax = plt.Axes3D(fig)
	plt.axis("off")
	# plot the surface
	#res_to_draw = [30,30]
	#X,t,h = draw(self,res_to_draw)
	#ax[:scatter3D](X[1,:,:],X[2,:,:],X[3,:,:])
	#ax[:scatter3D](self.control_pts[1,:,:],self.control_pts[2,:,:],self.control_pts[3,:,:],color="r")
	# plot the element boundaries
	local uniqueknots = [ UniqueKnotArray(self.knot_v[1]), UniqueKnotArray(self.knot_v[2]) ]
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]( [ uniqueknots[1][i], uniqueknots[1][i] ], [ minimum(uniqueknots[2]), maximum(uniqueknots[2]) ], [0.0, 0.0], color="black" )
	end
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]([ minimum(uniqueknots[1]), maximum(uniqueknots[1]) ], [ uniqueknots[2][i], uniqueknots[2][i] ], [0.0, 0.0], color="black" )
	end
	# The global b-spline basis functions
	bsplbasisf = function(gb::Array{Int64},t::Array{Float64})
			Bval = 0.0
			e,xi = e_xi_for_t(self,t)
			element_a_for_gb = zeros(Int64,2)
			element_a_for_gb[1] = self.GBEBI[1][e[1],gb[1]]
			element_a_for_gb[2] = self.GBEBI[2][e[2],gb[2]]
			if( minimum(element_a_for_gb) > 0 )
				Bval += basis(self, e, element_a_for_gb, xi)
			end
			return Bval
		end
	let
		h = 0.01
		# plot basis functions on the y=0 boundary
		for basis_j = 1:n[1]
			if( basis_j == 5 )
				local x = minimum(uniqueknots[1]):h:maximum(uniqueknots[1])
				y = zeros(length(x))
				basisfvals = zeros(length(x))
				for i = 1:length(x)
					basisfvals[i] = bsplbasisf([basis_j,1],[x[i],y[i]])
				end
				ax[:plot3D](x, y, basisfvals, color="purple" )
			end
		end
		# plot basis functions on the x=MAX boundary
		for basis_j = 1:n[2]
			if( false )
				y = minimum(uniqueknots[2]):h:maximum(uniqueknots[2])
				local x = ones(length(y)) * maximum(uniqueknots[1])
				basisfvals = zeros(length(y))
				for i = 1:length(y)
					basisfvals[i] = bsplbasisf([n[2],basis_j],[x[i],y[i]])
				end
				ax[:plot3D](x, y, basisfvals, color="purple" )
			end
		end
	end
	plt.zlim([0,1.2])
	plt.xlabel("x")
	plt.ylabel("y")
	plt.zlabel("z");
	plt.title("U-Spline Basis on Boundary")
	plt.savefig("plots_ignore/bdrybasisplot_bdrybasis_n5_00.pdf")
end


#### plotting the BERNSTEIN over the support of ONLY one of the basis functions (the first of two).
let
	local fig = plt.figure()
	local ax = plt.Axes3D(fig)
	plt.axis("off")
	# plot the surface
	#res_to_draw = [30,30]
	#X,t,h = draw(self,res_to_draw)
	#ax[:scatter3D](X[1,:,:],X[2,:,:],X[3,:,:])
	#ax[:scatter3D](self.control_pts[1,:,:],self.control_pts[2,:,:],self.control_pts[3,:,:],color="r")
	# plot the element boundaries
	local uniqueknots = [ UniqueKnotArray(self.knot_v[1]), UniqueKnotArray(self.knot_v[2]) ]
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]( [ uniqueknots[1][i], uniqueknots[1][i] ], [ minimum(uniqueknots[2]), maximum(uniqueknots[2]) ], [0.0, 0.0], color="black" )
	end
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]([ minimum(uniqueknots[1]), maximum(uniqueknots[1]) ], [ uniqueknots[2][i], uniqueknots[2][i] ], [0.0, 0.0], color="black" )
	end
	# The global b-spline basis functions
	bsplbasisf = function(gb::Array{Int64},t::Array{Float64})
			Bval = 0.0
			e,xi = e_xi_for_t(self,t)
			element_a_for_gb = zeros(Int64,2)
			element_a_for_gb[1] = self.GBEBI[1][e[1],gb[1]]
			element_a_for_gb[2] = self.GBEBI[2][e[2],gb[2]]
			if( minimum(element_a_for_gb) > 0 )
				Bval += basis(self, e, element_a_for_gb, xi)
			end
			return Bval
		end
	#
	bernsteinbasisf = function(bbi::Array{Int64},e_to_plot::Array{Int64},t::Array{Float64})
			Bval = 0.0
			e,xi = e_xi_for_t(self,t)
			element_a_for_gb = zeros(Int64,2)
			element_a_for_gb[1] = mod(bbi[1]-1,self.degree[1]+1)+1 #local bernstein basis index in the cell.
			element_a_for_gb[2] = mod(bbi[2]-1,self.degree[2]+1)+1 #local bernstein basis index in the cell.
			if( e == e_to_plot )
				Bval_1 = bernstein_basis(element_a_for_gb[1],self.degree[1],xi[1],dmin=-1.0,dmax=1.0)
				Bval_2 = bernstein_basis(element_a_for_gb[2],self.degree[2],xi[2],dmin=-1.0,dmax=1.0)
				Bval = Bval_1 * Bval_2
			end
			return Bval
		end
	let
		h = 0.0025
		# plot basis functions on the y=0 boundary
		n_bern_1 = self.nbezcurves[1]*(self.degree[1]+1)
		for basis_j = 1:n_bern_1
			cell_i =  div( basis_j-1, self.degree[1]+1 ) + 1
			if( cell_i == 2 || cell_i == 3 )
				@show cell_i
				local x = minimum(uniqueknots[1]):h:maximum(uniqueknots[1])
				y = zeros(length(x))
				basisfvals = zeros(length(x))
				for i = 1:length(x)
					basisfvals[i] = bernsteinbasisf([basis_j,1],[cell_i,1],[x[i],y[i]])
				end
				# Set the discontinuous sections to NaN so they don't plot.
				for i = 2:length(basisfvals)
					if( abs(basisfvals[i-1]-basisfvals[i]) > 0.5 )
						basisfvals[i] = NaN
					end
				end
				ax[:plot3D](x, y, basisfvals )
			end
		end
		# plot basis functions on the x=MAX boundary
		n_bern_2 = self.nbezcurves[2]*(self.degree[2]+1)
		for basis_j = 1:n_bern_2
			cell_i = 1
			if( false )
				y = minimum(uniqueknots[2]):h:maximum(uniqueknots[2])
				local x = ones(length(y)) * maximum(uniqueknots[1])
				basisfvals = zeros(length(y))
				for i = 1:length(y)
					basisfvals[i] = bernsteinbasisf([n_bern_2,basis_j],[self.nbezcurves[2],cell_i],[x[i],y[i]])
				end
				# Set the discontinuous sections to NaN so they don't plot.
				for i = 2:length(basisfvals)
					if( abs(basisfvals[i-1]-basisfvals[i]) > 0.5 )
						basisfvals[i] = NaN
					end
				end
				ax[:plot3D](x, y, basisfvals )
			end
		end
	end
	plt.zlim([0,1.2])
	plt.xlabel("x")
	plt.ylabel("y")
	plt.zlabel("z");
	plt.title("Bernstein Basis on Boundary")
	plt.savefig("plots_ignore/bdrybernsteinplot_bdrybases_support23_00.pdf")
end





####### plotting the DUAL of the first (of two) basis function (N_5)
let
	# The bernstein coeffs required to build the dual basis.
	coeffs =
		[ -0.360939022305,
		 0.94839986401,
		 -1.59816719209,
		 4.12745057436,
		 -16.1181668902,
		 9.57650526243,
		 17.0686202236,
		 -9.64370281977 ];

	local fig = plt.figure()
	local ax = plt.Axes3D(fig)
	plt.axis("off")
	# plot the surface
	#res_to_draw = [30,30]
	#X,t,h = draw(self,res_to_draw)
	#ax[:scatter3D](X[1,:,:],X[2,:,:],X[3,:,:])
	#ax[:scatter3D](self.control_pts[1,:,:],self.control_pts[2,:,:],self.control_pts[3,:,:],color="r")
	# plot the element boundaries
	local uniqueknots = [ UniqueKnotArray(self.knot_v[1]), UniqueKnotArray(self.knot_v[2]) ]
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]( [ uniqueknots[1][i], uniqueknots[1][i] ], [ minimum(uniqueknots[2]), maximum(uniqueknots[2]) ], [0.0, 0.0], color="black" )
	end
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]([ minimum(uniqueknots[1]), maximum(uniqueknots[1]) ], [ uniqueknots[2][i], uniqueknots[2][i] ], [0.0, 0.0], color="black" )
	end

	bernsteinbasisf = function(bbi::Array{Int64},e_to_plot::Array{Int64},t::Array{Float64})
		Bval = 0.0
		e,xi = e_xi_for_t(self,t)
		element_a_for_gb = zeros(Int64,2)
		element_a_for_gb[1] = mod(bbi[1]-1,self.degree[1]+1)+1 #local bernstein basis index in the cell.
		element_a_for_gb[2] = mod(bbi[2]-1,self.degree[2]+1)+1 #local bernstein basis index in the cell.
		if( e == e_to_plot )
			Bval_1 = bernstein_basis(element_a_for_gb[1],self.degree[1],xi[1],dmin=-1.0,dmax=1.0)
			Bval_2 = bernstein_basis(element_a_for_gb[2],self.degree[2],xi[2],dmin=-1.0,dmax=1.0)
			Bval = Bval_1 * Bval_2
		end
		return Bval
	end

	# The dual on the boundary.
	dualbasisf = function(t::Array{Float64})
		dualB_val = 0.0
		for i = 1:8
			basis_j = i+4
			degree = 3
			cell_i = div( basis_j-1, degree+1 ) + 1
			bern_basis_f_val = bernsteinbasisf( [basis_j,1], [cell_i,1], t )
			dualB_val += bern_basis_f_val * coeffs[i]
		end
		return dualB_val
	end
	let
		h = 0.001
		# plot basis function on the y=0 boundary
		let
			local x = minimum(uniqueknots[1]):h:maximum(uniqueknots[1])
			y = zeros(length(x))
			dualbasisvals = zeros(length(x))
			for i = 1:length(x)
				dualbasisvals[i] = dualbasisf([x[i],y[i]])
			end
			# Set the discontinuous sections to NaN so they don't plot.
			for i = 2:length(dualbasisvals)
				if( abs(dualbasisvals[i-1]-dualbasisvals[i]) > 0.5 )
					dualbasisvals[i] = NaN
				end
			end
			ax[:plot3D](x, y, dualbasisvals )
		end
		# plot basis functions on the x=MAX boundary
		#for basis_j = 1:n[2]
		#	y = minimum(uniqueknots[2]):h:maximum(uniqueknots[2])
		#	local x = ones(length(y)) * maximum(uniqueknots[1])
		#	dualbasisvals = zeros(length(y))
		#	for i = 1:length(y)
		#		dualbasisvals[i] = dualbasisf([x[i],y[i]])
		#	end
		#	ax[:plot3D](x, y, dualbasisvals )
		#end
	end
	#plt.zlim([0,1.2])
	plt.xlabel("x")
	plt.ylabel("y")
	plt.zlabel("z");
	plt.title("Dual Basis on Boundary - straight edge")
	plt.savefig("plots_ignore/bdrydual_N5_00.pdf")
end








####### plotting one of the global basis functions (second of two)
let
	res_to_draw = [30,30]
	X,t,h = draw(self,res_to_draw)
	local fig = plt.figure()
	local ax = plt.Axes3D(fig)
	plt.axis("off")
	# plot the surface
	#ax[:scatter3D](X[1,:,:],X[2,:,:],X[3,:,:])
	#ax[:scatter3D](self.control_pts[1,:,:],self.control_pts[2,:,:],self.control_pts[3,:,:],color="r")
	# plot the element boundaries
	local uniqueknots = [ UniqueKnotArray(self.knot_v[1]), UniqueKnotArray(self.knot_v[2]) ]
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]( [ uniqueknots[1][i], uniqueknots[1][i] ], [ minimum(uniqueknots[2]), maximum(uniqueknots[2]) ], [0.0, 0.0], color="black" )
	end
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]([ minimum(uniqueknots[1]), maximum(uniqueknots[1]) ], [ uniqueknots[2][i], uniqueknots[2][i] ], [0.0, 0.0], color="black" )
	end
	# The global b-spline basis functions
	bsplbasisf = function(gb::Array{Int64},t::Array{Float64})
			Bval = 0.0
			e,xi = e_xi_for_t(self,t)
			element_a_for_gb = zeros(Int64,2)
			element_a_for_gb[1] = self.GBEBI[1][e[1],gb[1]]
			element_a_for_gb[2] = self.GBEBI[2][e[2],gb[2]]
			if( minimum(element_a_for_gb) > 0 )
				Bval += basis(self, e, element_a_for_gb, xi)
			end
			return Bval
		end
	# plot basis functions on the y=0 boundary
	let
		h = 0.01
		for basis_j = 1:n[1]
			if( basis_j == 6 )
				local x = minimum(uniqueknots[1]):h:maximum(uniqueknots[1])
				y = zeros(length(x))
				basisfvals = zeros(length(x))
				for i = 1:length(x)
					basisfvals[i] = bsplbasisf([basis_j,1],[x[i],y[i]])
				end
				ax[:plot3D](x, y, basisfvals, color="brown" )
			end
		end
		for basis_j = 1:n[2]
			if( basis_j == 1 )
				y = minimum(uniqueknots[2]):h:maximum(uniqueknots[2])
				local x = ones(length(y)) * maximum(uniqueknots[1])
				basisfvals = zeros(length(y))
				for i = 1:length(y)
					basisfvals[i] = bsplbasisf([n[2],basis_j],[x[i],y[i]])
				end
				ax[:plot3D](x, y, basisfvals, color="brown" )
			end
		end
	end
	plt.zlim([0,1.2])
	plt.xlabel("x")
	plt.ylabel("y")
	plt.zlabel("z");
	plt.title("U-Spline Basis on Boundary")
	plt.savefig("plots_ignore/bdrybasisplot_bdrybasis_n6_00.pdf")
end



#### plotting the BERNSTEIN over the support of ONLY one of the basis functions (the second of two).
let
	local fig = plt.figure()
	local ax = plt.Axes3D(fig)
	plt.axis("off")
	# plot the surface
	#res_to_draw = [30,30]
	#X,t,h = draw(self,res_to_draw)
	#ax[:scatter3D](X[1,:,:],X[2,:,:],X[3,:,:])
	#ax[:scatter3D](self.control_pts[1,:,:],self.control_pts[2,:,:],self.control_pts[3,:,:],color="r")
	# plot the element boundaries
	local uniqueknots = [ UniqueKnotArray(self.knot_v[1]), UniqueKnotArray(self.knot_v[2]) ]
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]( [ uniqueknots[1][i], uniqueknots[1][i] ], [ minimum(uniqueknots[2]), maximum(uniqueknots[2]) ], [0.0, 0.0], color="black" )
	end
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]([ minimum(uniqueknots[1]), maximum(uniqueknots[1]) ], [ uniqueknots[2][i], uniqueknots[2][i] ], [0.0, 0.0], color="black" )
	end
	# The global b-spline basis functions
	bsplbasisf = function(gb::Array{Int64},t::Array{Float64})
			Bval = 0.0
			e,xi = e_xi_for_t(self,t)
			element_a_for_gb = zeros(Int64,2)
			element_a_for_gb[1] = self.GBEBI[1][e[1],gb[1]]
			element_a_for_gb[2] = self.GBEBI[2][e[2],gb[2]]
			if( minimum(element_a_for_gb) > 0 )
				Bval += basis(self, e, element_a_for_gb, xi)
			end
			return Bval
		end
	#
	bernsteinbasisf = function(bbi::Array{Int64},e_to_plot::Array{Int64},t::Array{Float64})
			Bval = 0.0
			e,xi = e_xi_for_t(self,t)
			element_a_for_gb = zeros(Int64,2)
			element_a_for_gb[1] = mod(bbi[1]-1,self.degree[1]+1)+1 #local bernstein basis index in the cell.
			element_a_for_gb[2] = mod(bbi[2]-1,self.degree[2]+1)+1 #local bernstein basis index in the cell.
			if( e == e_to_plot )
				Bval_1 = bernstein_basis(element_a_for_gb[1],self.degree[1],xi[1],dmin=-1.0,dmax=1.0)
				Bval_2 = bernstein_basis(element_a_for_gb[2],self.degree[2],xi[2],dmin=-1.0,dmax=1.0)
				Bval = Bval_1 * Bval_2
			end
			return Bval
		end
	let
		h = 0.0025
		# plot basis functions on the y=0 boundary
		n_bern_1 = self.nbezcurves[1]*(self.degree[1]+1)
		for basis_j = 1:n_bern_1
			cell_i =  div( basis_j-1, self.degree[1]+1 ) + 1
			if( cell_i == 3 )
				@show cell_i
				local x = minimum(uniqueknots[1]):h:maximum(uniqueknots[1])
				y = zeros(length(x))
				basisfvals = zeros(length(x))
				for i = 1:length(x)
					basisfvals[i] = bernsteinbasisf([basis_j,1],[cell_i,1],[x[i],y[i]])
				end
				# Set the discontinuous sections to NaN so they don't plot.
				for i = 2:length(basisfvals)
					if( abs(basisfvals[i-1]-basisfvals[i]) > 0.5 )
						basisfvals[i] = NaN
					end
				end
				ax[:plot3D](x, y, basisfvals )
			end
		end
		# plot basis functions on the x=MAX boundary
		n_bern_2 = self.nbezcurves[2]*(self.degree[2]+1)
		for basis_j = 1:n_bern_2
			cell_i =  div( basis_j-1, self.degree[2]+1 ) + 1
			if( cell_i == 1 )
				y = minimum(uniqueknots[2]):h:maximum(uniqueknots[2])
				local x = ones(length(y)) * maximum(uniqueknots[1])
				basisfvals = zeros(length(y))
				for i = 1:length(y)
					basisfvals[i] = bernsteinbasisf([n_bern_2,basis_j],[self.nbezcurves[2],cell_i],[x[i],y[i]])
				end
				# Set the discontinuous sections to NaN so they don't plot.
				for i = 2:length(basisfvals)
					if( abs(basisfvals[i-1]-basisfvals[i]) > 0.5 )
						basisfvals[i] = NaN
					end
				end
				ax[:plot3D](x, y, basisfvals )
			end
		end
	end
	plt.zlim([0,1.2])
	plt.xlabel("x")
	plt.ylabel("y")
	plt.zlabel("z");
	plt.title("Bernstein Basis on Boundary")
	plt.savefig("plots_ignore/bdrybernsteinplot_bdrybases_support31_00.pdf")
end



####### plotting the DUAL of the SECOND (of two) -- ON CORNER -- basis function (N_6)
let
	# The bernstein coeffs required to build the dual basis.
	coeffs =
		[ -1.,  4 , -6 , 4 , -6 , 4 , -1 ];

	local fig = plt.figure()
	local ax = plt.Axes3D(fig)
	plt.axis("off")
	# plot the surface
	#res_to_draw = [30,30]
	#X,t,h = draw(self,res_to_draw)
	#ax[:scatter3D](X[1,:,:],X[2,:,:],X[3,:,:])
	#ax[:scatter3D](self.control_pts[1,:,:],self.control_pts[2,:,:],self.control_pts[3,:,:],color="r")
	# plot the element boundaries
	local uniqueknots = [ UniqueKnotArray(self.knot_v[1]), UniqueKnotArray(self.knot_v[2]) ]
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]( [ uniqueknots[1][i], uniqueknots[1][i] ], [ minimum(uniqueknots[2]), maximum(uniqueknots[2]) ], [0.0, 0.0], color="black" )
	end
	for i = 1:self.nbezcurves[1]+1
		ax[:plot3D]([ minimum(uniqueknots[1]), maximum(uniqueknots[1]) ], [ uniqueknots[2][i], uniqueknots[2][i] ], [0.0, 0.0], color="black" )
	end

	bernsteinbasisf = function(bbi::Array{Int64},e_to_plot::Array{Int64},t::Array{Float64})
		Bval = 0.0
		e,xi = e_xi_for_t(self,t)
		element_a_for_gb = zeros(Int64,2)
		element_a_for_gb[1] = mod(bbi[1]-1,self.degree[1]+1)+1 #local bernstein basis index in the cell.
		element_a_for_gb[2] = mod(bbi[2]-1,self.degree[2]+1)+1 #local bernstein basis index in the cell.
		if( e == e_to_plot )
			Bval_1 = bernstein_basis(element_a_for_gb[1],self.degree[1],xi[1],dmin=-1.0,dmax=1.0)
			Bval_2 = bernstein_basis(element_a_for_gb[2],self.degree[2],xi[2],dmin=-1.0,dmax=1.0)
			Bval = Bval_1 * Bval_2
		end
		return Bval
	end

	# The dual on the boundary.
	dualbasisf = function(t::Array{Float64})
		dualB_val = 0.0
		if( abs(t[2]) < 1e-12 )
			for i = 1:4
				coeff_i = i
				basis_j = i+8
				degree = 3
				cell_i = div( basis_j-1, degree+1 ) + 1
				bern_basis_f_val = bernsteinbasisf( [basis_j,1], [cell_i,1], t )
				dualB_val += bern_basis_f_val * coeffs[coeff_i]
			end
		else
			for i = 1:4
				coeff_i = i+3
				basis_j = i
				degree = 3
				cell_i = div( basis_j-1, degree+1 ) + 1
				bern_basis_f_val = bernsteinbasisf( [12,basis_j], [3,cell_i], t )
				dualB_val += bern_basis_f_val * coeffs[coeff_i]
			end
		end
		return dualB_val
	end
	let
		h = 0.001
		# plot basis function on the y=0 boundary
		let
			local x = minimum(uniqueknots[1]):h:maximum(uniqueknots[1])
			y = zeros(length(x))
			dualbasisvals = zeros(length(x))
			for i = 1:length(x)
				dualbasisvals[i] = dualbasisf([x[i],y[i]])
			end
			# Set the discontinuous sections to NaN so they don't plot.
			for i = 2:length(dualbasisvals)
				if( abs(dualbasisvals[i-1]-dualbasisvals[i]) > 0.5 )
					dualbasisvals[i] = NaN
				end
			end
			ax[:plot3D](x, y, dualbasisvals, color="blue" )
		end
		# plot basis functions on the x=MAX boundary
		let
			y = minimum(uniqueknots[2]):h:maximum(uniqueknots[2])
			local x = ones(length(y)) * maximum(uniqueknots[1])
			dualbasisvals = zeros(length(x))
			for i = 1:length(x)
				dualbasisvals[i] = dualbasisf([x[i],y[i]])
			end
			# Set the discontinuous sections to NaN so they don't plot.
			for i = 2:length(dualbasisvals)
				if( abs(dualbasisvals[i-1]-dualbasisvals[i]) > 0.5 )
					dualbasisvals[i] = NaN
				end
			end
			ax[:plot3D](x, y, dualbasisvals, color="blue" )
		end
	end
	#plt.zlim([0,1.2])
	plt.xlabel("x")
	plt.ylabel("y")
	plt.zlabel("z");
	plt.title("Dual Basis on Boundary - corner")
	plt.savefig("plots_ignore/bdrydual_N6_00.pdf")
end









function build_matrix_diagram(
				A::Array{Float64,2},
				x::Array{Float64,1},
				b::Array{Float64,1},
				support_size::Int64,
				line_thickness_major::Float64,
				marker_size::Float64,
				margin::Float64,
				include_strike::Bool,
				inclue_e_text::Bool )

	x = rand(10)
	y = rand(10)
	nrows = size(A)[1]
	ncols = size(A)[2]
	@show(nrows)
	@show(ncols)
	colwidth = 1.0
	rowwidth = 1.0
	top = max(rowwidth * nrows, colwidth * ncols)
	bottom = 0.0
	left = 0.0
	right = left + ncols*colwidth + colwidth + 2*colwidth + 2*colwidth + colwidth

	width = right - left
	height = top - bottom
	final_top = top + 4*margin
	final_bottom = top - height - margin
	final_left = left - margin
	final_right = left + width + margin

	fig_width = 5
	fig_height = fig_width * (final_top-final_bottom)/(final_right-final_left)
	@show( fig_width )
	@show( fig_height )
	figure_size_mtrxdiag = (fig_width, fig_height)
	plt.figure(figsize=figure_size_mtrxdiag)
	plt.axis("off")
	plt.ylim([final_bottom, final_top])
	plt.xlim([final_left, final_right])

	# Plot the 'A' matrix.
	let
		for i = 0:nrows
			plt.plot([left,left+ncols*colwidth],[top-i*rowwidth,top-i*rowwidth],color="k",linewidth=line_thickness_major)
		end
		for i = 0:ncols
			plt.plot([left+i*colwidth,left+i*colwidth],[top,top-nrows*rowwidth],color="k",linewidth=line_thickness_major)
		end
		for i = 1:nrows
			for j = 1:ncols
				if( abs(A[i,j]) > 1e-12 )
					plt.scatter([ left + (j-1)*colwidth + 0.5*colwidth ], [ top - (i-1)*rowwidth - 0.5*rowwidth ],color="k",s=marker_size)
				end
			end
		end
	end

	# Plot the 'x' vector
	xvec_left = left+ncols*colwidth + colwidth
	let
		# note: the 'x' vector is 'ncols' tall.
		plt.plot([xvec_left, xvec_left],[top,top-ncols*rowwidth],color="k",linewidth=line_thickness_major)
		plt.plot([xvec_left + colwidth, xvec_left + colwidth],[top,top-ncols*rowwidth],color="k",linewidth=line_thickness_major)
		for i = 1:ncols+1
			plt.plot([xvec_left, xvec_left + colwidth],[top-(i-1)*rowwidth,top-(i-1)*rowwidth],color="k",linewidth=line_thickness_major)
		end
		for j = 1:ncols
			plt.scatter([ xvec_left + 0.5*colwidth ], [ top - (j-1)*rowwidth - 0.5*rowwidth ],color="k",s=marker_size)
		end
	end

	# draw the equals sign
	equals_left = xvec_left + 2*colwidth
	let
		plt.plot([equals_left, equals_left + 0.75*colwidth],[top-div(nrows,2)*rowwidth,top-div(nrows,2)*rowwidth],color="k",linewidth=line_thickness_major)
		plt.plot([equals_left, equals_left + 0.75*colwidth],[top-div(nrows,2)*rowwidth-0.5*rowwidth,top-div(nrows,2)*rowwidth-0.5*rowwidth ],color="k",linewidth=line_thickness_major)
	end

	# Plot the 'b' vector
	bvec_left = equals_left + 2*colwidth
	let
		# note: the 'b' vector is 'nrows' tall.
		plt.plot([bvec_left, bvec_left],[top,top-nrows*rowwidth],color="k",linewidth=line_thickness_major)
		plt.plot([bvec_left + colwidth, bvec_left + colwidth],[top,top-nrows*rowwidth],color="k",linewidth=line_thickness_major)
		for i = 1:nrows+1
			plt.plot([bvec_left, bvec_left + colwidth],[top-(i-1)*rowwidth,top-(i-1)*rowwidth],color="k",linewidth=line_thickness_major)
		end
		@show(b)
		for j = 1:nrows
			if( abs(b[j]) > 1e-12 )
				plt.scatter([ bvec_left + 0.5*colwidth ], [ top - (j-1)*rowwidth - 0.5*rowwidth ],color="k",s=marker_size)
			end
		end
	end

	local ax = plt.axes()
	ax[:text]( left+0.4*colwidth, top+0.2*rowwidth, "i → ", color="black" )
	#ax[:text]( right+0.3*colwidth, top-2*rowwidth+0.3*rowwidth, "← A", color="black" )
	ax[:text]( left-0.6*colwidth, top-0.7*rowwidth, "B", color="black" )
	ax[:text]( left-0.65*colwidth, top-1.4*rowwidth, "↓", color="black" )
	if( support_size > 1 )
		ax[:text]( left-0.6*colwidth, top-(nrows-support_size)*rowwidth-0.7*rowwidth, "e", color="black" )
		ax[:text]( left-0.65*colwidth, top-(nrows-support_size)*rowwidth-1.4*rowwidth, "↓", color="black" )
	elseif( support_size == 1 )
		ax[:text]( left-1.0*colwidth, top-(nrows-support_size)*rowwidth-0.7*rowwidth, "e", color="black" )
		ax[:text]( left-0.6*colwidth, top-(nrows-support_size)*rowwidth-0.7*rowwidth, "→ ", color="black" )
	end
	if( inclue_e_text )
		if( support_size == 2 )
			ax[:text]( left + 2*colwidth - 0.6*colwidth, top+1.0*rowwidth, "e = 2", color="black" )
			ax[:text]( left + 6*colwidth - 0.7*colwidth, top+1.0*rowwidth, "e = 3", color="black" )
		elseif( support_size == 1 )
			Awidth = size(A)[2]
			ax[:text]( left + (Awidth/2)*colwidth - 0.6*colwidth, top+1.0*rowwidth, "e = 3", color="black" )
		end
	end

	if( include_strike )
		plt.plot([left-0.2*colwidth,left+ncols*colwidth+0.0*colwidth],[top-1.5*rowwidth,top-1.5*rowwidth],color="r",linewidth=line_thickness_major*2.5)
		plt.plot([right-1.0*colwidth,right+0.4*colwidth],[top-1.5*rowwidth,top-1.5*rowwidth],color="r",linewidth=line_thickness_major*2.5)
	end

	return
end

## A, b of linear system for boundary basis 5.
A =
[ [ 0.0357142857143  0.0178571428571 0.00714285714286 0.00178571428571                0                0                0                0 ];
[  0.141666666667            0.125              0.1  0.0708333333333  0.0238095238095  0.0119047619048  0.0047619047619 0.00119047619048 ];
[ 0.0708333333333              0.1            0.125   0.141666666667   0.119047619048  0.0845238095238  0.0488095238095  0.0184523809524 ];
[               0                0                0                0 0.00714285714286  0.0285714285714  0.0714285714286   0.142857142857 ];
[ 0.00178571428571 0.00714285714286  0.0178571428571  0.0357142857143                0                0                0                0 ];
[               0                0                0                0              0.1            0.125            0.125           0.0875 ]];

b =
[
    0,
    0,
    0,
    0,
0.125,
0.875 ];

x = ones(size(A)[2])

line_thickness_major = 2.0
marker_size = 40.0
margin = 0.1
include_strike=false
inclue_e_text=true
support_size = 2
build_matrix_diagram(
				A,
				x,
				b,
				support_size,
				line_thickness_major,
				marker_size,
				margin,
				include_strike,
				inclue_e_text )
plt.savefig("plots_ignore/bdryduals_system_N5.pdf")





A =
[ [ 0                0                0            0.175             0.25             0.25              0.2 ];
  [ 0                0                0  0.0369047619048   0.097619047619   0.169047619048   0.238095238095 ];
  [ 0                0                0 0.00238095238095 0.00952380952381  0.0238095238095   0.047619047619 ];
  [ 0.047619047619  0.0238095238095 0.00952380952381 0.00238095238095                0                0                0 ];
  [ 0.238095238095   0.169047619048   0.097619047619  0.0369047619048                0                0                0 ];
  [ 0.2             0.25             0.25            0.175                0                0                0 ];
  [ 0.0142857142857  0.0571428571429   0.142857142857   0.571428571429   0.142857142857  0.0571428571429  0.0142857142857 ] ];

b =
[ 0.0,
0,
0,
0,
0,
0,
1 ];

x = ones(size(A)[2])


line_thickness_major = 2.0
marker_size = 40.0
margin = 0.1
include_strike=false
inclue_e_text=true
support_size = 1
build_matrix_diagram(
				A,
				x,
				b,
				support_size,
				line_thickness_major,
				marker_size,
				margin,
				include_strike,
				inclue_e_text )
plt.savefig("plots_ignore/bdryduals_system_N6.pdf")


function bdrycorner_bernstein_basisf(bbi::Int64,t::Float64)
	Bval = 0.0
	a = -1
	if( bbi <= 4 )
		a = bbi
		if( t < 1.0 )
			Bval += bernstein_basis(a,3,t,dmin=0.0,dmax=1.0)
		end
	end
	if( bbi >= 4 )
		a = bbi-3
		if( t >= 1.0 )
			Bval += bernstein_basis(a,3,t-1.0,dmin=0.0,dmax=1.0)
		end
	end
	return Bval
end


plt.figure()
t = collect(0.0:0.001:1.0)
for bbi = 1:4
	basis = Array{Float64}([ bdrycorner_bernstein_basisf(bbi,t[i]) for i = 1:length(t) ])
	plt.plot( t, basis )
end
plt.title("Cubic Bernstein functions" )
plt.savefig("plots_ignore/corner_bernstein_one_edge_00.pdf")


plt.figure()
t = collect(0.0:0.001:2.0)
for bbi = 1:7
	basis = Array{Float64}([ bdrycorner_bernstein_basisf(bbi,t[i]) for i = 1:length(t) ])
	plt.plot( t, basis )
end
plt.title("Cubic Bernstein functions along boundary in one cell" )
plt.savefig("plots_ignore/corner_bernstein_around_corner_00.pdf")

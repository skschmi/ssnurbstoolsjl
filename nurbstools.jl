
module SSNurbsTools

using SparseArrays
using LinearAlgebra

# Library for indexing
include("./ssindexingtools/indexingtools.jl")
using .SSIndexingTools
import .SSIndexingTools.LagrangeIEN
import .SSIndexingTools.IsogeometricIEN

"""
function bernstein_basis(a::Int64,
                        p::Int64,
                        t::Float64;
                        dmin::Float64=0.,
                        dmax::Float64=1.)
============================
# Parameters
* a: The function index
* p: The polynomial degree
* t: The parameter value where the basis function is to be evaluated

# Named Parameters
* dmin: The minimum value of the domain
* dmax: The maximum value of the domain
"""
function bernstein_basis(a::Int64,
                        p::Int64,
                        t::Union{Float64,ComplexF64};
                        dmin::Float64=0.,
                        dmax::Float64=1.)
    if(a < 1 || a > p+1 || real(t) < dmin || real(t) > dmax)
        return 0.0
    end
    tmapped = (t - dmin)/(dmax-dmin)
    result = binomial(p,a-1) * tmapped^(a-1) * (1.0-tmapped)^(p-(a-1))
    return result
end


"""
function del_bernstein_basis(a::Int64,
                           p::Int64,
                           t::Float64;
                           dmin::Float64=0.,
                           dmax::Float64=1.)
===============

# Parameters
* a: The function index
* p: The polynomial degree
* t: The parameter value where the basis function is to be evaluated

# Named Parameters
* dmin: The minimum value of the domain
* dmax: The maximum value of the domain
"""
function del_bernstein_basis(a::Int64,
                           p::Int64,
                           t::Float64;
                           dmin::Float64=0.,
                           dmax::Float64=1.)
    tmapped = (t - dmin)/(dmax-dmin)
    result = p * (bernstein_basis(a-1,p-1,tmapped) - bernstein_basis(a,p-1,tmapped)) / (dmax-dmin)
    return real(result)
end

"""
bernstein_basis_vdv(self::IGAGrid2D,a::Int64,p::Int64,xi::Float64)
=============

Returns both value and derivative of the bernstein basis

# Returns
* bernb
* delbernb
"""
function bernstein_basis_vdv(a::Int64,
                             p::Int64,
                             xi::Float64;
                             dmin::Float64=0.,
                             dmax::Float64=1.)
    h = 1.0e-40
    val = bernstein_basis(a,p,xi+h*im,dmin=dmin,dmax=dmax)
    bernb = real(val)
    delbernb = imag(val)/h
    return bernb, delbernb
end

"""
function UniqueKnotCount(knot_v::Array{Float64})
=================
# Parameters:
* knot_v: Valid knot vector, ::Array{Float64}

# Returns:
* Number of unique values in the knot vector
"""
function UniqueKnotCount(knot_v::Array{Float64})
    if(length(knot_v) <= 0)
        return 0
    end
    val = knot_v[1] - 9.9e9 #init to bogus value not already in the knot vector.
    count = 0
    len_kv = length(knot_v)
    for i = 1:len_kv
        if(knot_v[i] != val)
            val = knot_v[i]
            count += 1
        end
    end
    return count
end

"""
function UniqueKnotArray(knot_v::Array{Float64})
=================
# Parameters:
* knot_v: Valid knot vector, ::Array{Float64}

# Returns:
* Array containing each unique knot value (once each)
"""
function UniqueKnotArray(knot_v::Array{Float64})

    if(length(knot_v) <= 0)
        return Array{Float64}(undef,0)
    end

    # Determine how many unique values there are.
    count = UniqueKnotCount(knot_v)

    # Init the array
    uniqueknots = zeros(count)

    # Fill the array with knots
    val = knot_v[1]-9.9e9 #init to bogus value not already in the knot vector.
    j = 1
    len_kv = length(knot_v)
    for i = 1:len_kv
        if(knot_v[i] != val)
            val = knot_v[i]
            uniqueknots[j] = knot_v[i]
            j += 1
        end
    end

    return uniqueknots
end

"""
function Multiplicity(knot_v::Array{Float64},knot::Float64)
=================

Returns the multiplicity of a knot in a knot vector.

# Parameters:
* knot_v: The knot vector, ::Array{Float64}
* knot: The knot we're asking about, ::Float64

# Returns:
* The multiplicity of the knot.
"""
function Multiplicity(knot_v::Array{Float64},knot::Float64)
    count = 0
    len_kv = length(knot_v)
    for i = 1:len_kv
        if(knot_v[i] > knot)
            break
        end
        if(knot_v[i] == knot)
            count += 1
        end
    end
    return count
end



"""
function secondToEndKnotIndices(knot_v::Array{Float64})
========

Returns the index of the first different knot on each end,
from their respective end.
(Left value is the index from the left,
the right value is the index from the right)
aka  [0 0 0 1 2 3 4 5 5 5]
would return 4,4
"""
function secondToEndKnotIndices(knot_v::Array{Float64})
    left_i = -1
    right_i = -1
    for i = 1:length(knot_v)
        if(knot_v[i] != knot_v[1])
            left_i = i
            break
        end
    end
    for i = length(knot_v):-1:1
        if(knot_v[i] != knot_v[end])
            right_i = length(knot_v) - i + 1
            break
        end
    end
    return left_i,right_i
end



"""
function b_spline_basis(a::Int64,
                        degree::Int64,
                        C::Array{Float64,2}
                        xi::Float64)
=========

Bezier Extraction Operator (C) version

#Parameters:
* a: The index of the basis function in the element.
* degree: The polynomial degree of the B-Spline curve
* C: The bezier extraction operator for the element.
* xi: Value between -1 and 1

# Returns:
* N(xi) for the a-th NURBS function in the element.
"""
function b_spline_basis(a::Int64,
                        degree::Int64,
                        C::Array{Float64,2},
                        xi::Float64)
    B = Array{Float64}([bernstein_basis(aa,degree,xi;dmin=-1.0,dmax=1.0) for aa=1:degree+1])
    return dot(C[a,:][:],B[:])
end


"""
function del_b_spline_basis(a::Int64,
                            degree::Int64,
                            C::Array{Float64,2}
                            xi::Float64)
=========

Bezier Extraction Operator (C) version

#Parameters:
* a: The index of the basis function in the element.
* degree: The polynomial degree of the B-Spline curve
* C: The bezier extraction operator for the element.
* xi: Value between -1 and 1

# Returns:
* dN/dxi for the a-th NURBS function in the element.
"""
function del_b_spline_basis(a::Int64,
                            degree::Int64,
                            C::Array{Float64,2},
                            xi::Float64)
    del_B = Array{Float64}([del_bernstein_basis(aa,degree,xi;dmin=-1.0,dmax=1.0) for aa=1:degree+1])
    return dot(C[a,:][:],del_B[:])
end


"""
function weights_sum_term(degree::Int64,
                          C::Array{Float64,2}
                          weights::Array{Float64},
                          xi::Float64)
=========

Bezier Extraction Operator (C) version

#Parameters:
* degree: The polynomial degree of the B-Spline curve
* C: The bezier extraction operator for the element.
* weights: The weights for the element
* xi: Value between -1 and 1

# Returns:
* weights_sum_term for the a-th NURBS function in the element.
"""
function weights_sum_term(degree::Int64,
                          C::Array{Float64,2},
                          weights::Array{Float64},
                          xi::Float64)
    W = 0.
    val = 0.
    for j = 1:length(weights)
        val = b_spline_basis(j,degree,C,xi) * weights[j]
        W += val
    end
    return W
end


"""
function del_weights_sum_term(degree::Int64,
                              C::Array{Float64,2}
                              weights::Array{Float64},
                              xi::Float64)
=========

Bezier Extraction Operator (C) version

#Parameters:
* degree: The polynomial degree of the B-Spline curve
* C: The bezier extraction operator for the element.
* weights: The weights for the element
* xi: Value between -1 and 1

# Returns:
* del_weights_sum_term for the element.
"""
function del_weights_sum_term(degree::Int64,
                              C::Array{Float64,2},
                              weights::Array{Float64},
                              xi::Float64)
    delW = 0.
    val = 0.
    for j = 1:length(weights)
        val = del_b_spline_basis(j,degree,C,xi) * weights[j]
        delW += val
    end
    return delW
end


"""
function nurbs_basis
======

Bezier Extraction Operator (C) version

#Parameters:
* a: The index of the basis function in the element.
* weights: The weights for the element
* xi: Value between -1 and 1
* C_a: The a-th row of the bezier extraction operator

# Returns:
* Value of the the a-th NURBS basis function, evaluated at xi.
"""
function nurbs_basis(a::Int64,
                     degree::Int64,
                     C::Array{Float64,2},
                     weights::Array{Float64},
                     xi::Float64)
    numerator =  b_spline_basis(a,degree,C,xi) * weights[a]
    denominator = weights_sum_term(degree,C,weights,xi)
    result = numerator/denominator
    if(isnan(result))
        result = 0.0
    end
    return result
end




"""
function nurbs_basis
======

Multi-D version of nurbs_basis.
Returns R(xi1,xi2,...) for multi-d Nurbs basis R
"""
function nurbs_basis(a::Array{Int64},
                    degree::Array{Int64},
                    C::Array{Array{}},
                    weights::Array{Array{Float64},1},
                    xi::Array{Float64})

    result = 1.0
    dim_p = length(a)
    for dir = 1:dim_p
        result *= nurbs_basis(a[dir],
                              degree[dir],
                              C[dir],
                              weights[dir],
                              xi[dir])
    end
    return result
end

"""
function nurbs_basis_ds
======

Returns the derivative of the Nurbs basis function w.r.t. the design parameter s.
Or, in other words, returns dR/ds where R is the Nurbs basis function
"""
function nurbs_basis_ds(a::Int64,
                        degree::Int64,
                        C::Array{Float64,2},
                        weights::Array{Float64},
                        weights_ds::Array{Float64},
                        xi::Float64)
    N = b_spline_basis(a,degree,C,xi)
    w_N = weights[a] * N
    dwds_N = weights_ds[a] * N
    sum_w_N = weights_sum_term(degree,C,weights,xi)
    sum_dwds_N = weights_sum_term(degree,C,weights_ds,xi)
    result = (dwds_N * sum_w_N - w_N * sum_dwds_N) / ((sum_w_N)^2)
    return result
end


"""
function nurbs_basis_ds
======

Multi-D version.
Returns the derivative of the Nurbs basis function w.r.t. the design parameter s.
Or, in other words, returns dR/ds where R is the Nurbs basis function
"""
function nurbs_basis_ds(a::Array{Int64},
                        degree::Array{Int64},
                        C::Array{Array{}},
                        weights::Array{Array{Float64},1},
                        weights_ds::Array{Array{Float64},1},
                        xi::Array{Float64})
    # Collect the R, dRds values
    dim_p = length(a)
    R = zeros(dim_p)
    dRds = zeros(dim_p)
    for dir = 1:dim_p
        R[dir] = nurbs_basis(a[dir],degree[dir],C[dir],weights[dir],xi[dir])
        dRds[dir] = nurbs_basis_ds(a[dir],degree[dir],C[dir],weights[dir],weights_ds[dir],xi[dir])
    end
    # Use the product rule to form the derivative
    result = 0.0
    for dir = 1:dim_p
        subresult = 1.0
        for odir = 1:dim_p
            if(odir == dir)
                subresult *= dRds[odir]
            else
                subresult *= R[odir]
            end
        end
        result += subresult
    end
    return result
end

"""
function del_nurbs_basis
======

Returns dR/dxi where R is the Nurbs basis function
"""
function del_nurbs_basis(a::Int64,
                         degree::Int64,
                         C::Array{Float64,2},
                         weights::Array{Float64},
                         xi::Float64)
    #Defined on pages 51-52 of IGA book.
    W = weights_sum_term(degree,C,weights,xi)
    delW = del_weights_sum_term(degree,C,weights,xi)

    delBezN = del_b_spline_basis(a,degree,C,xi)
    bezN = b_spline_basis(a,degree,C,xi)

    result = weights[a] * ((W*delBezN - delW*bezN) / W^2)
    return result
end

"""
function del_nurbs_basis
======

Multi-D version
Returns dR/dxi where R is the Nurbs basis function
"""
function del_nurbs_basis(a::Array{Int64},
                         degree::Array{Int64},
                         C::Array{Array{}},
                         weights::Array{Array{Float64},1},
                         xi::Array{Float64})
    # Collect the R, dRdxi values
    dim_p = length(a)
    R = zeros(dim_p)
    dRdxi = zeros(dim_p)
    for dir = 1:dim_p
        R[dir] = nurbs_basis(a[dir],degree[dir],C[dir],weights[dir],xi[dir])
        dRdxi[dir] = del_nurbs_basis(a[dir],degree[dir],C[dir],weights[dir],xi[dir])
    end
    # Use the product rule to form the derivative
    result = zeros(dim_p)
    for dir = 1:dim_p
        subresult = 1.0
        for odir = 1:dim_p
            if(odir == dir)
                subresult *= dRdxi[odir]
            else
                subresult *= R[odir]
            end
        end
        result[dir] = subresult
    end
    return result
end

"""
function del_nurbs_basis_ds
======

Returns d/ds dR/dxi
Or, return dR/ds/dxi
Or, in other words, derivative of dR/dxi w.r.t. the design parameter s
Or, in other words, d(del_nurbs_basis)/ds
"""
function del_nurbs_basis_ds(a::Int64,
                               degree::Int64,
                               C::Array{Float64,2},
                               weights::Array{Float64},
                               weights_ds::Array{Float64},
                               xi::Float64)

    N = b_spline_basis(a,degree,C,xi)
    dNdxi = del_b_spline_basis(a,degree,C,xi)

    w_N = weights[a] * N
    dwds_N = weights_ds[a] * N
    w_dNdxi = weights[a] * dNdxi
    dwds_dNdxi = weights_ds[a] * dNdxi

    sum_w_N = weights_sum_term(degree,C,weights,xi)
    sum_dwds_N = weights_sum_term(degree,C,weights_ds,xi)
    sum_w_dNdxi = del_weights_sum_term(degree,C,weights,xi)
    sum_dwds_dNdxi = del_weights_sum_term(degree,C,weights_ds,xi)

    G = w_dNdxi * sum_w_N - w_N * sum_w_dNdxi
    G_prime = w_dNdxi * sum_dwds_N + dwds_dNdxi * sum_w_N - w_N * sum_dwds_dNdxi - dwds_N * sum_w_dNdxi
    H = sum_w_N^2
    H_prime = 2 * sum_w_N * sum_dwds_N

    result = (G_prime * H - G * H_prime) / (H^2)

    return result
end


"""
function del_nurbs_basis_ds
======

* Multi-D version
Returns d/ds dR/dxi.
Or, in other words, dR/ds/dxi
Or, in other words, derivative of dR/dxi w.r.t. the design parameter s
Or, in other words, d(del_nurbs_basis)/ds
"""
function del_nurbs_basis_ds(a::Array{Int64},
                               degree::Array{Int64},
                               C::Array{Array{}},
                               weights::Array{Array{Float64},1},
                               weights_ds::Array{Array{Float64},1},
                               xi::Array{Float64})
    # Collect the R, dRds, dRdxi, dRdsdxi values
    dim_p = length(a)
    R = zeros(dim_p)
    dRds = zeros(dim_p)
    dRdxi = zeros(dim_p)
    dRdsdxi = zeros(dim_p)
    for dir = 1:dim_p
        R[dir] = nurbs_basis(a[dir],degree[dir],C[dir],weights[dir],xi[dir])
        dRds[dir] = nurbs_basis_ds(a[dir],degree[dir],C[dir],weights[dir],weights_ds[dir],xi[dir])
        dRdxi[dir] = del_nurbs_basis(a[dir],degree[dir],C[dir],weights[dir],xi[dir])
        dRdsdxi[dir] = del_nurbs_basis_ds(a[dir],degree[dir],C[dir],weights[dir],weights_ds[dir],xi[dir])
    end

    # Use the product rule to form the derivative
    result = zeros(dim_p)
    # Looping over each xi direction
    for xi_dir = 1:dim_p
        sumterm_result = 0.0
        #Looping over each sum term
        for s_dir = 1:dim_p
            prodterm_result = 1.0
            # Looping over each product term in the sum
            for xi_odir = 1:dim_p
                if(xi_odir == xi_dir)
                    if( s_dir == xi_odir )
                        prodterm_result *= dRdsdxi[xi_odir]
                    else
                        prodterm_result *= dRdxi[xi_odir]
                    end
                else
                    if( s_dir == xi_odir )
                        prodterm_result *= dRds[xi_odir]
                    else
                        prodterm_result *= R[xi_odir]
                    end
                end
            end
            sumterm_result += prodterm_result
        end
        result[xi_dir] = sumterm_result
    end
    return result
end


"""
function bez_ext_op_matrix
======
This function is for B-splines.  Returns the bezier extraction
operator for inserting a knot into the knot_vector

# Parameters:
* new_knot: The new knot to be inserted
* degree: The polynomial degree of the curve
* knot_v: The old knot vector

# Returns
* C,new_knot_v
"""
function bez_ext_op_matrix(new_knot::Float64,
                           degree::Int64,
                           knot_v::Array{Float64})

    # number of control points
    num_cp = length(knot_v)-degree-1

    #Init the new knot vector
    new_knot_v = zeros(length(knot_v)+1)

    #Insert the new knot into the knot vector
    old_i = 1
    new_i = 1
    new_knot_i = -1
    len_kv = length(knot_v)
    knot_added = false
    while(old_i <= len_kv)
        new_knot_v[new_i] = knot_v[old_i]
        if(new_knot >= knot_v[old_i] &&
                                old_i < len_kv &&
                                new_knot <= knot_v[old_i+1] &&
                                knot_added == false )
            new_i += 1
            new_knot_v[new_i] = new_knot
            new_knot_i = new_i
            knot_added = true
        end
        old_i += 1
        new_i += 1
    end

    # Init the bezExtOp Matrix
    n = num_cp
    m = num_cp + 1
    C = spzeros(n,m)

    #Fill in the new control points array
    k = new_knot_i-1
    p = degree
    for i = 1:m
        # Determine alpha
        if( i >= 1 && i <= k-p )
            alpha = 1.
        elseif( i == 1 || i == m )
            alpha = 0. # we don't use alpha in this case (some papers are wrong)
        elseif( i >= k-p+1 && i <= k )
            alpha = (new_knot - knot_v[i]) / (knot_v[i+p] - knot_v[i])
            if( (knot_v[i+p] - knot_v[i]) == 0 )
                println("ERROR!! denominator is zero!")
            end
        else
            alpha = 0.
        end

        # Determine the new points
        if( i == 1 )
            C[i,i] = 1. #"C[i,i] = alpha" found in some papers is wrong.
        elseif( i > 1 && i < m )
            C[i-1,i] = (1.0-alpha)
            C[i,i] = alpha
        elseif( i == m )
            C[i-1,i] = 1. #"C[i-1,i] = (1.-alpha)" found in some papers is wrong.
        end
    end

    # Return the results
    return C,new_knot_v
end

"""
function InsertKnot(new_knot::Float64,
                    degree::Int64,
                    knot_v::Array{Float64},
                    control_pts::Array{Float64,2})
======
This function is for B-splines.

# Parameters:
* new_knot: The new knot to be inserted
* degree: The polynomial degree of the curve
* knot_v: The old knot vector
* control_pts: The control points of the old spline
     (dimension d-by-n, for spatial dimenion d and for n control points)

# Returns
* new_knot_v,new_control_pts
"""
function InsertKnot(new_knot::Float64,
                    degree::Int64,
                    knot_v::Array{Float64},
                    control_pts::Array{Float64,2})

    #Init the new knot vector
    new_knot_v = zeros(length(knot_v)+1)

    #Insert the new knot into the knot vector
    old_i = 1
    new_i = 1
    new_knot_i = -1
    len_kv = length(knot_v)
    knot_added = false
    while(old_i <= len_kv)
        new_knot_v[new_i] = knot_v[old_i]
        if( new_knot >= knot_v[old_i] &&
                                old_i < len_kv &&
                                new_knot <= knot_v[old_i+1] &&
                                knot_added == false)
            new_i += 1
            new_knot_v[new_i] = new_knot
            new_knot_i = new_i
            knot_added = true
        end
        old_i += 1
        new_i += 1
    end

    # Init the new control points array
    n = nothing
    m = nothing
    new_control_pts = nothing
    if(length(size(control_pts)) == 1)
        n = length(control_pts)
        m = length(control_pts)+1
        new_control_pts = Array{Float64}(m)
    else
        n = size(control_pts)[2]
        m = size(control_pts)[2]+1
        new_control_pts = Array{Float64}(undef,size(control_pts)[1],m)
    end

    # Fill in the new control points array
    k = new_knot_i-1
    p = degree
    for i = 1:m
        # Determine alpha
        if( i >= 1 && i <= k-p )
            alpha = 1.
        elseif( i == 1 || i == m )
            alpha = 0. # we don't use alpha in this case (some papers are wrong)
        elseif( i >= k-p+1 && i <= k )
            alpha = (new_knot - knot_v[i]) / (knot_v[i+p] - knot_v[i])
            if( (knot_v[i+p] - knot_v[i]) == 0 )
                println("ERROR!! denominator is zero!")
            end
        else
            alpha = 0.
        end

        # Determine the new points
        if( i == 1 )
            new_control_pts[:,i] = control_pts[:,i]
        elseif( i > 1 && i < m )
            new_control_pts[:,i] = alpha*control_pts[:,i] + (1.0-alpha)*control_pts[:,i-1]
        elseif( i == m )
            new_control_pts[:,i] = control_pts[:,n]
        end
    end

    # Return the results
    return new_knot_v,new_control_pts
end

"""
function InsertKnot(new_knot::Float64,
                    degree::Int64,
                    knot_v::Array{Float64},
                    control_pts::Array{Float64,2},
                    weights::Array{Float64})
======
This function is for NURBS (includes weights).

# Parameters
* new_knot: The new knot to be inserted
* degree: The polynomial degree of the curve
* knot_v: The old knot vector
* control_pts: The control points of the old spline
     (dimension d-by-n, for spatial dimenion d and for n control points)
* weights: The wieghts for each NURBS control point

# Returns
new_knot_v, new_control_pts, new_weights
"""
function InsertKnot(new_knot::Float64,
                    degree::Int64,
                    knot_v::Array{Float64},
                    control_pts::Array{Float64,2},
                    weights::Array{Float64})

    # Project to the higher-dimensional space.
    dim_s = size(control_pts)[1]
    n_cont_pts = size(control_pts)[2]
    nurbs_control_pts = Array{Float64}(dim_s+1,n_cont_pts)
    for row = 1:dim_s
        nurbs_control_pts[row,:] = control_pts[row,:][:].*weights[:]
    end
    nurbs_control_pts[dim_s+1,:] = weights[:]

    # Insert the knot
    new_knot_v, new_nurbs_control_pts = InsertKnot(new_knot,degree,knot_v,nurbs_control_pts)

    # Convert back to NURBS
    new_weights = new_nurbs_control_pts[dim_s+1,:][:]
    new_control_pts = Array{Float64}(dim_s,size(new_nurbs_control_pts)[2])
    for row = 1:dim_s
        new_control_pts[row,:] = new_nurbs_control_pts[row,:][:] ./ new_weights[:]
    end

    # Return the result
    return new_knot_v, new_control_pts, new_weights
end

"""
function InsertKnot(new_knot::Float64,
                    degree::Int64,
                    knot_v::Array{Float64},
                    control_pts::Array{Float64,2},
                    weights::Array{Float64},
                    control_pts_ds::Array{Float64,2},
                    weights_ds::Array{Float64})
======
This function is for NURBS (includes weights), and DERIVATIVE w.r.t. a design variable.

# Parameters
* new_knot: The new knot to be inserted
* degree: The polynomial degree of the curve
* knot_v: The old knot vector
* control_pts: The control points of the old spline
     (dimension d-by-n, for spatial dimenion d and for n control points)
* weights: The wieghts for each NURBS control point
* control_pts_ds: The derivative of the control points of the old spline
     with respect to one of the design parameters.
     (dimension d-by-n, for spatial dimenion d and for n control points)
* weights_ds: The derivative of the wieghts for each NURBS control point
     with respect to one of the design parameters.

# Returns
new_knot_v, new_control_pts, new_weights
"""
function InsertKnot(new_knot::Float64,
                    degree::Int64,
                    knot_v::Array{Float64},
                    control_pts::Array{Float64,2},
                    weights::Array{Float64},
                    control_pts_ds::Array{Float64,2},
                    weights_ds::Array{Float64})

    # Project to the higher-dimensional space.
    dim_s = size(control_pts)[1]
    n_cont_pts = size(control_pts)[2]
    bspline_control_pts = Array{Float64}(undef,dim_s+1,n_cont_pts)
    bspline_control_pts_ds = Array{Float64}(undef,dim_s+1,n_cont_pts)
    for row = 1:dim_s
        bspline_control_pts[row,:] = control_pts[row,:][:].*weights[:]
        # Using the Product rule
        bspline_control_pts_ds[row,:] = control_pts_ds[row,:][:].*weights[:] + control_pts[row,:][:].*weights_ds[:]
    end
    bspline_control_pts[dim_s+1,:] = weights[:]
    bspline_control_pts_ds[dim_s+1,:] = weights_ds[:]

    # Insert the knot
    new_knot_v, new_bspline_control_pts = InsertKnot(new_knot, degree, knot_v, bspline_control_pts)
    new_knot_v, new_bspline_control_pts_ds = InsertKnot(new_knot, degree, knot_v, bspline_control_pts_ds)

    # Convert back to NURBS
    # value
    new_weights = new_bspline_control_pts[dim_s+1,:][:]
    new_control_pts = Array{Float64}(undef,dim_s,size(new_bspline_control_pts)[2])
    # derivative
    new_weights_ds = new_bspline_control_pts_ds[dim_s+1,:][:]
    new_control_pts_ds = Array{Float64}(undef,dim_s,size(new_bspline_control_pts)[2])
    for row = 1:dim_s
        new_control_pts[row,:] = new_bspline_control_pts[row,:][:] ./ new_weights[:]
        new_control_pts_ds[row,:] = (new_bspline_control_pts_ds[row,:][:] .* new_weights[:] -
                                new_bspline_control_pts[row,:][:] .* new_weights_ds[:]) ./ ((new_weights[:]).^2)
    end

    # Return the result
    return new_knot_v, new_control_pts, new_weights, new_control_pts_ds, new_weights_ds
end









#####***********************************
#####***********************************
#####***********************************
#####***********************************
#####***********************************
#####***********************************
#####***********************************
#####***********************************
#####***********************************
#####***********************************
#####***********************************
#####***********************************
#####***********************************
#####***********************************












"""
struct Nurbs
==========

# Member Variables
* dim_p: Parametric dimension
* dim_s: Spatial dimension
* degree: The polynomial degree (degree) of the curve in each parametric dimension
* knot_v: The knot vector in each parametric dimension
* spans: The number of 'spans' between control points in each parametric direction.
            Or, if n is the number of control points, then spans = n .- 1 in each
            paramteric dimension.  I don't remember why I didn't just save the number
            of control points in each direction but there was a good reason, I'm sure.
* control_pts: The array of control points. An array of dimension
            dim_s x ncpt1 x ncpt2...  where ncpt1,ncpt2, etc is the number of
            control points in each paramteric direction.
* control_pts_ds:  The sensitivity of the control points with respect to the
            design parameters s.
* weights: Array of arrays, containing the array of weights for each paramter
            direction.
* weights_ds: The sensitivity of the weights with respect to the design
            parameters s.
* weights_el: The weights associated with each element (or bezier curve) on
            the patch.
* weights_el_ds:  The sensitivity of the weights with respect to the design
            parameters s, for each element (or bezier curve) on the patch.
* ndesignvars: The number of design variables (length of array 's')
* nnodesperknotspan:  The number of nodes associated with each element (or
            or bezier curve), in each parametric direction.
            TODO: This should have its name changed to 'nnodesperbezcurve'
            since now we have an element for each bezier curve including
            the degenate ones.
* nbezcurves:  The number of elements (or bezier curves) in each parametric
            direction.

* TODO: finish the documentation.
"""
mutable struct Nurbs

    dim_p::Int64
    dim_s::Int64
    degree::Array{Int64}
    knot_v::Array{Array{}}
    spans::Array{Int64}

    # Evaluated design
    control_pts::Array{Float64}
    control_pts_ds::Array{Array{Float64}}
    weights::Array{Array{Float64}}
    weights_ds::Array{Array{Array{Float64}}}

    weights_el::Array{Any}
    weights_el_ds::Array{Any}

    ndesignvars::Int64

    # Characteristics of the curve
    nnodesperknotspan::Array{Int64}
    nbezcurves::Array{Int64}

    # Unique knots in each parametric direction (array of Float64-array)
    #uniqueknots::Array{Array{}}

    # Bezier Extraction Operators
    C::Array{SparseMatrixCSC{},1}

    # The element bezier extraction operators
    C_el::Array{Any}

    # Control points for each knot-span (element)
    # both actual control points (IEN) and bernstein (BBIEN)
    IEN::Array{Array{}}
    BBIEN::Array{Array{}}

    # Index of element basis that corresponds to a global basis index
    # (Global Basis Element Basis Index)
    GBEBI::Array{Array{}}

    # For speed improvement, save-off often needed dxdxi values
    use_value_dictionaries::Bool
    basis_dict::Array{Dict{Any,Any}}
    basis_ds_dict::Array{Dict{Any,Any}}
    x_dict::Array{Dict{Any,Any}}
    dxdxi_dict::Array{Dict{Any,Any}}
    dxdsdxi_dict::Array{Dict{Any,Any}}
    delbasis_dict::Array{Dict{Any,Any}}
    delbasis_ds_dict::Array{Dict{Any,Any}}

    function Nurbs(dim_p::Int64,
                   dim_s::Int64,
                   degree::Array{Int64},
                   knot_v::Array{Array{}},
                   spans::Array{Int64},
                   control_pts::Union{Array{Function},Array{Float64}},
                   weights::Union{Array{Array{Function}},Array{Array{Float64}}};
                   s::Array{Float64}=[0.],
                   use_value_dictionaries=true)
        # Set the variables
        self = new()
        self.dim_p = copy(dim_p)
        self.dim_s = copy(dim_s)
        self.degree = copy(degree)
        self.knot_v = copy(knot_v)
        self.spans = copy(spans)

        # Flag to use the value dictionaries (only use when the same values
        # are asked for over and over.)
        self.use_value_dictionaries = use_value_dictionaries

        # Convert inputs to functions if needed
        if(typeof(control_pts[1]) == Float64)
            control_pts_f = Array{Function}(undef,size(control_pts))
            for (i,cp) in enumerate(control_pts)
                control_pts_f[i] = function(s) control_pts[i] end
            end
        else
            control_pts_f = control_pts
        end
        if(typeof(weights[1][1]) == Float64)
            weights_f = Array{Array{Function}}(undef,self.dim_p)
            for dir = 1:self.dim_p
                weights_f[dir] = Array{Function}(undef,size(weights[dir]))
                for (i,w) in enumerate(weights[dir])
                    weights_f[dir][i] = function(s) weights[dir][i] end
                end
            end
        else
            weights_f = weights
        end

        # Init the design
        init_design(self,control_pts_f,weights_f,s)

        # Init special arrays (IEN, BBIEN, etc)
        init_special_arrays(self)

        # For speed improvement, save-off often needed basis values
        init_saveoff_dicts(self)

        return self
    end

    function Nurbs(other::Nurbs)
        # Set the variables
        self = new()
        self.dim_p = copy(other.dim_p)
        self.dim_s = copy(other.dim_s)
        self.degree = copy(other.degree)
        self.knot_v = copy(other.knot_v)
        self.spans = copy(other.spans)
        self.control_pts = copy(other.control_pts)
        self.control_pts_ds = copy(other.control_pts_ds)
        for i in 1:length(other.control_pts_ds)
            self.control_pts_ds[i] = copy(other.control_pts_ds[i])
        end
        self.weights = copy(other.weights)
        self.weights_ds = copy(other.weights_ds)
        for i in 1:length(other.weights_ds)
            self.weights_ds[i] = copy(other.weights_ds[i])
        end
        self.ndesignvars = copy(other.ndesignvars)
        self.nnodesperknotspan = copy(other.nnodesperknotspan)
        self.nbezcurves = copy(other.nbezcurves)
        self.use_value_dictionaries = copy(other.use_value_dictionaries)
        #self.uniqueknots = copy(other.uniqueknots)
        init_special_arrays(self)
        init_saveoff_dicts(self)
        return self
    end

end


function init_saveoff_dicts(self::Nurbs)
    # For speed improvement, save-off often needed basis values
    if(self.dim_p == 1)
        self.basis_dict = Array{Dict{Any,Any}}(undef,self.nbezcurves[1])
        for e1 = 1:self.nbezcurves[1]
            self.basis_dict[e1] = Dict()
        end
        self.basis_ds_dict = Array{Dict{Any,Any}}(undef,self.nbezcurves[1])
        for e1 = 1:self.nbezcurves[1]
            self.basis_ds_dict[e1] = Dict()
        end
        self.x_dict = Array{Dict{Any,Any}}(undef,self.nbezcurves[1])
        for e1 = 1:self.nbezcurves[1]
            self.x_dict[e1] = Dict()
        end
        self.dxdxi_dict = Array{Dict{Any,Any}}(undef,self.nbezcurves[1])
        for e1 = 1:self.nbezcurves[1]
            self.dxdxi_dict[e1] = Dict()
        end
        self.dxdsdxi_dict = Array{Dict{Any,Any}}(undef,self.nbezcurves[1])
        for e1 = 1:self.nbezcurves[1]
            self.dxdsdxi_dict[e1] = Dict()
        end
        self.delbasis_dict = Array{Dict{Any,Any}}(undef,self.nbezcurves[1])
        for e1 = 1:self.nbezcurves[1]
            self.delbasis_dict[e1] = Dict()
        end
        self.delbasis_ds_dict = Array{Dict{Any,Any}}(undef,self.nbezcurves[1])
        for e1 = 1:self.nbezcurves[1]
            self.delbasis_ds_dict[e1] = Dict()
        end
    elseif(self.dim_p == 2)
        self.basis_dict = Array{Dict{Any,Any},2}(undef,self.nbezcurves[1],self.nbezcurves[2])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                self.basis_dict[e1,e2] = Dict()
            end
        end
        self.basis_ds_dict = Array{Dict{Any,Any},2}(undef,self.nbezcurves[1],self.nbezcurves[2])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                self.basis_ds_dict[e1,e2] = Dict()
            end
        end
        self.x_dict = Array{Dict{Any,Any},2}(undef,self.nbezcurves[1],self.nbezcurves[2])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                self.x_dict[e1,e2] = Dict()
            end
        end
        self.dxdxi_dict = Array{Dict{Any,Any},2}(undef,self.nbezcurves[1],self.nbezcurves[2])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                self.dxdxi_dict[e1,e2] = Dict()
            end
        end
        self.dxdsdxi_dict = Array{Dict{Any,Any},2}(undef,self.nbezcurves[1],self.nbezcurves[2])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                self.dxdsdxi_dict[e1,e2] = Dict()
            end
        end
        self.delbasis_dict = Array{Dict{Any,Any},2}(undef,self.nbezcurves[1],self.nbezcurves[2])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                self.delbasis_dict[e1,e2] = Dict()
            end
        end
        self.delbasis_ds_dict = Array{Dict{Any,Any},2}(undef,self.nbezcurves[1],self.nbezcurves[2])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                self.delbasis_ds_dict[e1,e2] = Dict()
            end
        end
    elseif(self.dim_p == 3)
        self.basis_dict = Array{Dict{Any,Any},3}(undef,self.nbezcurves[1],self.nbezcurves[2],self.nbezcurves[3])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                for e3 = 1:self.nbezcurves[3]
                    self.basis_dict[e1,e2,e3] = Dict()
                end
            end
        end
        self.basis_ds_dict = Array{Dict{Any,Any},3}(undef,self.nbezcurves[1],self.nbezcurves[2],self.nbezcurves[3])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                for e3 = 1:self.nbezcurves[3]
                    self.basis_ds_dict[e1,e2,e3] = Dict()
                end
            end
        end
        self.x_dict = Array{Dict{Any,Any},3}(undef,self.nbezcurves[1],self.nbezcurves[2],self.nbezcurves[3])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                for e3 = 1:self.nbezcurves[3]
                    self.x_dict[e1,e2,e3] = Dict()
                end
            end
        end
        self.dxdxi_dict = Array{Dict{Any,Any},3}(undef,self.nbezcurves[1],self.nbezcurves[2],self.nbezcurves[3])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                for e3 = 1:self.nbezcurves[3]
                    self.dxdxi_dict[e1,e2,e3] = Dict()
                end
            end
        end
        self.dxdsdxi_dict = Array{Dict{Any,Any},3}(undef,self.nbezcurves[1],self.nbezcurves[2],self.nbezcurves[3])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                for e3 = 1:self.nbezcurves[3]
                    self.dxdsdxi_dict[e1,e2,e3] = Dict()
                end
            end
        end
        self.delbasis_dict = Array{Dict{Any,Any},3}(undef,self.nbezcurves[1],self.nbezcurves[2],self.nbezcurves[3])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                for e3 = 1:self.nbezcurves[3]
                    self.delbasis_dict[e1,e2,e3] = Dict()
                end
            end
        end
        self.delbasis_ds_dict = Array{Dict{Any,Any},3}(undef,self.nbezcurves[1],self.nbezcurves[2],self.nbezcurves[3])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                for e3 = 1:self.nbezcurves[3]
                    self.delbasis_ds_dict[e1,e2,e3] = Dict()
                end
            end
        end
    end
end



"""
function initBezExtOpForEl
====

Inits the bezier extraction operator for the given element, and returns it
"""
function initBezExtOpForEl(self::Nurbs,e::Array{Int64})
    C_e = Array{Array{}}(undef,self.dim_p)
    for dim_p_i = 1:self.dim_p
        elnodes = self.IEN[dim_p_i][:,e[dim_p_i]]
        bbelnodes = self.BBIEN[dim_p_i][:,e[dim_p_i]]
        C_e[dim_p_i] = self.C[dim_p_i][elnodes,bbelnodes]
    end
    return C_e
end


"""
function initWeightsForEl
====

Inits the weights array for a given element, and returns it
"""
function initWeightsForEl(self::Nurbs,e::Array{Int64},weights::Array{Array{Float64}})
    weights_e = Array{Array{Float64}}(undef,self.dim_p)
    for dim_p_i = 1:self.dim_p
        elnodes = self.IEN[dim_p_i][:,e[dim_p_i]]
        weights_e[dim_p_i] = weights[dim_p_i][elnodes]
    end
    return weights_e
end


"""
function init_special_arrays
====

Inits all data structures dependent on the knot vector
(to be called after the knot vector is created or modified)
"""
function init_special_arrays(self::Nurbs)

    # Set other variables
    self.nnodesperknotspan = self.degree .+ 1
    #self.uniqueknots = Array{Array{}}(self.dim_p)
    self.nbezcurves = Array{Int64}(undef,self.dim_p)
    for dir = 1:self.dim_p
        #self.uniqueknots[dir] = UniqueKnotArray(self.knot_v[dir])
        self.nbezcurves[dir] = length(self.knot_v[dir]) - 2*self.degree[dir] - 1
    end

    # Init IEN and BBIEN
    self.IEN = Array{Array{}}(undef,self.dim_p)
    for dir = 1:self.dim_p
        self.IEN[dir] = zeros(Int64,self.nnodesperknotspan[dir],self.nbezcurves[dir])
        for e = 1:self.nbezcurves[dir]
            self.IEN[dir][:,e] = IsogeometricIEN(e,self.degree[dir])
        end
    end
    self.BBIEN = Array{Array{}}(undef,self.dim_p)
    for dir = 1:self.dim_p
        self.BBIEN[dir] = zeros(Int64,self.nnodesperknotspan[dir],self.nbezcurves[dir])
        for e = 1:self.nbezcurves[dir]
            self.BBIEN[dir][:,e] = LagrangeIEN(e,self.degree[dir])
        end
    end

    # GBEBI array (Global Basis' Element Basis Index)
    # The row is the element index, the column is the global basis index,
    #   and the value is the local element basis index corresponding to the global basis.
    self.GBEBI = Array{Array{}}(undef,self.dim_p)
    for dir = 1:self.dim_p
        self.GBEBI[dir] = zeros(Int64,self.nbezcurves[dir],self.spans[dir]+1)
        for e = 1:self.nbezcurves[dir]
            for glob_b = 1:self.spans[dir]+1
                findf(i) = i==glob_b
                local_index = findall(findf,self.IEN[dir][:,e])
                if(length(local_index)==0)
                    self.GBEBI[dir][e,glob_b] = 0
                else
                    self.GBEBI[dir][e,glob_b] = local_index[1]
                end
            end
        end
    end

    # Bezier Extraction Operators
    self.C = CreateBezierExtractionOperator(self)

    # Bezier Ex for each element
    if(self.dim_p == 1)
        self.C_el = Array{Any}(undef,self.nbezcurves[1])
        for e1 = 1:self.nbezcurves[1]
            self.C_el[e1] = initBezExtOpForEl(self,[e1])
        end
    elseif(self.dim_p == 2)
        self.C_el = Array{Any}(undef,self.nbezcurves[1],self.nbezcurves[2])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                self.C_el[e1,e2] = initBezExtOpForEl(self,[e1,e2])
            end
        end
    elseif(self.dim_p == 3)
        self.C_el = Array{Any}(undef,self.nbezcurves[1],self.nbezcurves[2],self.nbezcurves[3])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                for e3 = 1:self.nbezcurves[3]
                    self.C_el[e1,e2,e3] = initBezExtOpForEl(self,[e1,e2,e3])
                end
            end
        end
    end

    # weights_el
    if(self.dim_p == 1)
        self.weights_el = Array{Any}(undef,self.nbezcurves[1])
        for e1 = 1:self.nbezcurves[1]
            self.weights_el[e1] = initWeightsForEl(self,[e1],self.weights)
        end
    elseif(self.dim_p == 2)
        self.weights_el = Array{Any}(undef,self.nbezcurves[1],self.nbezcurves[2])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                self.weights_el[e1,e2] = initWeightsForEl(self,[e1,e2],self.weights)
            end
        end
    elseif(self.dim_p == 3)
        self.weights_el = Array{Any}(undef,self.nbezcurves[1],self.nbezcurves[2],self.nbezcurves[3])
        for e1 = 1:self.nbezcurves[1]
            for e2 = 1:self.nbezcurves[2]
                for e3 = 1:self.nbezcurves[3]
                    self.weights_el[e1,e2,e3] = initWeightsForEl(self,[e1,e2,e3],self.weights)
                end
            end
        end
    end

    # weights_el_ds
    self.weights_el_ds = Array{Any}(undef,self.ndesignvars)
    for s_dir = 1:self.ndesignvars
        if(self.dim_p == 1)
            self.weights_el_ds[s_dir] = Array{Any}(undef,self.nbezcurves[1])
            for e1 = 1:self.nbezcurves[1]
                self.weights_el_ds[s_dir][e1] = initWeightsForEl(self,[e1],self.weights_ds[s_dir])
            end
        elseif(self.dim_p == 2)
            self.weights_el_ds[s_dir] = Array{Any}(undef,self.nbezcurves[1],self.nbezcurves[2])
            for e1 = 1:self.nbezcurves[1]
                for e2 = 1:self.nbezcurves[2]
                    self.weights_el_ds[s_dir][e1,e2] = initWeightsForEl(self,[e1,e2],self.weights_ds[s_dir])
                end
            end
        elseif(self.dim_p == 3)
            self.weights_el_ds[s_dir] = Array{Any}(undef,self.nbezcurves[1],self.nbezcurves[2],self.nbezcurves[3])
            for e1 = 1:self.nbezcurves[1]
                for e2 = 1:self.nbezcurves[2]
                    for e3 = 1:self.nbezcurves[3]
                        self.weights_el_ds[s_dir][e1,e2,e3] = initWeightsForEl(self,[e1,e2,e3],self.weights_ds[s_dir])
                    end
                end
            end
        end
    end

    return
end




"""
function init_design
========
"""
function init_design(self::Nurbs,
                     control_pts_f::Array{Function},
                     weights_f::Array{Array{Function}},
                     s::Array{Float64})

    zero_complex_part = function(c::ComplexF64)
        return c - imag(c)*im
    end
    set_complex_h = function(c::ComplexF64,h::Float64)
        val = zero_complex_part(c)
        val = val + h*im
        return val
    end

    # make sure there is at least one design variable
    # even if actually none (the algorithm assumes at least one)
    if(length(s) == 0)
        s = Array{Float64}([0.0])
    end

    h = 1.0e-40
    self.ndesignvars = length(s)

    # Init the arrays
    self.control_pts = zeros(Float64,size(control_pts_f))
    self.control_pts_ds = Array{Array{Float64}}(undef,length(s))
    for j = 1:length(s)
        self.control_pts_ds[j] = zeros(Float64,size(control_pts_f))
    end
    self.weights = Array{Array{Float64}}(undef,self.dim_p)
    for i = 1:self.dim_p
        self.weights[i] = zeros(Float64,size(weights_f[i]))
    end
    self.weights_ds = Array{Array{Array{Float64}}}(undef,length(s))
    for j = 1:length(s)
        self.weights_ds[j] = Array{Array{Float64}}(undef,self.dim_p)
        for i = 1:self.dim_p
            self.weights_ds[j][i] = zeros(Float64,size(weights_f[i]))
        end
    end

    s_cplx = convert(Array{ComplexF64},s)

    # Fill in the arrays
    for (j, cpf) in enumerate(control_pts_f)
        for k = 1:length(s)
            s_cplx[k] = set_complex_h(s_cplx[k],h)
            val = cpf(s_cplx)
            self.control_pts[j] = real(val)
            self.control_pts_ds[k][j] = imag(val)/h
            s_cplx[k] = zero_complex_part(s_cplx[k])
        end
    end
    for i = 1:self.dim_p
        for (j,wf) in enumerate(weights_f[i])
            for k = 1:length(s)
                s_cplx[k] = set_complex_h(s_cplx[k],h)
                val = wf(s_cplx)
                self.weights[i][j] = real(val)
                self.weights_ds[k][i][j] = imag(val)/h
                s_cplx[k] = zero_complex_part(s_cplx[k])
            end
        end
    end

    return
end





"""
function RefineMesh(self::Nurbs,n_knotspans::Array{Int64})
========

Modifies "nurbs" by inserting knots until n_knotspans
knot spans exist in the span of the spline.

# Parameters:
* self::Nurbs
* n_knotspans::Array{Int64}  Size dim_p array

# Returns:
* Pointer to the same Nurbs object that was passed in, now modified.
"""
function RefineMesh(self::Nurbs,n_knotspans::Array{Int64})

    for dir = 1:self.dim_p
        first_knot = self.knot_v[dir][1]
        last_knot = self.knot_v[dir][end]
        knotvals = range(first_knot, stop=last_knot, length=n_knotspans[dir]+1)
        p = self.degree[dir]

        # Insert the interior knots
        len_knotvals = length(knotvals)
        n_inserted = 0
        for i = 2:len_knotvals-1
            if(Multiplicity(self.knot_v[dir],knotvals[i]) == 0)
                n_inserted += 1
                if(mod(n_inserted,50) == 0)
                    println("RefineMesh: Inserting knot number ",n_inserted)
                end
                ## TODO: call 'InsertMultipleKnots' here (after it's written) to make
                ##   adding all these knots faster.
                InsertKnot(self,dir,knotvals[i];reinit_dep_arrs=false)
            end
        end
    end

    # These need to be re-initialized after the knot vector is modified.
    init_special_arrays(self)
    init_saveoff_dicts(self)

    GC.gc()
    return self
end








"""
function ConvertToBernsteinMesh(self::Nurbs)
=================

Modifies self
by repeating each internal knot until there exists 'n_elements-1'
interior knots, each repeated p times, which creates a bernstein
basis for each element in the domain. (Also referred to as
bezier decomposition).

# Parameters:
* nurbs::Nurbs

# Returns:
* Pointer to self.
"""
function ConvertToBernsteinMesh(self::Nurbs)

    for dir = 1:self.dim_p
        orig_knot_v = copy(self.knot_v[dir])
        p = self.degree[dir]

        left_i,right_i = secondToEndKnotIndices(self.knot_v[dir])
        bound_left = left_i
        bound_right = length(self.knot_v[dir])-right_i+1

        # All interior knot values that define the bezier curves
        knotvals = UniqueKnotArray(self.knot_v[dir][bound_left:bound_right])

        # Insert the interior knots
        for i = 1:length(knotvals)
            num_already = Multiplicity(orig_knot_v[bound_left:bound_right],knotvals[i])
            for j = num_already+1:(p*num_already)
                InsertKnot(self,dir,knotvals[i];reinit_dep_arrs=false)
            end
        end

    end

    init_special_arrays(self)
    init_saveoff_dicts(self)

    GC.gc()
    return self
end



"""
function CreateBezierExtractionOperator(self::Nurbs)
========
Creates a bezier extraction operator, one for each dim_p direction

Essentially, the operator maps the basis functions in
the bezier-decomposed mesh to the basis functions in
the original Nurbs mesh.

# Parameters:
* self::Nurbs

# Returns:
* Array of Bezier Extraction operators (Array of sparse matrices)
"""
function CreateBezierExtractionOperator(self::Nurbs)

    # Array of sparse matricies, of length dim_p
    C = Array{SparseMatrixCSC{},1}(undef,self.dim_p)

    for dir = 1:self.dim_p

        p = self.degree[dir]

        # Expanded knot vector, lengthened a bit for cases of incomplete knot vectors
        left_i,right_i = secondToEndKnotIndices(self.knot_v[dir])
        extra_left = max((p+2) - left_i,0)
        extra_right = max((p+2) - right_i,0)
        exp_knot_v = zeros(length(self.knot_v[dir])+extra_left+extra_right)
        exp_knot_v[extra_left+1:extra_left+length(self.knot_v[dir])] = self.knot_v[dir]
        for i = 1:extra_left
            exp_knot_v[i] = self.knot_v[dir][1]
        end
        for i = length(exp_knot_v):-1:length(exp_knot_v)-extra_right
            exp_knot_v[i] = self.knot_v[dir][end]
        end

        # The number of 'control points' in the temporarily expanded knot vector
        size_c_init = length(exp_knot_v) - p - 1

        # This will be the bezier extraction operator when we're done
        C[dir] = SparseMatrixCSC{Float64}(I, size_c_init, size_c_init)

        # All interior knot values that define the bezier curves
        knotvals = UniqueKnotArray(exp_knot_v[p+2:end-p-1])

        # copy for use in creating C
        knot_v_cpy = Array(exp_knot_v)

        for i = 1:length(knotvals)
            num_already = Multiplicity(exp_knot_v[p+2:end-p-1],knotvals[i])
            for j = num_already+1:(p*num_already)
                new_knot = knotvals[i]
                C1,knot_v_cpy = bez_ext_op_matrix(new_knot,p,knot_v_cpy)
                C[dir] = (sparse(C1)'*sparse(C[dir])')'

                #println(replace(string(full(C[dir])),";","\n"))
            end
        end

        C[dir] = C[dir][ extra_left+1:end-extra_right, extra_left*p+1:end-extra_right*p ]
    end

    return C
end


"""
function CreateWeightsMatrix(nurbs::Nurbs)
========
Creates the Weights matrix (see Bez Ext paper equation 21)
"""
function CreateWeightsMatrix(self::Nurbs)
    W = Array{SparseMatrixCSC{},1}(self.dim_p)
    for dir = 1:self.dim_p
        W[dir] = spdiagm(self.weights[dir])
    end
    return W
end




"""
function dict_has_key(dict,e,key)
======

Helper function
"""
function dict_has_key(self::Nurbs,dict,e,key)
    if(self.use_value_dictionaries)
        if(length(e)==1)
            return haskey(dict[e[1]],key)
        elseif(length(e)==2)
            return haskey(dict[e[1],e[2]],key)
        end
    end
    return false
end
"""
function dict_get_value(dict,e,key)
======

Helper function
"""
function dict_get_value(self::Nurbs,dict,e,key)
    if(self.use_value_dictionaries)
        if(length(e)==1)
            return dict[e[1]][key]
        elseif(length(e)==2)
            return dict[e[1],e[2]][key]
        end
    end
    println("Error: use_value_dictionaries is false!")
    exit()
    return 0
end
"""
function dict_set_value(dict,e,key,value)
======

Helper function
"""
function dict_set_value(self::Nurbs,dict,e,key,value)
    if(self.use_value_dictionaries)
        if(length(e)==1)
            dict[e[1]][key] = value
        elseif(length(e)==2)
            dict[e[1],e[2]][key] = value
        end
    end
end



"""
function bezExtOpForEl(self::Nurbs,e::Array{Int64})
======

Helper function - returns the C_el matrix for the given element
(Assumes C_el has already been initialized)
"""
function bezExtOpForEl(self::Nurbs,e::Array{Int64})
    returnval = nothing
    if(self.dim_p == 1)
        returnval = self.C_el[e[1]]
    elseif(self.dim_p == 2)
        returnval = self.C_el[e[1],e[2]]
    elseif(self.dim_p == 3)
        returnval = self.C_el[e[1],e[2],e[3]]
    end
    return returnval
end



"""
function weightsForEl
====

Returns the weights array for a given element
"""
function weightsForEl(self::Nurbs,e::Array{Int64})
    returnval = nothing
    if(self.dim_p == 1)
        returnval = self.weights_el[e[1]]
    elseif(self.dim_p == 2)
        returnval = self.weights_el[e[1],e[2]]
    elseif(self.dim_p == 3)
        returnval = self.weights_el[e[1],e[2],e[3]]
    end
    return returnval
end




"""
function weightsForEl_ds
====

Returns the weights array for a given element
"""
function weightsForEl_ds(self::Nurbs,e::Array{Int64},s_dir::Int64)
    returnval = nothing
    if(self.dim_p == 1)
        returnval = self.weights_el_ds[s_dir][e[1]]
    elseif(self.dim_p == 2)
        returnval = self.weights_el_ds[s_dir][e[1],e[2]]
    elseif(self.dim_p == 3)
        returnval = self.weights_el_ds[s_dir][e[1],e[2],e[3]]
    end
    return returnval
end




"""
function controlPtsForEl
====

Returns the weights array for a given element
"""
function controlPtsForEl(self::Nurbs,e::Array{Int64},control_pts::Array{Float64})
    control_pts_e = nothing
    if(self.dim_p == 1)
        control_pts_e = Array{Float64}(undef,self.dim_s,self.degree[1]+1)
        elnodes_1 = self.IEN[1][:,e[1]]
        control_pts_e[:,:] = control_pts[:,elnodes_1]
    elseif(self.dim_p == 2)
        control_pts_e = Array{Float64}(undef,self.dim_s,self.degree[1]+1,self.degree[2]+1)
        elnodes_1 = self.IEN[1][:,e[1]]
        elnodes_2 = self.IEN[2][:,e[2]]
        control_pts_e[:,:,:] = control_pts[:,elnodes_1,elnodes_2]
    elseif(self.dim_p == 3)
        control_pts_e = Array{Float64}(undef,self.dim_s,self.degree[1]+1,self.degree[2]+1,self.degree[3]+1)
        elnodes_1 = self.IEN[1][:,e[1]]
        elnodes_2 = self.IEN[2][:,e[2]]
        elnodes_3 = self.IEN[3][:,e[3]]
        control_pts_e[:,:,:,:] = control_pts[:,elnodes_1,elnodes_2,elnodes_3]
    end
    return control_pts_e
end




"""
function t_range_for_e
=====

Returns the min and max t values for element e.
Returns T_RANGE, an array of dimension 2-by-dim_p
"""
function t_range_for_e(self::Nurbs,e::Array{Int64})
    T_RANGE = zeros(2,self.dim_p)
    for dir = 1:self.dim_p
        knotvec = self.knot_v[dir]
        i0 = self.degree[dir]+1
        T_RANGE[1,dir] = knotvec[i0]
        T_RANGE[2,dir] = knotvec[i0+1]
    end
    return T_RANGE
end




"""
function e_xi_for_t
=====

Returns the e,xi for parameter t
"""
function e_xi_for_t(self::Nurbs,t::Array{Float64})

    # Using a binary search to identify the element, xi value

    e = zeros(Int64,self.dim_p)
    xi = zeros(self.dim_p)

    for dir = 1:self.dim_p
        knotvec = self.knot_v[dir]
        i0 = self.degree[dir]+1
        iend = length(knotvec)-self.degree[dir]
        imid = i0 + div((iend-i0),2)+1
        while(iend-i0 > 4)
            if(t[dir] <= knotvec[imid])
                iend = imid
            else
                i0 = imid
            end
            imid = i0 + div((iend-i0),2)+1
        end
        i_found = 0
        for i_looking = i0:iend-1
            i_found = i_looking
            if(knotvec[i_looking] != knotvec[i_looking+1] &&
                           t[dir] >= knotvec[i_looking] &&
                           t[dir] <= knotvec[i_looking+1])
                break
            end
        end
        xi[dir] = 2.0 * ((t[dir]-knotvec[i_found])/(knotvec[i_found+1]-knotvec[i_found])) - 1.0
        e[dir] = i_found-self.degree[dir]
    end
    return e,xi
end




"""
function e_a_xi_for_gb_t
=====

Returns the e,a,xi for global basis gb at parameter t

# Parameters
* gb: The global basis index
* t: The global parameter t value.

# Returns
* e: element index corresponding to parameter t
* a: element basis function index corresponding to global basis index gb.
     NOTE: 'a' is returned as index zero (invalid index) if the global index
     is outside of the element.
* xi: The parameter xi value in element e corresponding to the parameter t value.

"""
function e_a_xi_for_gb_t(self::Nurbs,gb::Array{Int64},t::Array{Float64})
    e,xi = e_xi_for_t(self,t)
    a = zeros(Int64,self.dim_p)
    for dir = 1:self.dim_p
        a[dir] = self.GBEBI[dir][e[dir],gb[dir]]
    end
    return e,a,xi
end




"""
function basis(self::Nurbs,
                  e::Array{Int64},
                  a::Array{Int64},
                  xi::Array{Float64})
================

Returns the value of the a-th IGA basis function over
the e-th knot-span at xi, where xi is a length dim_p array.
Assumes an element in xi-space is in the range (-1,1) in all dimensions.

# Parameters
* self::IGAGrid2D Pointer to the Nurbs object
* e: Element index (Array: index in each parameter direction)
* a: Element node index
* xi: Array length dim_p, of floats: The parameter value where the function is evaluated
* Returns: The value of the NURBS basis (of the mesh) evaluated at xi
"""
function basis(self::Nurbs,
                 e::Array{Int64},
                 a::Array{Int64},
                 xi::Array{Float64})
    Result = nothing
    if( dict_has_key(self,self.basis_dict,e,(a,xi)) )
        Result = dict_get_value(self,self.basis_dict,e,(a,xi))
    else
        C_e = bezExtOpForEl(self,e)
        weights_e = weightsForEl(self,e)
        Result = nurbs_basis(a,
                             self.degree,
                             C_e,
                             weights_e,
                             xi)
        dict_set_value(self,self.basis_dict,e,(a,xi),Result)
    end
    return Result
end
function basis(self::Nurbs,
                 e::Array{Int64},
                 xi::Array{Float64})
    if(self.dim_p == 1)
        Result = ones(self.nnodesperknotspan[1],1)
        for a1 = 1:self.nnodesperknotspan[1]
            Result[a1] = basis(self,e,[a1],xi)
        end
    elseif(self.dim_p == 2)
        Result = ones(self.nnodesperknotspan[1],self.nnodesperknotspan[2])
        for a1 = 1:self.nnodesperknotspan[1]
            for a2 = 1:self.nnodesperknotspan[2]
                Result[a1,a2] = basis(self,e,[a1,a2],xi)
            end
        end
    elseif(self.dim_p == 3)
        Result = ones(self.nnodesperknotspan[1],self.nnodesperknotspan[2],self.nnodesperknotspan[3])
        for a1 = 1:self.nnodesperknotspan[1]
            for a2 = 1:self.nnodesperknotspan[2]
                for a3 = 1:self.nnodesperknotspan[3]
                    Result[a1,a2,a3] = basis(self,e,[a1,a2,a3],xi)
                end
            end
        end
    end
    return Result
end
function basis_tensorprod(self::Nurbs,
                          e::Array{Int64},
                          xi::Array{Float64})
    #TODO: Make this function multi-D!
    # Supports only 2D, currently.
    results_a1 = zeros(self.nnodesperknotspan[1])
    results_a2 = zeros(self.nnodesperknotspan[2])
    C_e = bezExtOpForEl(self,e)
    weights_e = weightsForEl(self,e)
    for a1 = 1:self.nnodesperknotspan[1]
        results_a1[a1] = nurbs_basis(a1,
                             self.degree[1],
                             C_e[1],
                             weights_e[1],
                             xi[1])
    end
    for a2 = 1:self.nnodesperknotspan[2]
        results_a2[a2] = nurbs_basis(a2,
                             self.degree[2],
                             C_e[2],
                             weights_e[2],
                             xi[2])
    end
    result = results_a1*results_a2'
    return result
end









"""
function basis_ds(self::Nurbs,
                  e::Array{Int64},
                  a::Array{Int64},
                  xi::Array{Float64})
================

Returns the derivative, w.r.t. the design params, of the a-th IGA basis function over
the e-th knot-span at xi, where xi is a length dim_p array.
Assumes an element in xi-space is in the range (-1,1) in all dimensions.

# Parameters
* self::IGAGrid2D Pointer to the Nurbs object
* e: Element index (Array: index in each parameter direction)
* a: Element node index
* xi: Array length dim_p, of floats: The parameter value where the function is evaluated
* Returns: The derivative d/ds of the NURBS basis (of the mesh) evaluated at xi
"""
function basis_ds(self::Nurbs,
                  e::Array{Int64},
                  a::Array{Int64},
                  xi::Array{Float64},
                  s_dir::Int64)
    Result = nothing
    if( dict_has_key(self,self.basis_ds_dict,e,(a,xi,s_dir)) )
        Result = dict_get_value(self,self.basis_ds_dict,e,(a,xi,s_dir))
    else
        C_e = bezExtOpForEl(self,e)
        weights_e = weightsForEl(self,e)
        weights_e_ds = weightsForEl_ds(self,e,s_dir)
        Result = nurbs_basis_ds(a,
                                self.degree,
                                C_e,
                                weights_e,
                                weights_e_ds,
                                xi)
        dict_set_value(self,self.basis_ds_dict,e,(a,xi,s_dir),Result)
    end
    return Result
end
function basis_ds(self::Nurbs,
                 e::Array{Int64},
                 xi::Array{Float64},
                 s_dir::Int64)
    if(self.dim_p == 1)
        Result = ones(self.nnodesperknotspan[1],1)
        for a1 = 1:self.nnodesperknotspan[1]
            Result[a1] = basis_ds(self,e,[a1],xi,s_dir)
        end
    elseif(self.dim_p == 2)
        Result = ones(self.nnodesperknotspan[1],self.nnodesperknotspan[2])
        for a1 = 1:self.nnodesperknotspan[1]
            for a2 = 1:self.nnodesperknotspan[2]
                Result[a1,a2] = basis_ds(self,e,[a1,a2],xi,s_dir)
            end
        end
    elseif(self.dim_p == 3)
        Result = ones(self.nnodesperknotspan[1],self.nnodesperknotspan[2],self.nnodesperknotspan[3])
        for a1 = 1:self.nnodesperknotspan[1]
            for a2 = 1:self.nnodesperknotspan[2]
                for a3 = 1:self.nnodesperknotspan[3]
                    Result[a1,a2,a3] = basis_ds(self,e,[a1,a2,a3],xi,s_dir)
                end
            end
        end
    end
    return Result
end






"""
function del_basis_dRdxi
======

Returns the value of the derivative of the IGA
basis functions with respect to the parameter values xi,eta
"""
function del_basis_dRdxi(self::Nurbs,
                         e::Array{Int64},
                         a::Array{Int64},
                         xi::Array{Float64})

    # ************************************
    # Compute the dR/dxi matrix
    # ************************************
    C_e = bezExtOpForEl(self,e)
    weights_e = weightsForEl(self,e)
    dRdxi = del_nurbs_basis(a,
                            self.degree,
                            C_e,
                            weights_e,
                            xi)
    dRdxi = reshape(dRdxi,1,self.dim_p)

    # return the result
    return dRdxi
end






"""
function del_basis_dRdsdxi
======

Returns dR/ds/dxi.

Returns the value of the derivative of the IGA
basis functions with respect to the parameter values xi,eta, and the
design parameter s.

"""
function del_basis_dRdsdxi(self::Nurbs,
                           e::Array{Int64},
                           a::Array{Int64},
                           xi::Array{Float64},
                           s_dir::Int64)

    # ************************************
    # Compute the dR/dxi matrix
    # ************************************
    C_e = bezExtOpForEl(self,e)
    weights_e = weightsForEl(self,e)
    weights_e_ds = weightsForEl_ds(self,e,s_dir)
    dRdsdxi = del_nurbs_basis_ds(a,
                                 self.degree,
                                 C_e,
                                 weights_e,
                                 weights_e_ds,
                                 xi)
    dRdsdxi = reshape(dRdsdxi,1,self.dim_p)

    # return the result
    return dRdsdxi
end






"""
function delBasis(self::Nurbs,
                     e::Array{Int64},
                     a::Array{Int64},
                     xi::Array{Float64})
================

Returns the value of the derivative of IGA basis
functions of element e at xi, where xi in (-1,1)x(-1,1),
with respect to the spatial coordinates.
Assumes an element goes from xi = -1 to 1, and returns
the 1st derivative of the basis function a, at xi, with respect to x.

# Parameters
* self::Nurbs Pointer to the Nurbs object
* e: Element index
* a: Element node index
* xi: Array length dim_p, of floats: The parameter value where the function is evaluated
* Returns: The value of the derivative (with respect to x) of the NURBS basis
    evaluated at xi.
"""
function delBasis(self::Nurbs,
                     e::Array{Int64},
                     a::Array{Int64},
                     xi::Array{Float64})

    Result_dRdx = nothing

    if( dict_has_key(self,self.delbasis_dict,e,(a,xi)) )
        # Do this if we already have the value saved-off
        Result_dRdx = dict_get_value(self,self.delbasis_dict,e,(a,xi))
    else

        # ***************************
        # Compute [dR/dxi1 dR/dxi2]
        # ***************************
        dRdxi = del_basis_dRdxi(self,e,a,xi)

        # ************************************
        # Compute the dxi/dx in each direction
        # ************************************
        dx_dxi = domain_dXdxi(self,e,reshape(xi,self.dim_p,1))
        # The "dx_dxi" matrix must be square and invertable or this code will fail.
        dxidx = zeros(self.dim_p,self.dim_p)
        dxidx[:,:] = inv(dx_dxi)

        # ************************************
        # Put together the solution
        # ************************************
        #   [x x]  =  [x x] * [ x x ]
        #                     [ x x ]
        Result_dRdx = dRdxi * dxidx

        dict_set_value(self,self.delbasis_dict,e,(a,xi),Result_dRdx)
    end

    # Return the result
    return Result_dRdx
end
function delBasis_tensorprod(self::Nurbs,
                        e::Array{Int64},
                        xi::Array{Float64})

    # TODO: This function is only for 2D (dim_s,dim_p both = 2)
    #
    # ************************************
    # Compute the dR/dxi matrix
    # ************************************
    dRdxi = zeros(2,self.nnodesperknotspan[1],self.nnodesperknotspan[2])

    C_e = bezExtOpForEl(self,e)
    control_pts_e = controlPtsForEl(self,e,self.control_pts)
    weights_e = weightsForEl(self,e)

    R_a1 = zeros(self.nnodesperknotspan[1])
    R_a2 = zeros(self.nnodesperknotspan[2])

    dRdxi_a1 = zeros(self.nnodesperknotspan[1])
    dRdxi_a2 = zeros(self.nnodesperknotspan[2])

    for a1 = 1:self.nnodesperknotspan[1]
        R_a1[a1] = nurbs_basis(a1,self.degree[1],C_e[1],weights_e[1],xi[1])
        dRdxi_a1[a1] = del_nurbs_basis(a1,self.degree[1],C_e[1],weights_e[1],xi[1])
    end
    for a2 = 1:self.nnodesperknotspan[2]
        R_a2[a2] = nurbs_basis(a2,self.degree[2],C_e[2],weights_e[2],xi[2])
        dRdxi_a2[a2] = del_nurbs_basis(a2,self.degree[2],C_e[2],weights_e[2],xi[2])
    end

    for a1 = 1:self.nnodesperknotspan[1]
        for a2 = 1:self.nnodesperknotspan[2]
            dRdxi[1,a1,a2] = dRdxi_a1[a1]*R_a2[a2]
            dRdxi[2,a1,a2] = R_a1[a1]*dRdxi_a2[a2]
        end
    end

    # ************************************
    # Compute the dxi/dx in each direction
    # ************************************
    dim_s = 2
    dim_p = 2
    DXDXI = zeros(dim_s,dim_p)
    for a1 in 1:self.degree[1]+1
        for a2 in 1:self.degree[2]+1
            DXDXI[:,1] += (dRdxi[1,a1,a2] * control_pts_e[:,a1,a2])[:]
            DXDXI[:,2] += (dRdxi[2,a1,a2] * control_pts_e[:,a1,a2])[:]
        end
    end
    dx_dxi = DXDXI

    # The "dx_dxi" matrix must be square and invertable or this code will fail.
    dxidx = zeros(self.dim_p,self.dim_p)
    try
        dxidx[:,:] = inv(dx_dxi)
    catch
        println("inv failed in 'delBasis_tensorprod'!")
        println("e = ",e)
        println("xi = ",xi)
        println("dx_dxi = ",dx_dxi)
        exit()
    end
    # ************************************
    # Put together the solution
    # ************************************
    #   [x x]  =  [x x] * [ x x ]
    #                     [ x x ]
    Result_dRdx = zeros(2,self.nnodesperknotspan[1],self.nnodesperknotspan[2])
    for a1 = 1:self.nnodesperknotspan[1]
        for a2 = 1:self.nnodesperknotspan[2]
            Result_dRdx[:,a1,a2] = transpose(transpose(dRdxi[:,a1,a2])*dxidx)
        end
    end
    return Result_dRdx
end






"""
function delBasis_ds
=========

Returns dR/ds/dx
"""
function delBasis_ds(self::Nurbs,
                     e::Array{Int64},
                     a::Array{Int64},
                     xi::Array{Float64},
                     s_dir::Int64)

    Result_dRdsdx = nothing
    if( dict_has_key(self,self.delbasis_ds_dict,e,(a,xi,s_dir)) )
        Result_dRdsdx = dict_get_value(self,self.delbasis_ds_dict,e,(a,xi,s_dir))
    else
        # ***************************
        # Compute the Jacobian values
        # ***************************
        J = Jacobian(self,e,xi)
        J_inv = inv(J)
        J_ds = Jacobian_ds(self,e,xi,s_dir)
        J_inv_ds = -(J_inv * J_ds * J_inv)

        # ***************************
        # Compute [dR/dxi1 dR/dxi2]
        # ***************************
        dRdxi = del_basis_dRdxi(self,e,a,xi)

        # ***************************
        # Compute [dR/ds/dxi1 dR/ds/dxi2]
        # ***************************
        dR_ds_dxi = del_basis_dRdsdxi(self,e,a,xi,s_dir)

        # ***************************
        # Compute dR/ds/dx
        # ***************************
        Result_dRdsdx = (dR_ds_dxi * J_inv) + (dRdxi * J_inv_ds)

        dict_set_value(self,self.delbasis_ds_dict,e,(a,xi,s_dir),Result_dRdsdx)
    end

    # Return the result
    return Result_dRdsdx

end










"""
function domain_X
========

e: The element
xi: xi value to be evaluated, array shape (dim_p,1)
"""
function domain_X(self::Nurbs,
                  e::Array{Int64},
                  xi::Array{Float64,2})

    # Multi-d version.
    # This is what we're doing.  This function returns the spacial domain X point
    # for a parameter point t, in 1D, 2D, and 3D.  Initially, we need to get this
    # function working to the point where it can draw a dim_s=2,dim_p=1 curve, and when
    # that works it'll be easier to see how to make it work in arbitrary dimensions.
    # See page 51 of the IGA book.

    X = nothing
    if( dict_has_key(self,self.x_dict,e,xi) )
        X = dict_get_value(self,self.x_dict,e,xi)
    else
        degree = self.degree
        C_e = bezExtOpForEl(self,e)
        control_pts = controlPtsForEl(self,e,self.control_pts)
        weights = weightsForEl(self,e)

        # Multi-d
        dim_p = length(xi) # "xi" is just a single point in parameter space.
        dim_s = nothing
        if(dim_p == 1)
            dim_s = length(control_pts[:,1])
        elseif(dim_p == 2)
            dim_s = length(control_pts[:,1,1])
        elseif(dim_p == 3)
            dim_s = length(control_pts[:,1,1,1])
        end

        X = zeros(dim_s,1)

        if(dim_p == 1)
            for i_d1 in 1:degree[1]+1
                NBH = nurbs_basis([i_d1],degree,C_e,weights,xi)
                X += (NBH * control_pts[:,i_d1])[:]
            end

        elseif(dim_p == 2)
            for i_d1 in 1:degree[1]+1
                for i_d2 in 1:degree[2]+1
                    NBH = nurbs_basis([i_d1,i_d2],degree,C_e,weights,xi)
                    X += (NBH * control_pts[:,i_d1,i_d2])[:]
                end
            end

        elseif(dim_p == 3)
            for i_d1 in 1:degree[1]+1
                for i_d2 in 1:degree[2]+1
                    for i_d3 in 1:degree[3]+1
                        NBH = nurbs_basis([i_d1,i_d2,i_d3],degree,C_e,weights,xi)
                        X += (NBH * control_pts[:,i_d1,i_d2,i_d3])[:]
                    end
                end
            end

        end

        dict_set_value(self,self.x_dict,e,xi,X)
    end
    return X
end



"""
function domain_X
========

t: t value to be evaluated, array shape (dim_p,1)
"""
function domain_X(self::Nurbs,
                  t::Array{Float64,2})
    e,xi = e_xi_for_t(self,t)
    return domain_X(self,e,reshape(xi,self.dim_p,1))
end




"""
function domain_dXdxi
===========
Returns dxdxi of the NURBS surface.

# Parameters
* self::Nurbs
* e::Array{Int64} Array length dim_p
* xi::Array{Float64} Array length dim_p, array shape (dim_p,1)
"""
function domain_dXdxi(self::Nurbs,
                      e::Array{Int64},
                      xi::Array{Float64,2})

    DXDXI = nothing
    if( dict_has_key(self,self.dxdxi_dict,e,xi) )
        DXDXI = dict_get_value(self,self.dxdxi_dict,e,xi)
    else

        degree = self.degree
        C_e = bezExtOpForEl(self,e)
        control_pts = controlPtsForEl(self,e,self.control_pts)
        weights = weightsForEl(self,e)

        # Multi-d version; "cpsupport" version
        dim_p = length(xi) # "xi" is just a single point in parameter space.
        dim_s = nothing
        if(dim_p == 1)
            dim_s = length(control_pts[:,1])
        elseif(dim_p == 2)
            dim_s = length(control_pts[:,1,1])
        elseif(dim_p == 3)
            dim_s = length(control_pts[:,1,1,1])
        end

        DXDXI = zeros(dim_s,dim_p)

        if(dim_p == 1)
            for i_d1 in 1:degree[1]+1
                dRdxi = del_nurbs_basis([i_d1],degree,C_e,weights,xi)
                for pdir = 1:dim_p
                    DXDXI[:,pdir] += (dRdxi[pdir] * control_pts[:,i_d1])[:]
                end
            end

        elseif(dim_p == 2)
            for i_d1 in 1:degree[1]+1
                for i_d2 in 1:degree[2]+1
                    dRdxi = del_nurbs_basis([i_d1,i_d2],degree,C_e,weights,xi)
                    for pdir = 1:dim_p
                        DXDXI[:,pdir] += (dRdxi[pdir] * control_pts[:,i_d1,i_d2])[:]
                    end
                end
            end

        elseif(dim_p == 3)
            for i_d1 in 1:degree[1]+1
                for i_d2 in 1:degree[2]+1
                    for i_d3 in 1:degree[3]+1
                        dRdxi = del_nurbs_basis([i_d1,i_d2,i_d3],degree,C_e,weights,xi)
                        for pdir = 1:dim_p
                            DXDXI[:,pdir] += (dRdxi[pdir] * control_pts[:,i_d1,i_d2,i_d3])[:]
                        end
                    end
                end
            end

        end

        dict_set_value(self,self.dxdxi_dict,e,xi,DXDXI)
    end

    # Returns: Matrix of dimension dim_s-by-dim_p
    return DXDXI

end



"""
function domain_dXdt
===========
Returns dxdt of the NURBS surface. (The first
derivative with respect to the surface parameter t,
instead of the element parameter xi)

# Parameters
* self::Nurbs
* t::Array{Float64} Array length dim_p, array shape (dim_p,1)
"""
function domain_dXdt(self::Nurbs,
                     t::Array{Float64,2})
    e,xi = e_xi_for_t(self,t)

    # DXDXI is matrix of dimension dim_s-by-dim_p
    DXDXI = domain_dXdxi(self,e,reshape(xi,self.dim_p,1))

    # T_RANGE is a matrix of dimension 2-by-dim_p
    T_RANGE = t_range_for_e(self,e)

    DXDT = zeros(size(DXDXI))
    for dir_s = 1:self.dim_s
        for dir_p = 1:self.dim_p
            element_width = T_RANGE[2,dir_p]-T_RANGE[1,dir_p]
            DXDT[dir_s,dir_p] = DXDXI[dir_s,dir_p] * (2.0/element_width)
        end
    end

    # Returns matrix of dimension dim_s-by-dim_p
    return DXDT
end



"""
function domain_dXdsdxi
===========

Returns the derivative dX/ds/dxi (dim_s-by-dim_p matrix)

t: t value to be evaluated, array shape (dim_p,1)
s_dir: The design parameter to take the derivative with respect to.
"""
function domain_dXdsdxi(self::Nurbs,
                        e::Array{Int64},
                        xi::Array{Float64},
                        s_dir::Int64)

    DXDSDXI = nothing
    if( dict_has_key(self,self.dxdsdxi_dict,e,(xi,s_dir)) )
        DXDSDXI = dict_get_value(self,self.dxdsdxi_dict,e,(xi,s_dir))
    else

        degree = self.degree
        C_e = bezExtOpForEl(self,e)
        control_pts = controlPtsForEl(self,e,self.control_pts)
        control_pts_ds = controlPtsForEl(self,e,self.control_pts_ds[s_dir])
        weights = weightsForEl(self,e)
        weights_ds = weightsForEl_ds(self,e,s_dir)

        # Multi-d version;
        dim_p = length(xi) # "xi" is just a single point in parameter space.
        dim_s = nothing
        if(dim_p == 1)
            dim_s = length(control_pts[:,1])
        elseif(dim_p == 2)
            dim_s = length(control_pts[:,1,1])
        elseif(dim_p == 3)
            dim_s = length(control_pts[:,1,1,1])
        end

        DXDSDXI = zeros(dim_s,dim_p)

        if(dim_p == 1)
            for i_d1 in 1:degree[1]+1
                dRdsdxi = del_nurbs_basis_ds([i_d1],degree,C_e,weights,weights_ds,xi)
                dRdxi = del_nurbs_basis([i_d1],degree,C_e,weights,xi)
                for pdir = 1:dim_p
                    DXDSDXI[:,pdir] += (control_pts[:,i_d1] * dRdsdxi[pdir] + control_pts_ds[:,i_d1] * dRdxi[pdir])[:]
                end
            end


        elseif(dim_p == 2)
            for i_d1 in 1:degree[1]+1
                for i_d2 in 1:degree[2]+1
                    dRdsdxi = del_nurbs_basis_ds([i_d1,i_d2],degree,C_e,weights,weights_ds,xi)
                    dRdxi = del_nurbs_basis([i_d1,i_d2],degree,C_e,weights,xi)
                    for pdir = 1:dim_p
                        DXDSDXI[:,pdir] += (control_pts[:,i_d1,i_d2] * dRdsdxi[pdir] + control_pts_ds[:,i_d1,i_d2] * dRdxi[pdir])[:]
                    end
                end
            end


        elseif(dim_p == 3)
            for i_d1 in 1:degree[1]+1
                for i_d2 in 1:degree[2]+1
                    for i_d3 in 1:degree[3]+1
                        dRdsdxi = del_nurbs_basis_ds([i_d1,i_d2,i_d3],degree,C_e,weights,weights_ds,xi)
                        dRdxi = del_nurbs_basis([i_d1,i_d2,i_d3],degree,C_e,weights,xi)
                        for pdir = 1:dim_p
                            DXDSDXI[:,pdir] += (control_pts[:,i_d1,i_d2,i_d3] * dRdsdxi[pdir] + control_pts_ds[:,i_d1,i_d2,i_d3] * dRdxi[pdir])[:]
                        end
                    end
                end
            end

        end

        dict_set_value(self,self.dxdsdxi_dict,e,(xi,s_dir),DXDSDXI)
    end

    return DXDSDXI
end





"""
function Jacobian(self::Nurbs,e::Array{Int64},xi::Array{Int64})
========

# Parameters
* self: Pointer to the Nurbs
* e: Element index
* xi:

# Returns:
* dim_s-by-dim_p matrix containing [[dx1/dxi1, dx1/dxi2, ...]; [dx2/dxi1, dx2/dxi2, ...]; ...]
"""
function Jacobian(self::Nurbs,e::Array{Int64},xi::Array{Float64})
    # ************************************
    # Compute the dx/dxi in each direction
    # ************************************
    dxdxi = domain_dXdxi(self,e,reshape(xi,self.dim_p,1))
    # This is the Jacobian
    J = dxdxi
    return J
end





"""
function Jacobian_ds(self::Nurbs,e::Array{Int64},xi::Array{Int64},s_dir::Int64)
========

Returns the Sensitivity of the Jacobian (derivative of the Jacobian w.r.t.
    the design parameter s)

# Parameters
* self: Pointer to the Nurbs
* e: Element index
* xi:
* s_dir: The index of the design parameter we take derivative with respect to.

# Returns:
* dim_s-by-dim_p matrix containing [[dx1/dxi1/ds, dx1/dxi2/ds, ...]; [dx2/dxi1/ds, dx2/dxi2/ds, ...]; ...]
"""
function Jacobian_ds(self::Nurbs,e::Array{Int64},xi::Array{Float64},s_dir::Int64)
    # ************************************
    # Compute the dx/dxi in each direction
    # ************************************
    dxdsdxi = domain_dXdsdxi(self,e,xi,s_dir)
    J_ds = dxdsdxi
    return J_ds
end








"""
function draw
==========
"""
function draw(self::Nurbs,res::Array{Int64})

    knot_v = self.knot_v

    if(self.dim_p == 1)
        epsilon = 1.0e-14
        h = (maximum(knot_v[1]) - minimum(knot_v[1]))/(res[1]-1) - epsilon
        t = collect( minimum(knot_v[1]):h:minimum(knot_v[1])+(res[1]-1)*h )
        while( length(t) != res[1] )
            epsilon = epsilon * 10
            if(epsilon > 1.0)
                println("Error with epsilon in 'draw' for dim_p=1 in nurbstools")
                exit()
            end
            h = (maximum(knot_v[1]) - minimum(knot_v[1]))/(res[1]-1) - epsilon
            t = collect( minimum(knot_v[1]):h:minimum(knot_v[1])+(res[1]-1)*h )
        end
        t = reshape(t,1,res[1])
        X = zeros(self.dim_s,res[1])
        for i = 1:res[1]
            e,xi = e_xi_for_t(self,Array{Float64}([t[i]]))
            X[:,i] = domain_X(self,e,reshape(xi,self.dim_p,1))
        end
        return X,t,[h]
    end
    if(self.dim_p == 2)
        h1 = (maximum(knot_v[1]) - minimum(knot_v[1]))/(res[1]-1) - 1.0e-14
        h2 = (maximum(knot_v[2]) - minimum(knot_v[2]))/(res[2]-1) - 1.0e-14
        t1 = minimum(knot_v[1]):h1:minimum(knot_v[1])+(res[1]-1)*h1
        t2 = minimum(knot_v[2]):h2:minimum(knot_v[2])+(res[2]-1)*h2
        T1 = zeros(res[1],res[2])
        T2 = zeros(res[1],res[2])
        T1[:,:] = [t1[i] for i = 1:res[1], j = 1:res[2]]
        T2[:,:] = [t2[j] for i = 1:res[1], j = 1:res[2]]
        T = zeros(2,res[1]*res[2])
        for y_i = 1:res[2]
            for x_i = 1:res[1]
                T[1,(y_i-1)*res[1]+x_i] = T1[x_i,y_i]
                T[2,(y_i-1)*res[1]+x_i] = T2[x_i,y_i]
            end
        end
        X = zeros(self.dim_s,res[1]*res[2])
        for i = 1:res[1]*res[2]
            e,xi = e_xi_for_t(self,T[:,i])
            X[:,i] = domain_X(self,e,reshape(xi,self.dim_p,1))
        end
        return X,T,[h1,h2]
    end
    if(self.dim_p == 3)
        h1 = (maximum(knot_v[1]) - minimum(knot_v[1]))/(res[1]-1) - 1.0e-14
        h2 = (maximum(knot_v[2]) - minimum(knot_v[2]))/(res[2]-1) - 1.0e-14
        h3 = (maximum(knot_v[3]) - minimum(knot_v[3]))/(res[3]-1) - 1.0e-14
        t1 = minimum(knot_v[1]):h1:minimum(knot_v[1])+(res[1]-1)*h1
        t2 = minimum(knot_v[2]):h2:minimum(knot_v[2])+(res[2]-1)*h2
        t3 = minimum(knot_v[3]):h3:minimum(knot_v[3])+(res[3]-1)*h3
        T1 = zeros(res[1],res[2],res[3])
        T2 = zeros(res[1],res[2],res[3])
        T3 = zeros(res[1],res[2],res[3])
        T1[:,:,:] = [t1[i] for i = 1:res[1], j = 1:res[2], k = 1:res[3]]
        T2[:,:,:] = [t2[j] for i = 1:res[1], j = 1:res[2], k = 1:res[3]]
        T3[:,:,:] = [t3[k] for i = 1:res[1], j = 1:res[2], k = 1:res[3]]
        T = zeros(self.dim_p,res[1]*res[2]*res[3])
        for z_i = 1:res[3]
            for y_i = 1:res[2]
                for x_i = 1:res[1]
                    T[1,(z_i-1)*res[1]*res[2]+(y_i-1)*res[1]+x_i] = T1[x_i,y_i,z_i]
                    T[2,(z_i-1)*res[1]*res[2]+(y_i-1)*res[1]+x_i] = T2[x_i,y_i,z_i]
                    T[3,(z_i-1)*res[1]*res[2]+(y_i-1)*res[1]+x_i] = T3[x_i,y_i,z_i]
                end
            end
        end
        X = zeros(self.dim_s,res[1]*res[2]*res[3])
        for i = 1:res[1]*res[2]*res[3]
            e,xi = e_xi_for_t(self,T[:,i])
            X[:,i] = domain_X(self,e,reshape(xi,self.dim_p,1))
        end
        return X,T,[h1,h2,h3]
    end
    return 0
end



"""
function outputObj
==========
Outputs the NURBS as a .obj file.
NOTE: Only works for surfaces.
"""
function outputObj(self::Nurbs,
                   filename::String,
                   res::Array{Int64})
    if(self.dim_p != 2)
        println( "-----------------------------------------" )
        println( "NOTE: 'outputObj' only works for dim_p=2!" )
        println( "-----------------------------------------" )
        return
    end
    f = open(filename,"w");

    # Compute X in a grid with dimensions res[1]-by-res[2]
    knot_v = self.knot_v
    h1 = (maximum(knot_v[1]) - minimum(knot_v[1]))/(res[1]-1) - 1.0e-14
    h2 = (maximum(knot_v[2]) - minimum(knot_v[2]))/(res[2]-1) - 1.0e-14
    t1 = minimum(knot_v[1]):h1:minimum(knot_v[1])+(res[1]-1)*h1
    t2 = minimum(knot_v[2]):h2:minimum(knot_v[2])+(res[2]-1)*h2
    T1 = zeros(res[1],res[2])
    T2 = zeros(res[1],res[2])
    T1[:,:] = [t1[i] for i = 1:res[1], j = 1:res[2]]
    T2[:,:] = [t2[j] for i = 1:res[1], j = 1:res[2]]
    T = zeros(2,res[1],res[2])
    for y_i = 1:res[2]
        for x_i = 1:res[1]
            T[1,x_i,y_i] = T1[x_i,y_i]
            T[2,x_i,y_i] = T2[x_i,y_i]
        end
    end
    X = zeros(self.dim_s,res[1],res[2])
    for y_i = 1:res[2]
        for x_i = 1:res[1]
            e,xi = e_xi_for_t(self,T[:,x_i,y_i])
            X[:,x_i,y_i] = domain_X(self,e,reshape(xi,self.dim_p,1))
        end
    end

    VERTSIDS = zeros(Int64,res[1],res[2])
    v_id = 0
    for i = 1:res[1]
        for j = 1:res[2]
            v_id += 1
            vert = zeros(3)
            if( self.dim_s == 1 )
                vert[1] = X[1,i,j]
                vert[2] = 0.0
                vert[3] = 0.0
            elseif( self.dim_s == 2 )
                vert[1] = X[1,i,j]
                vert[2] = X[2,i,j]
                vert[3] = 0.0
            elseif( self.dim_s == 3 )
                vert[:] = X[:,i,j]
            end
            write(f,"v"*" "*string(vert[1])*" "*string(vert[2])*" "*string(vert[3])*"\n")
            VERTSIDS[i,j] = v_id
        end
    end
    for j = 1:res[2]-1
        for i = 1:res[1]-1
            write(f,"f"*" "*string(VERTSIDS[i,  j  ])*" "*string(VERTSIDS[i,  j+1])*" "
                           *string(VERTSIDS[i+1,j+1])*" "*string(VERTSIDS[i+1,j  ])*"\n")
            write(f,"f"*" "*string(VERTSIDS[i,  j  ])*" "*string(VERTSIDS[i+1,j])*" "
                           *string(VERTSIDS[i+1,j+1])*" "*string(VERTSIDS[i,  j+1  ])*"\n")
        end
    end
    close(f)
end


"""
function InsertMultipleKnots(self::Nurbs,direction_p::Int64,new_knots::Array{Float64})
=========
Inserts multiple knots into the Nurbs spline, in a way that makes it faster
than adding each knot one at a time.

# Parameters:
* self: NURBS object pointer
* direction_p: The parameter direction to which the knot is being added.
* new_knot: Array of knot values to be inserted.
* reinit_dep_arrs: Specify whether to re-initialize arrays dependent on
         the knot vector (should only be false if this is called by an outer
         funciton that will do it at the end)
"""
function InsertMultipleKnots(self::Nurbs,
                             direction_p::Int64,
                             new_knots::Array{Float64};
                             reinit_dep_arrs::Bool = true)
	## NOTE: This will only work for 1D for now.

    println("UNDER CONSTRUCTION.")
    quit()

    # Step 1: Init new arrays that are large enough to hold the new control points, knots.
    #  TODO
    # Step 2: Insert each knot, keeping the old arrays and making the modifications in-place.
    #  TODO
    # Step 3: Finish up.
    #  TODO

    # These need to be re-initialized after the knot vector is modified.
    if( reinit_dep_arrs )
        init_special_arrays(self)
        init_saveoff_dicts(self)
    end

	GC.gc()
	return
end




"""
function InsertKnot(self::Nurbs,direction_p::Int64,new_knot::Float64)
=========
Inserts a knot into the Nurbs spline

# Parameters:
* self: NURBS object pointer
* direction_p: The parameter direction to which the knot is being added.
* new_knot: The knot value to be inserted
* reinit_dep_arrs: Specify whether to re-initialize arrays dependent on
         the knot vector (should only be false if this is called by an outer
         funciton that will do it at the end)
"""
function InsertKnot(self::Nurbs,
                    direction_p::Int64,
                    new_knot::Float64;
                    reinit_dep_arrs::Bool = true)

    new_control_pts = nothing
    new_control_pts_ds = nothing
    new_kv = nothing
    new_cpts = nothing
    new_del_cpts = nothing
    new_wts = nothing
    new_del_wts = nothing
    new_del_wts_returned = nothing
    wts_multi_d = nothing
    c_pts_multi_d = nothing
    del_wts_multi_d = nothing
    del_c_pts_multi_d = nothing

    # Parameter 1D
    if(self.dim_p == 1)
        direction_p = 1

        wts_multi_d = self.weights
        c_pts_multi_d = self.control_pts
        del_wts_multi_d = self.weights_ds
        del_c_pts_multi_d = self.control_pts_ds

        if(direction_p == 1)
            new_control_pts = Array{Float64}(undef,self.dim_s,
                                    length(c_pts_multi_d[1,:])+1)
            new_control_pts_ds = Array{Any}(undef,self.ndesignvars)
            for partial_i = 1:self.ndesignvars
                new_control_pts_ds[partial_i] = Array{Float64}(undef,self.dim_s,
                                        length(c_pts_multi_d[1,:])+1)
            end
            new_del_wts = Array{Any}(undef,self.ndesignvars)
            for direction_s = 1:self.dim_s
                for partial = 1:self.ndesignvars

                    c_pts_1d = c_pts_multi_d[direction_s,:]
                    c_pts_1d = reshape(c_pts_1d,1,length(c_pts_1d))
                    wts_1d = wts_multi_d[direction_p]

                    del_c_pts_1d = del_c_pts_multi_d[partial][direction_s,:]
                    del_c_pts_1d = reshape(del_c_pts_1d,1,length(del_c_pts_1d))
                    del_wts_1d = del_wts_multi_d[partial][direction_p]

                    new_kv,new_cpts,new_wts,new_del_cpts,new_del_wts_returned, = InsertKnot(new_knot,
                                                                                   self.degree[direction_p],
                                                                                   self.knot_v[direction_p],
                                                                                   c_pts_1d,
                                                                                   wts_1d,
                                                                                   del_c_pts_1d,
                                                                                   del_wts_1d)
                    new_del_wts[partial] = new_del_wts_returned
                    new_control_pts[direction_s,:] = new_cpts
                    new_control_pts_ds[partial][direction_s,:] = new_del_cpts
                end
            end
        end

        self.weights[direction_p] = new_wts
        self.control_pts = new_control_pts
        for partial = 1:self.ndesignvars
            self.weights_ds[partial][direction_p] = new_del_wts[partial]
            self.control_pts_ds[partial] = new_control_pts_ds[partial]
        end
        self.knot_v[direction_p] = new_kv
        self.spans[direction_p] = size(new_control_pts)[1+direction_p]-1

    # Parameter 2D
    elseif(self.dim_p == 2)

        wts_multi_d = self.weights
        c_pts_multi_d = self.control_pts
        del_wts_multi_d = self.weights_ds
        del_c_pts_multi_d = self.control_pts_ds

        if(direction_p == 1)
            new_control_pts = Array{Float64}(undef,self.dim_s,
                                    length(c_pts_multi_d[1,:,1])+1,
                                    length(c_pts_multi_d[1,1,:]))
            new_control_pts_ds = Array{Any}(undef,self.ndesignvars)
            for partial_i = 1:self.ndesignvars
                new_control_pts_ds[partial_i] = Array{Float64}(undef,self.dim_s,
                                        length(c_pts_multi_d[1,:,1])+1,
                                        length(c_pts_multi_d[1,1,:]))
            end
            new_del_wts = Array{Any}(undef,self.ndesignvars)
            for direction_s = 1:self.dim_s
                for odir = 1:length(c_pts_multi_d[1,1,:])
                    for partial = 1:self.ndesignvars

                        c_pts_1d = c_pts_multi_d[direction_s,:,odir]
                        c_pts_1d = reshape(c_pts_1d,1,length(c_pts_1d))
                        wts_1d = wts_multi_d[direction_p]

                        del_c_pts_1d = del_c_pts_multi_d[partial][direction_s,:,odir]
                        del_c_pts_1d = reshape(del_c_pts_1d,1,length(del_c_pts_1d))
                        del_wts_1d = del_wts_multi_d[partial][direction_p]

                        new_kv,new_cpts,new_wts,new_del_cpts,new_del_wts_returned, = InsertKnot(new_knot,
                                                                                       self.degree[direction_p],
                                                                                       self.knot_v[direction_p],
                                                                                       c_pts_1d,
                                                                                       wts_1d,
                                                                                       del_c_pts_1d,
                                                                                       del_wts_1d)
                        new_del_wts[partial] = new_del_wts_returned
                        new_control_pts[direction_s,:,odir] = new_cpts
                        new_control_pts_ds[partial][direction_s,:,odir] = new_del_cpts
                    end
                end
            end

        elseif(direction_p == 2)
            new_control_pts = Array{Float64}(undef,
                                    self.dim_s,
                                    length(c_pts_multi_d[1,:,1]),
                                    length(c_pts_multi_d[1,1,:])+1)
            new_control_pts_ds = Array{Any}(undef,self.ndesignvars)
            for partial_i = 1:self.ndesignvars
                new_control_pts_ds[partial_i] = Array{Float64}(undef,self.dim_s,
                                        length(c_pts_multi_d[1,:,1]),
                                        length(c_pts_multi_d[1,1,:])+1)
            end
            new_del_wts = Array{Any}(undef,self.ndesignvars)
            for direction_s = 1:self.dim_s
                for odir = 1:length(c_pts_multi_d[1,:,1])
                    for partial = 1:self.ndesignvars
                        c_pts_1d = c_pts_multi_d[direction_s,odir,:]
                        c_pts_1d = reshape(c_pts_1d,1,length(c_pts_1d))
                        wts_1d = wts_multi_d[direction_p]

                        del_c_pts_1d = del_c_pts_multi_d[partial][direction_s,odir,:]
                        del_c_pts_1d = reshape(del_c_pts_1d,1,length(del_c_pts_1d))
                        del_wts_1d = del_wts_multi_d[partial][direction_p]

                        new_kv,new_cpts,new_wts,new_del_cpts,new_del_wts_returned, = InsertKnot(new_knot,
                                                                                       self.degree[direction_p],
                                                                                       self.knot_v[direction_p],
                                                                                       c_pts_1d,
                                                                                       wts_1d,
                                                                                       del_c_pts_1d,
                                                                                       del_wts_1d)
                        new_del_wts[partial] = new_del_wts_returned
                        new_control_pts[direction_s,odir,:] = new_cpts
                        new_control_pts_ds[partial][direction_s,odir,:] = new_del_cpts
                    end
                end
            end
        end

        self.weights[direction_p] = new_wts
        self.control_pts = new_control_pts
        for partial = 1:self.ndesignvars
            self.weights_ds[partial][direction_p] = new_del_wts[partial]
            self.control_pts_ds[partial] = new_control_pts_ds[partial]
        end
        self.knot_v[direction_p] = new_kv
        self.spans[direction_p] = size(new_control_pts)[1+direction_p]-1

    # Parameter 3D
    elseif(self.dim_p == 3)

        wts_multi_d = self.weights
        c_pts_multi_d = self.control_pts
        del_wts_multi_d = self.weights_ds
        del_c_pts_multi_d = self.control_pts_ds

        if(direction_p == 1)
            new_control_pts = Array{Float64}(undef,self.dim_s,
                                    length(c_pts_multi_d[1,:,1,1])+1,
                                    length(c_pts_multi_d[1,1,:,1]),
                                    length(c_pts_multi_d[1,1,1,:]))
            new_control_pts_ds = Array{Any}(self.ndesignvars)
            for partial_i = 1:self.ndesignvars
                new_control_pts_ds[partial_i] = Array{Float64}(undef,self.dim_s,
                                    length(c_pts_multi_d[1,:,1,1])+1,
                                    length(c_pts_multi_d[1,1,:,1]),
                                    length(c_pts_multi_d[1,1,1,:]))
            end
            new_del_wts = Array{Any}(undef,self.ndesignvars)
            for direction_s = 1:self.dim_s
                for odir1 = 1:length(c_pts_multi_d[1,1,:,1])
                    for odir2 = 1:length(c_pts_multi_d[1,1,1,:])
                        for partial = 1:self.ndesignvars
                            c_pts_1d = c_pts_multi_d[direction_s,:,odir1,odir2]
                            c_pts_1d = reshape(c_pts_1d,1,length(c_pts_1d))
                            wts_1d = wts_multi_d[direction_p]

                            del_c_pts_1d = del_c_pts_multi_d[partial][direction_s,:,odir1,odir2]
                            del_c_pts_1d = reshape(del_c_pts_1d,1,length(del_c_pts_1d))
                            del_wts_1d = del_wts_multi_d[partial][direction_p]

                            new_kv,new_cpts,new_wts,new_del_cpts,new_del_wts_returned, = InsertKnot(new_knot,
                                                                                           self.degree[direction_p],
                                                                                           self.knot_v[direction_p],
                                                                                           c_pts_1d,
                                                                                           wts_1d,
                                                                                           del_c_pts_1d,
                                                                                           del_wts_1d)
                            new_del_wts[partial] = new_del_wts_returned
                            new_control_pts[direction_s,:,odir1,odir2] = new_cpts
                            new_control_pts_ds[partial][direction_s,:,odir1,odir2] = new_del_cpts
                        end
                    end
                end
            end

        elseif(direction_p == 2)
            new_control_pts = Array{Float64}(undef,self.dim_s,
                                    length(c_pts_multi_d[1,:,1,1]),
                                    length(c_pts_multi_d[1,1,:,1])+1,
                                    length(c_pts_multi_d[1,1,1,:]))
            new_control_pts_ds = Array{Any}(self.ndesignvars)
            for partial_i = 1:self.ndesignvars
                new_control_pts_ds[partial_i] = Array{Float64}(self.dim_s,
                                    length(c_pts_multi_d[1,:,1,1]),
                                    length(c_pts_multi_d[1,1,:,1])+1,
                                    length(c_pts_multi_d[1,1,1,:]))
            end
            new_del_wts = Array{Any}(self.ndesignvars)
            for direction_s = 1:self.dim_s
                for odir1 = 1:length(c_pts_multi_d[1,:,1,1])
                    for odir2 = 1:length(c_pts_multi_d[1,1,1,:])
                        for partial = 1:self.ndesignvars
                            c_pts_1d = c_pts_multi_d[direction_s,odir1,:,odir2]
                            c_pts_1d = reshape(c_pts_1d,1,length(c_pts_1d))
                            wts_1d = wts_multi_d[direction_p]

                            del_c_pts_1d = del_c_pts_multi_d[partial][direction_s,odir1,:,odir2]
                            del_c_pts_1d = reshape(del_c_pts_1d,1,length(del_c_pts_1d))
                            del_wts_1d = del_wts_multi_d[partial][direction_p]

                            new_kv,new_cpts,new_wts,new_del_cpts,new_del_wts_returned, = InsertKnot(new_knot,
                                                                                           self.degree[direction_p],
                                                                                           self.knot_v[direction_p],
                                                                                           c_pts_1d,
                                                                                           wts_1d,
                                                                                           del_c_pts_1d,
                                                                                           del_wts_1d)
                            new_del_wts[partial] = new_del_wts_returned
                            new_control_pts[direction_s,odir1,:,odir2] = new_cpts
                            new_control_pts_ds[partial][direction_s,odir1,:,odir2] = new_del_cpts
                        end
                    end
                end
            end

        elseif(direction_p == 3)
            new_control_pts = Array{Float64}(self.dim_s,
                                    length(c_pts_multi_d[1,:,1,1]),
                                    length(c_pts_multi_d[1,1,:,1]),
                                    length(c_pts_multi_d[1,1,1,:])+1)
            new_control_pts_ds = Array{Any}(self.ndesignvars)
            for partial_i = 1:self.ndesignvars
                new_control_pts_ds[partial_i] = Array{Float64}(self.dim_s,
                                    length(c_pts_multi_d[1,:,1,1]),
                                    length(c_pts_multi_d[1,1,:,1]),
                                    length(c_pts_multi_d[1,1,1,:])+1)
            end
            new_del_wts = Array{Any}(self.ndesignvars)
            for direction_s = 1:self.dim_s
                for odir1 = 1:length(c_pts_multi_d[1,:,1,1])
                    for odir2 = 1:length(c_pts_multi_d[1,1,:,1])
                        for partial = 1:self.ndesignvars
                            c_pts_1d = c_pts_multi_d[direction_s,odir1,odir2,:]
                            c_pts_1d = reshape(c_pts_1d,1,length(c_pts_1d))
                            wts_1d = wts_multi_d[direction_p]

                            del_c_pts_1d = del_c_pts_multi_d[partial][direction_s,odir1,odir2,:]
                            del_c_pts_1d = reshape(del_c_pts_1d,1,length(del_c_pts_1d))
                            del_wts_1d = del_wts_multi_d[partial][direction_p]

                            new_kv,new_cpts,new_wts,new_del_cpts,new_del_wts_returned, = InsertKnot(new_knot,
                                                                                           self.degree[direction_p],
                                                                                           self.knot_v[direction_p],
                                                                                           c_pts_1d,
                                                                                           wts_1d,
                                                                                           del_c_pts_1d,
                                                                                           del_wts_1d)

                            new_del_wts[partial] = new_del_wts_returned
                            new_control_pts[direction_s,odir1,odir2,:] = new_cpts
                            new_control_pts_ds[partial][direction_s,odir1,odir2,:] = new_del_cpts
                        end
                    end
                end
            end
        end

        self.weights[direction_p] = new_wts
        self.control_pts = new_control_pts
        for partial = 1:self.ndesignvars
            self.weights_ds[partial][direction_p] = new_del_wts[partial]
            self.control_pts_ds[partial] = new_control_pts_ds[partial]
        end
        self.knot_v[direction_p] = new_kv
        self.spans[direction_p] = size(new_control_pts)[1+direction_p]-1


    end

    # These need to be re-initialized after the knot vector is modified.
    if( reinit_dep_arrs )
        init_special_arrays(self)
        init_saveoff_dicts(self)
    end

    GC.gc()
    return
end




end # module SSNurbsTools









#=
"""
function basis_bezextop(self::Nurbs,
                  e::Array{Int64},
                  xi::Array{Float64})
================

(Bezier extraction version)

Returns the values of the IGA basis functions over
the e-th knot-span at xi, where xi is a length dim_p array.
Assumes an element in xi-space is in the range (-1,1) in all dimensions.

# Parameters
* self::IGAGrid2D Pointer to the Nurbs object
* e: Element index (Array: index in each parameter direction)
* xi: Array length dim_p, of floats: The parameter value where the function is evaluated
* Returns: The value of the NURBS basis (of the mesh) evaluated at values xi
"""
function basis_bezextop(self::Nurbs,
                  e::Array{Int64},
                  xi::Array{Float64})

    Result = nothing
    if( dict_has_key(self,self.basis_dict,e,xi) )
        Result = dict_get_value(self,self.basis_dict,e,xi)
    else
        if(self.dim_p == 1)
            Result = ones(self.nnodesperknotspan[1],1)
        elseif(self.dim_p == 2)
            Result = ones(self.nnodesperknotspan[1],self.nnodesperknotspan[2])
        end

        for dir = 1:self.dim_p
            # Vector of values of the bernstein basis functions at xi
            bernvB = reshape(Array{Float64}([bernstein_basis_vdv(aa,self.degree[dir],xi[dir],dmin=-1.,dmax=1.)[1]
                                        for aa=1:self.nnodesperknotspan[dir]]),self.nnodesperknotspan[dir],1)
            # The bezier extraction operator for element e.
            elnodes = self.IEN[dir][:,e[dir]]
            bbelnodes = self.BBIEN[dir][:,e[dir]]
            elC = self.C[dir][elnodes,bbelnodes]
            # The bspline basis function values for element e, function a
            bspN = elC[:,:]*bernvB
            # W matrix for element e.
            elW = self.W[dir][elnodes,elnodes]
            #The normalizing factor W(xi)
            Wxi = dot(bspN[:],diag(elW)[:])
            # The values of the NURBS basis function for element e
            Rxi = ((1/Wxi) .* (elW * bspN))
            # Update the result
            if(self.dim_p == 1)
                Result = Result .* Rxi
            elseif(self.dim_p == 2)
                if(dir == 1)
                    for i = 1:self.nnodesperknotspan[dir]
                        Result[:,i] =  Result[:,i][:] .* Rxi[:]
                    end
                elseif(dir == 2)
                    for i = 1:self.nnodesperknotspan[dir]
                        Result[i,:] =  Result[i,:][:] .* Rxi[:]
                    end
                end
            end
        end
        dict_set_value(self,self.basis_dict,e,xi,Result)
    end
    return Result
end
=#






#=
"""
function del_basis_dRdxi_bezextop
======

(Bezier Extraction Operator version)
Returns the value of the derivative of the IGA
basis functions with respect to the parameter values xi,eta
"""
function del_basis_dRdxi_bezextop(self::Nurbs,
                                  e::Array{Int64},
                                  a::Array{Int64},
                                  xi::Array{Float64})

    # Init the array
    dRdxi = zeros(1,self.dim_p)

    # First, compute 1D N, dN/dxi in each direction
    R_val1D = zeros(self.dim_p)
    dRdxi_val1D = zeros(self.dim_p)
    for dir = 1:self.dim_p
        # The bezier extraction operator for 1D-element e[dir]
        elnodes = self.IEN[dir][:,e[dir]]
        bbelnodes = self.BBIEN[dir][:,e[dir]]
        elC = self.C[dir][elnodes,bbelnodes]
        elCv = reshape(elC[a[dir],:],1,self.nnodesperknotspan[dir])
        # Vector of values of the bernstein basis functions at xi[dir]

        # Use helper function to get bernsteinbasis values
        bernvB = zeros(self.nnodesperknotspan[dir],1)
        delbernvB = zeros(self.nnodesperknotspan[dir],1)
        for aa=1:self.nnodesperknotspan[dir]
            bernvB[aa],delbernvB[aa] = bernstein_basis_vdv(aa,self.degree[dir],xi[dir],dmin=-1.,dmax=1.)
        end

        # The bspline basis function values for element e, function a
        N_val1D = (elC * bernvB)
        # The bspline del_basis function values for element e, function a
        dNdxi_val1D = (elC * delbernvB)
        # W matrix for element e.
        elW = self.W[dir][elnodes,elnodes]
        #The normalizing factor W(xi)
        Wxi = dot(N_val1D[:],diag(elW)[:])
        #The derivative of the normalizing factor
        delWxi = dot(dNdxi_val1D[:],diag(elW)[:])
        # The values of the NURBS basis function for element e
        R_val1D[dir] = ((1/Wxi) .* (elW * N_val1D))[a[dir]]
        # The values of the derivatives of the NURBS basis functions for element e
        # (using the quotient rule)
        dRdxi_val1D[dir] = ((elW*dNdxi_val1D*Wxi - elW*N_val1D*delWxi)/(Wxi^2))[a[dir]]
    end
    # Now, compute the dN/dxi in each direction
    if(self.dim_p == 1)
        dRdxi[1,dir] = dRdxi_val1D[dir]
    elseif(self.dim_p == 2)
        dir = 1
        odir = 2
        dRdxi[1,dir] = dRdxi_val1D[dir]*R_val1D[odir]
        dir = 2
        odir = 1
        dRdxi[1,dir] = dRdxi_val1D[dir]*R_val1D[odir]
    end

    return dRdxi
end
=#

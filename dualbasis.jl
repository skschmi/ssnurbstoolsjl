#Steven Schmidt

# Computes the coefficients necessary to represent the dual basis with the original basis.

module SSDualBasis

using LinearAlgebra

# Library for Gaussian Quadrature
include("./ssgaussquadjl/gaussquad.jl")
using .SSGaussQuad
import .SSGaussQuad.GaussQuadTable
import .SSGaussQuad.GaussQuadIntegrate
import .SSGaussQuad.NumGaussPts

# Library for NURBS
include("./nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.Nurbs
import .SSNurbsTools.UniqueKnotArray
import .SSNurbsTools.bernstein_basis
import .SSNurbsTools.del_bernstein_basis
import .SSNurbsTools.basis
import .SSNurbsTools.delBasis
import .SSNurbsTools.nurbs_basis
import .SSNurbsTools.domain_dXdt
import .SSNurbsTools.domain_X
import .SSNurbsTools.ConvertToBernsteinMesh
import .SSNurbsTools.RefineMesh
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.e_a_xi_for_gb_t
import .SSNurbsTools.t_range_for_e
import .SSNurbsTools.Jacobian

"""
function computeDualBasis
============
Given a set of basis functions, this function computes the coefficients of the
dual of the given basis.

# Parameters
* input_basis: The input basis functions, with signature input_basis( a::Int64, xi::Float64 )
               Note: the input basis is assumed to be defined from -1 to 1.
* degree: The degree of the input basis
* num_bases:  The total number of basis functions in the basis-space.
* basis_index: The index of the basis we want the dual of (An integer between 1 and num_bases)
* param_min: The minimum parameter value of the domain
* param_max: The maximum parameter value of the domain.

# Returns
* Array containing the coefficients needed to represent the dual of the input basis with the input basis.
"""
function computeDualBasis( input_basis::Function, degree::Int64, num_bases::Int64, basis_index::Int64; param_min=-1.0, param_max=1.0 )::Array{Float64}
	# For gaussian quadrature integration
	GQT1D = SSGaussQuad.GaussQuadTable(NumGaussPts(degree+degree))

	# compute the Gramian matrix
	G = zeros(num_bases,num_bases)
	jacobianf = function(params)
			#dx/dxi
			return (param_max-param_min)/2.0
		end
	for i = 1:num_bases
		for j = 1:num_bases
			f = function(params)
					xi = params[1]
					t = param_min + ((xi+1.0)/2.0)*(param_max-param_min)
					return input_basis(i,t) * input_basis(j,t)
				end
			G[i,j] = GaussQuadIntegrate(f,GQT1D,jacobianf)
		end
	end

	# Compute the kronneker-delta vector
	b = reshape( zeros(num_bases), num_bases, 1 )
	b[ basis_index ] = 1.0

	println( "size(G):", size(G) )
	println( "G:" )
	println(replace(string(G), ";" => "\n" ))

	# Solve the system
	coeff_vec = G\b

	println( "size(coeff_vec):", size(coeff_vec) )
	println( "coeff_vec:" )
	println(replace(string(coeff_vec), ";" => "\n" ))

	return coeff_vec
end






"""
function computeBSplineDualBasis
============
Given a B-Spline, compute the dual basis function of the given basis function.
(For now this is 1D only!!)

# Parameters
* self: The B-Spline object
* a: The index of the global basis function that we want to compute the dual of.
* support:  An array of elements over which the basis function is non-zero.

# Returns
* Matrix with the element-by-element bernstein coefficients required to build the dual basis function over the B-spline.
    Each column contains the coefficients for a particular cell, the e-th column for cell e.
* Also returns the A,x,b vectors of the system
"""
function computeBSplineDualBasis( self::Nurbs, gb::Int64, support::Array{Int64} )::Tuple{Array{Float64,2},Array{Float64,2},Array{Float64,1},Array{Float64,1}}

	# Initial important values
	degree = self.degree[1]
	n_elem = length(support)
	n_glob_bf = n_elem + degree
	n_bern_bf = self.nnodesperknotspan[1]*n_elem

	# The index that each element is placed in the matrix/array
	EtoI = Dict()
	for i = 1:length(support)
		e = support[i]
		EtoI[e] = i
	end

	# The index of the row that each global basis function is placed in the matrix/array
	GBtoI = Dict()
	ItoGB = zeros(Int64,n_glob_bf)
	min_gb = minimum(support)
	max_gb = maximum(support) + degree
	index = 1
	for gbval = min_gb:max_gb
		GBtoI[gbval] = index
		ItoGB[index] = gbval
		index += 1
	end

	println( "n_glob_bf: ", n_glob_bf )
	println( "n_bern_bf: ", n_bern_bf )
	println( "n_elem: ", n_elem )
	println( "support: ", support )

	# Initializing the matrix and vector used in the linear system.
	A = zeros( n_glob_bf + n_elem, n_bern_bf )
	b = zeros( n_glob_bf + n_elem )
	b[GBtoI[gb]] = 1.0

	# Compute the weights over each element.
	begin
		# The array containing the weights on each element
		we = zeros(n_elem)
		# For gaussian quadrature integration
		GQT1D = SSGaussQuad.GaussQuadTable(NumGaussPts(self.degree[1]))
		# Loop over each element to compute the weight.
		for e in support
			jacobianf = function(params)
					# dx/dxi
					T_RANGE = t_range_for_e(self,[e])
					jac = (T_RANGE[2]-T_RANGE[1])/2.0
					return jac
				end
			# The element-level index for the global basis function index.
			element_a_for_gb = self.GBEBI[1][e,gb][1]
			f = function(params)
					xi = params[1]
					if( element_a_for_gb > 0 )
						return basis(self,[e],[element_a_for_gb],[xi])
					end
					return 0.0
				end
			we[EtoI[e]] = GaussQuadIntegrate(f,GQT1D,jacobianf)
		end
		# Normalize the weights
		sum_of_weights = sum(we)
		for e in support
			we[EtoI[e]] = we[EtoI[e]]/sum_of_weights
			#println( "we[EtoI[e]] for e ",e," is:   ", we[EtoI[e]] )
		end
		# Put the weights into the b vector
		for e in support
			b[ n_glob_bf + EtoI[e] ] = we[EtoI[e]]
		end
	end

	# Assemble the upper values of the A matrix
	begin
		GQT1D = SSGaussQuad.GaussQuadTable(NumGaussPts(self.degree[1]+self.degree[1]))
		for e in support
			jacobianf = function(params)
					# dx/dxi
					T_RANGE = t_range_for_e(self,[e])
					jac = (T_RANGE[2]-T_RANGE[1])/2.0
					return jac
				end
			f = function(params)
					xi = params[1]
					B = reshape( Array{Float64}([bernstein_basis(aa,degree,xi;dmin=-1.0,dmax=1.0) for aa=1:degree+1]), degree+1, 1 )
					return (self.C_el[e][1] * B) * B'
				end
			VALS = GaussQuadIntegrate(f,GQT1D,jacobianf)
			gb_range = self.IEN[1][:,e]
			for i = 1:length(gb_range)
				gb_range[i] = GBtoI[gb_range[i]]
			end
			bb_range = collect( (EtoI[e]-1)*(degree+1)+1 : (EtoI[e])*(degree+1) )

			## Putting the values into the matrix
			A[gb_range,bb_range] += VALS
		end
	end

	# Assemble the lower values of the A matrix (element-by-element section)
	begin
		GQT1D = SSGaussQuad.GaussQuadTable(NumGaussPts(self.degree[1]+self.degree[1]))
		for e in support
			jacobianf = function(params)
					# dx/dxi
					T_RANGE = t_range_for_e(self,[e])
					jac = (T_RANGE[2]-T_RANGE[1])/2.0
					return jac
				end
			f = function(params)
					xi = params[1]
					# The element-level index for the global basis function index.
					element_a_for_gb = self.GBEBI[1][e,gb][1]
					if( element_a_for_gb > 0 )
						B = reshape( Array{Float64}([bernstein_basis(aa,degree,xi;dmin=-1.0,dmax=1.0) for aa=1:degree+1]), degree+1, 1 )
						return (( reshape(self.C_el[e][1][element_a_for_gb,:], 1,degree+1) * B) * B')[:]
					end
					return zeros( degree+1 )
				end
			VALS = GaussQuadIntegrate(f,GQT1D,jacobianf)
			bb_range = collect( (EtoI[e]-1)*(degree+1)+1 : (EtoI[e])*(degree+1) )

			## Putting the values into the matrix
			A[n_glob_bf+EtoI[e],bb_range] += VALS
		end
	end

	## Solve the linear least squares problem to find the solution 'd'.
	d = pinv(A)*b

	# The result values go in here.
	COEFFS = zeros( self.nnodesperknotspan[1], self.nbezcurves[1] )
	for e in support
		bb_range = collect( (EtoI[e]-1)*(degree+1)+1 : (EtoI[e])*(degree+1) )
		COEFFS[:,e] = d[bb_range]
	end

	return COEFFS,A,d,b

end







"""
function computeBSplineDualBasis
============
Given a B-Spline, compute the dual basis function of the given basis function.
Works for all dimensions (When it's finished).

# Parameters
* self: The B-Spline object
* a: The index of the global basis function that we want to compute the dual of.
* support:  support[dim] contains an array of 1d element indices for the support in that param dimension (must be a contiguous list)

# Returns
* Matrix with the element-by-element bernstein coefficients required to build the dual basis function over the B-spline.
    Each column contains the coefficients for a particular cell, the e-th column for cell e.
* Also returns the A,x,b vectors of the linear system
"""
function computeBSplineDualBasisGeneral( self::Nurbs, gb::Array{Int64}, support::Array{Array{Int64}} )::Tuple{ Array{Float64,2}, Array{Float64,2}, Array{Float64,1}, Array{Float64,1} }

	# Initial important values

	function compute_n_elem()
		n_elem = 1
		for dir = 1:self.dim_p
			n_elem *= length(support[dir])
		end
		return n_elem
	end
	n_elem = compute_n_elem()

	function compute_n_glob_bf()
		n_glob_bf = 1
		for dir = 1:self.dim_p
			n_glob_bf *= (length(support[dir]) + self.degree[dir])
		end
		return n_glob_bf
	end
	n_glob_bf = compute_n_glob_bf()

	function compute_n_bern_bf()
		n_bern_bf = 1
		for dir = 1:self.dim_p
			n_bern_bf *= self.nnodesperknotspan[dir] * length(support[dir])
		end
		return n_bern_bf
	end
	n_bern_bf = compute_n_bern_bf()

	@show(n_glob_bf)
	@show(n_bern_bf)
	exit()  ### STILL UNDER CONSTRUCTION.

	# The index that each element is placed in the matrix/array
	EtoI = Dict()
	for i = 1:length(support)
		e = support[i]
		EtoI[e] = i
	end

	# The index of the row that each global basis function is placed in the matrix/array
	GBtoI = Dict()
	ItoGB = zeros(Int64,n_glob_bf)
	min_gb = minimum(support)
	max_gb = maximum(support) + degree
	index = 1
	for gbval = min_gb:max_gb
		GBtoI[gbval] = index
		ItoGB[index] = gbval
		index += 1
	end

	println( "n_glob_bf: ", n_glob_bf )
	println( "n_bern_bf: ", n_bern_bf )
	println( "n_elem: ", n_elem )
	println( "support: ", support )

	# Initializing the matrix and vector used in the linear system.
	A = zeros( n_glob_bf + n_elem, n_bern_bf )
	b = zeros( n_glob_bf + n_elem )
	b[GBtoI[gb]] = 1.0

	# Compute the weights over each element.
	begin
		# The array containing the weights on each element
		we = zeros(n_elem)
		# For gaussian quadrature integration
		GQT1D = SSGaussQuad.GaussQuadTable(NumGaussPts(self.degree[1]))
		# Loop over each element to compute the weight.
		for e in support
			jacobianf = function(params)
					# dx/dxi
					T_RANGE = t_range_for_e(self,[e])
					jac = (T_RANGE[2]-T_RANGE[1])/2.0
					return jac
				end
			# The element-level index for the global basis function index.
			element_a_for_gb = self.GBEBI[1][e,gb][1]
			f = function(params)
					xi = params[1]
					if( element_a_for_gb > 0 )
						return basis(self,[e],[element_a_for_gb],[xi])
					end
					return 0.0
				end
			we[EtoI[e]] = GaussQuadIntegrate(f,GQT1D,jacobianf)
		end
		# Normalize the weights
		sum_of_weights = sum(we)
		for e in support
			we[EtoI[e]] = we[EtoI[e]]/sum_of_weights
			#println( "we[EtoI[e]] for e ",e," is:   ", we[EtoI[e]] )
		end
		# Put the weights into the b vector
		for e in support
			b[ n_glob_bf + EtoI[e] ] = we[EtoI[e]]
		end
	end

	# Assemble the upper values of the A matrix
	begin
		GQT1D = SSGaussQuad.GaussQuadTable(NumGaussPts(self.degree[1]+self.degree[1]))
		for e in support
			jacobianf = function(params)
					# dx/dxi
					T_RANGE = t_range_for_e(self,[e])
					jac = (T_RANGE[2]-T_RANGE[1])/2.0
					return jac
				end
			f = function(params)
					xi = params[1]
					B = reshape( Array{Float64}([bernstein_basis(aa,degree,xi;dmin=-1.0,dmax=1.0) for aa=1:degree+1]), degree+1, 1 )
					return (self.C_el[e][1] * B) * B'
				end
			VALS = GaussQuadIntegrate(f,GQT1D,jacobianf)
			gb_range = self.IEN[1][:,e]
			for i = 1:length(gb_range)
				gb_range[i] = GBtoI[gb_range[i]]
			end
			bb_range = collect( (EtoI[e]-1)*(degree+1)+1 : (EtoI[e])*(degree+1) )

			## Putting the values into the matrix
			A[gb_range,bb_range] += VALS
		end
	end

	# Assemble the lower values of the A matrix (element-by-element section)
	begin
		GQT1D = SSGaussQuad.GaussQuadTable(NumGaussPts(self.degree[1]+self.degree[1]))
		for e in support
			jacobianf = function(params)
					# dx/dxi
					T_RANGE = t_range_for_e(self,[e])
					jac = (T_RANGE[2]-T_RANGE[1])/2.0
					return jac
				end
			f = function(params)
					xi = params[1]
					# The element-level index for the global basis function index.
					element_a_for_gb = self.GBEBI[1][e,gb][1]
					if( element_a_for_gb > 0 )
						B = reshape( Array{Float64}([bernstein_basis(aa,degree,xi;dmin=-1.0,dmax=1.0) for aa=1:degree+1]), degree+1, 1 )
						return (( reshape(self.C_el[e][1][element_a_for_gb,:], 1,degree+1) * B) * B')[:]
					end
					return zeros( degree+1 )
				end
			VALS = GaussQuadIntegrate(f,GQT1D,jacobianf)
			bb_range = collect( (EtoI[e]-1)*(degree+1)+1 : (EtoI[e])*(degree+1) )

			## Putting the values into the matrix
			A[n_glob_bf+EtoI[e],bb_range] += VALS
		end
	end

	## Solve the linear least squares problem to find the solution 'd'.
	d = pinv(A)*b

	# The result values go in here.
	COEFFS = zeros( self.nnodesperknotspan[1], self.nbezcurves[1] )
	for e in support
		bb_range = collect( (EtoI[e]-1)*(degree+1)+1 : (EtoI[e])*(degree+1) )
		COEFFS[:,e] = d[bb_range]
	end

	return COEFFS,A,d,b

end








end # end module SSDualBasis

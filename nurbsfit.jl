#Steven Schmidt

# Takes an arbitrary function f(x), and a knot vector,
# and computes the control points and weights that best
# approximates the function.

module SSNurbsFit

include("./ssnurbstoolsjl/nurbstools.jl")
using .SSNurbsTools
import .SSNurbsTools.bernstein_basis
import .SSNurbsTools.b_spline_basis
import .SSNurbsTools.Nurbs
import .SSNurbsTools.draw
import .SSNurbsTools.basis
import .SSNurbsTools.Jacobian
import .SSNurbsTools.e_xi_for_t
import .SSNurbsTools.e_a_xi_for_gb_t
import .SSNurbsTools.bezExtOpForEl
import .SSNurbsTools.domain_X


"""
function b_spline_basis_global(gb::Int64,
                               t::Float64,
                               nurbs::Nurbs)
===========
The global basis function gb at parameter value t for the provided NURBS.

# Parameters
* gb: Global basis function index
* t: The parameter t value to be evaluated
* nurbs: The b-spline curve (weights assumed to be one?)
"""
function b_spline_basis_global(gb::Int64,
                               t::Float64,
                               nurbs::Nurbs)
    result = 0.0
    e,a,xi = e_a_xi_for_gb_t(nurbs,[gb],[t])
    if(a[1] != 0)
        C_e = bezExtOpForEl(nurbs,e)
        result += b_spline_basis(a[1],nurbs.degree[1],C_e[1],xi[1])
    end
    return result
end


"""
function fit_b_spline(t::Array{Float64,1},
                      X::Array{Float64,2},
                      degree::Int64,
                      knot_v::Array{Float64})
============
Given a sequence of sample points, find a b-spline curve
that best fits it. (Using method from 11.3 of Sederberg CAGD book)

# Parameters
* t: Parameter value of each sample point
* X: 2-by-nsamples array, with the x,f(x) pairs in each column
* degree: The degree of the curve to be fitted
* knot_v: The knot-vector of the B-spline to be fitted.

# Returns
* b_spline_curve::Nurbs
"""
function fit_b_spline(t::Array{Float64,1},
                      X::Array{Float64,2},
                      degree::Int64,
                      knot_v::Array{Float64})

    # *************************************
    # ********* Initializing NURBS  *******
    # *************************************
    dim_p = 1
    dim_s = size(X)[1]
    p = [degree]
    n = [length(knot_v)-degree-1] #len(knot_v) = n+p+1
    num_control_pts = n[1]
    spans = n .- 1
    weights = Array{Array{Float64}}(undef,1)
    weights[1] = Array{Float64}(ones(num_control_pts))
    # Init the control points to something regular
    control_pts = zeros(dim_s,num_control_pts)
    for cp_i = 1:size(control_pts)[2]
        for dim = 1:dim_s
            control_pts[dim,cp_i] = float(cp_i-1)
        end
    end
    # Create the initial guess of the b_spline_curve
    b_spline_curve = Nurbs(dim_p, dim_s, p, Array[knot_v], spans, control_pts, weights)

    # *****************************
    # *** Pinned control points ***
    # *****************************
    # (maybe allow this to be a passed-in param in the future)
    pinned_cp_i = [1,num_control_pts] # the ends
    pinned_cp_X = [X[:,1] X[:,end]]
    if(t[1] > t[end])
        # if we have switched-direction t values, then we switch this.
        pinned_cp_i = [1,num_control_pts] # the ends
        pinned_cp_X = [X[:,end] X[:,1]]
    end

    # ******
    # TODO: Project to higher-D space to turn NURBS into B-spline, then fit, then project back.
    #  This will allow us to fit with the weights, too.
    # TODO number 2: Reparameterize to make the parameterization follow the curve more evenly
    #  along the arc-length.
    # *****

    # ***************************************************************************
    # *********  Fitting Curve to Data (see 11.3 of Sederberg CAGD book)  *******
    # ***************************************************************************

    # The matrix M of the Linear solve equation M*T = B
    M = zeros(num_control_pts,num_control_pts)
    for i = 1:num_control_pts
        for j = 1:num_control_pts
            # Sum over the data points
            M[i,j] = 0.0
            for k = 1:length(t)
                M[i,j] += b_spline_basis_global(i,t[k],b_spline_curve) *  b_spline_basis_global(j,t[k],b_spline_curve)
            end
        end
    end
    # Deal with pinned control points
    for cp_i in pinned_cp_i
        M[cp_i,:] = 0.
        M[cp_i,cp_i] = 1.
    end

    # Solving for the control point values in each spatial dimension
    for dim = 1:dim_s
        # The vector B of the Linear solve equation M*T = B
        B = zeros(num_control_pts,1)
        for i = 1:num_control_pts
            B[i] = 0.0
            for k = 1:length(t)
                B[i] += X[dim,k] * b_spline_basis_global(i,t[k],b_spline_curve)
            end
        end
        # Deal with pinned control points
        for (cp_i_index,cp_i) in enumerate(pinned_cp_i)
            B[cp_i] = pinned_cp_X[dim,cp_i_index]
        end
        println("Doing matrix solve...")
        b_spline_curve.control_pts[dim,:] = M\B
    end

    # Return the resulting B-Spline b_spline_curve
    return b_spline_curve
end


"""
function find_t_samples(x::Array{Float64},
                        fofx::Array{Float64},
                        dfdx::Function,
                        t_dir::Int64)
=============

Finds evenly-spaced values of t along the arc-length of the curve

# Parameters
* x
* fofx
* dfdx
* t_dir: +1 = t goes from xmin to xmax, -1 = from xmax to xmin.

"""
function find_t_samples(x::Array{Float64},
                        fofx::Array{Float64},
                        dfdx::Function,
                        t_dir::Int64)

    if(t_dir != 1 && t_dir != -1)
        println("Error with t_dir.")
        exit()
    end

    #=
    # Approach 1 (innefficient)
    param_t_for_x = function(x0::Float64,x::Float64,dfdx::Function)
        # Line integral
        tointegrate = function(x::Float64)
            return sqrt(1+dfdx(x)^2)
        end
        return abs(quadgk(tointegrate,x0,x)[1])
    end
    # Use the line-integral to specify each t-value
    if( t_dir == 1 )
        x_start = minimum(x)
    else
        x_start = maximum(x)
    end
    t = zeros(length(x))
    for k = 1:length(t)
        integral = param_t_for_x(x_start,x[k],dfdx)
        t[k] = integral
    end
    =#

    # Approach 2 (more efficient)
    arclength_integral = function(x0::Float64,x::Float64,dfdx::Function)
        # Line integral
        tointegrate = function(x::Float64)
            return sqrt(1+dfdx(x)^2)
        end
        return abs(quadgk(tointegrate,x0,x)[1])
    end
    # Use the line-integral to specify each t-value
    t2 = zeros(length(x))
    if ((t_dir == 1 && minimum(x) == x[1]) || (t_dir == -1 && minimum(x) == x[end]))
        k_indices = 1:length(t2)
    else
        k_indices = length(t2):-1:1
    end
    prev_integral = 0.0
    prev_k = k_indices[1]
    for k in k_indices
        integral = prev_integral + arclength_integral(x[prev_k],x[k],dfdx)
        t2[k] = integral
        prev_integral = integral
        prev_k = k
    end

    #@show t_dir,norm(t-t2)
    # return t

    return t2
end




"""
function fit_b_spline(x::Array{Float64},
                      fofx::Array{Float64},
                      t::Array{Float64},
                      degree::Int64,
                      n_bez::Int64)
============
Does the fitting once to the x values already sampled
(But generates the parameter t values as the arc-length
value along the curve, using the 'dfdx' function)

# Parameters
* x
* fofx
* t: Array of t values corresponding to each x value
* degree
* n_bez

# Returns
* b_spline_curve::Nurbs
* X::Array{Float64,2} of dimension 2-by-nsamples containing x,f(x) pairs
   that were sampled along f and were the points fitted to.
"""
function fit_b_spline(x::Array{Float64},
                      fofx::Array{Float64},
                      t::Array{Float64},
                      degree::Int64,
                      n_bez::Int64)

    # Save the max and min of t
    tmin = minimum(t)
    tmax = maximum(t)

    # The x,f(x) values, with the x values on the first row and the f(x) values
    # on the second row.  A.k.a, each column is an [x,y] point.
    X = [reshape(x,1,length(x));
         reshape(fofx,1,length(fofx))]

    # **********************************************************
    # ** Computing the B-spline curve that fits to the points **
    # **********************************************************

    # ****************************
    # Constructing the knot vector
    # ****************************
    t_intervals = zeros(n_bez)
    t_intervals[:] = (tmax-tmin)/n_bez
    # The sum of "t_intervals" should be equal to approx (tmax-tmin)
    knot_v = [tmin]
    for i = 1:degree
        push!(knot_v,tmin)
    end
    t_sofar = tmin
    for i = 1:n_bez-1
        t_sofar = t_sofar + t_intervals[i]
        push!(knot_v,t_sofar)
    end
    for i = 1:degree
        push!(knot_v,tmax)
    end
    push!(knot_v,tmax)

    # ****************************
    # ****** Doing the fit *******
    # ****************************

    # Fit once against points sampled along the x-axis
    b_spline_curve = fit_b_spline(t,X,degree,knot_v)

    return b_spline_curve,X
end


"""
function fit_b_spline(f::Function,
                      dfdx::Function,
                      xmin::Float64,
                      xmax::Float64,
                      nsamples::Int64,
                      n_bez::Int64)
=========

Does the fit twice, once with the x-values
sampled along the x-axis, and a second time
with the x-values sampled along the arc-length
of the curve (approximately, according to the
first step's approxmiation)

# Parameters
* f: Function of x to be approximated by a B-Spline
* dfdx: Derivative of f
* xmin, xmax: Span of domain to be approximated
* nsamples: The number of samples in the domain to take, to approximate to.
* n_bez:  The number of bezier curves in the produced B-spline

# Returns
* b_spline_curve::Nurbs
* X::Array{Float64,2} of dimension 2-by-nsamples containing x,f(x) pairs
   that were sampled along f and were the points fitted to.
"""
function fit_b_spline(f::Function,
                      dfdx::Function,
                      xmin::Float64,
                      xmax::Float64,
                      t_dir::Int64,
                      nsamples::Int64,
                      degree::Int64,
                      n_bez::Int64)

    if(t_dir != 1 && t_dir != -1)
        println("Error with t_dir.")
        exit()
    end

    # Do it once, with the samples going along the x-axis
    h = (xmax-xmin)/(nsamples-1)-1e-14
    x = collect(xmin:h:xmax)
    fofx = Array{Float64}([f(x[i]) for i = 1:length(x)])
    t = find_t_samples(x,fofx,dfdx,t_dir)
    t = t/maximum(t) # Normalize t
    b_spline_curve,X = fit_b_spline(x, fofx, t, degree, n_bez)


    # Do it again, with the samples going along the arc-length
    CURVE_X,t_cur,h_cur = draw(b_spline_curve,[nsamples])
    x = CURVE_X[1,:][:]
    # Set the values that are out of range to either xmin or xmax
    x_lt_xmin(val) = val < xmin
    x_gt_xmax(val) = val > xmax
    x[find(x_lt_xmin,x)] = xmin
    x[find(x_gt_xmax,x)] = xmax
    # Find the f(x) values
    fofx = Array{Float64}([f(x[i]) for i = 1:length(x)])
    # find the t values again.
    t = find_t_samples(x,fofx,dfdx,t_dir)
    # Normalize t
    t = t/maximum(t)
    b_spline_curve,X = fit_b_spline(x, fofx, t, degree, n_bez)

    gc()

    return b_spline_curve,X
end




"""
function fit_b_spline(f::Array{Function},
                      dfdx::Array{Function},
                      xmin::Array{Float64},
                      xmax::Array{Float64},
                      t_dir::Array{Int64},
                      nsamples::Int64,
                      degree::Int64,
                      n_bez::Int64)
=========
Find the fit to a connected series of separate piecewise functions
"""
function fit_b_spline(f::Array{Function},
                      dfdx::Array{Function},
                      xmin::Array{Float64},
                      xmax::Array{Float64},
                      t_dir::Array{Int64},
                      nsamples::Int64,
                      degree::Int64,
                      n_bez::Int64)

    x_arrays = Array{Any}(0)
    fofx_arrays = Array{Any}(0)
    t_arrays = Array{Any}(0)

    # Find the x, fofx, and t arrays for each function
    for func_i = 1:length(f)
        h = (xmax[func_i]-xmin[func_i])/(nsamples-1)-1e-14
        x = collect(xmin[func_i]:h:xmax[func_i])
        fofx = Array{Float64}([f[func_i](x[i]) for i = 1:length(x)])
        t = find_t_samples(x,fofx,dfdx[func_i],t_dir[func_i])

        # Resample the x-values one more time to make them more even along the arc-length
        b_spline_curve, X = fit_b_spline(x, fofx, t, degree, n_bez)
        CURVE_X,t_cur,h_cur = draw(b_spline_curve,[nsamples])
        x = CURVE_X[1,:][:]
        # Set the values that are out of range to either xmin or xmax
        x_lt_xmin(val) = val < xmin[func_i]
        x_gt_xmax(val) = val > xmax[func_i]
        x[find(x_lt_xmin,x)] = xmin[func_i]
        x[find(x_gt_xmax,x)] = xmax[func_i]
        # Find the f(x) values
        fofx = Array{Float64}([f[func_i](x[i]) for i = 1:length(x)])
        # find the t values again.
        t = find_t_samples(x,fofx,dfdx[func_i],t_dir[func_i])

        gc()

        push!(x_arrays,x)
        push!(fofx_arrays,fofx)
        push!(t_arrays,t)
    end

    # Combine them together into a single array
    x_all = Array{Float64}(0)
    fofx_all = Array{Float64}(0)
    t_all = Array{Float64}(0)

    prev_t_max = 0.0
    for k = 1:length(t_arrays)
        i_indices = 1:length(t_arrays[k])
        if(t_arrays[k][1] > t_arrays[k][end])
            i_indices = length(t_arrays[k]):-1:1
        end
        for i in i_indices
            push!(x_all,x_arrays[k][i])
            push!(fofx_all,fofx_arrays[k][i])
            new_t_val = t_arrays[k][i] + prev_t_max
            push!(t_all,new_t_val)
        end
        prev_t_max = maximum(t_all)
    end

    # Normalize the t array again
    t_all = t_all/maximum(t_all)

    # Compute the curve.
    b_spline_curve,X = fit_b_spline(x_all, fofx_all, t_all, degree, n_bez)

    gc()

    return b_spline_curve,X
end








end # end module SSNurbsFit
